export default {
    // baseUrl: window.baseUrl,
    uploadUrl: window.uploadUrl,
    baseUrl: process.env.NODE_ENV === 'production' ? window.baseUrl : '/api'
}
