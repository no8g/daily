import Vue from 'vue'
import Router from 'vue-router'
import store from './../store'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'index',
            redirect: '/query_daily',
            component: () => import(/* webpackChunkName: "index" */ '@/views/index/index.vue'),
            children: [{
                path: '/add_daily',
                name: 'add_daily',
                component: () => import(/* webpackChunkName: "add_daily" */ '@/views/add_daily/index.vue'), // 添加日报
            }, {
                path: '/query_daily',
                name: 'query_daily',
                component: () => import(/* webpackChunkName: "query_daily" */ '@/views/query_daily/index.vue'), // 查看日报
            }, {
                path: '/daily_statistics',
                name: 'daily_statistics',
                component: () => import(/* webpackChunkName: "daily_statistics" */ '@/views/daily_statistics/index.vue'), // 日报统计
            }, {
                path: '/role_manage',
                name: 'role_manage',
                component: () => import(/* webpackChunkName: "role" */ '@/views/config/role/index.vue'), // 角色
            }, {
                path: '/team_manage',
                name: 'team_manage',
                component: () => import(/* webpackChunkName: "role" */ '@/views/team/index.vue'), // 团队
            }, {
                path: '/position_manage',
                name: 'position_manage',
                component: () => import(/* webpackChunkName: "role" */ '@/views/position/index.vue'), // 职位管理
            }]
        }, {
            path: '/404',
            name: '404',
            component: () => import(/* webpackChunkName: "404" */ '../views/error/404.vue')
        }
    ]
})

function checkMenus(url) {
    // 检查有没有对应的权限
    let bool = false
    if (store.state.userInfo.menu.childs) {
        store.state.userInfo.menu.childs.find(m => {
            const mChild = m.childs
            if (mChild.length > 0) {
                mChild.find(child => {
                    const cChild = child.childs
                    if (cChild.length > 0) {
                        try {
                            cChild.find(c => {
                                if (c.url === `/${url.split('/')[1]}`) {
                                    bool = true
                                    return true
                                }
                            })
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    if (child.url === url) {
                        bool = true
                        return true
                    }
                    if (bool) {
                        return true
                    }
                })
            } else {
                if (m.url === url) {
                    bool = true
                    return true
                }
            }
        })
    } else {
        bool = true
    }
    return bool
}

// 全局路由改变前钩子
router.beforeEach((to, from, next) => {
    // 这里简单地使用localStorage来判断是否登陆
    // if (!window.sessionStorage.getItem('accessToken')) {
    //     if (to.fullPath !== '/login') {
    //         next({ name: 'login', replace: true })
    //     }
    // }
    // next()
    // 跳转的path存在权限表中则进行跳转
    if (to.path !== '/login' && to.path !== '/404' && to.path !== '/') {
        // 检查菜单权限中有没有对应的url
        if (checkMenus(to.path)) {
            next()
        } else {
            // 没权限跳转到404页面，或执行其他操作
            // next({ name: '404', replace: true })
            next()
        }
    } else {
        next()
    }
})

export default router
