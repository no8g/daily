import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/newMaterialApplication/list`, 'GET', params, successCallback, errorCallback)
}

function add(params, successCallback, errorCallback) {
    axiosRequest(`/daily/add`, 'POST', params, successCallback, errorCallback)
}

function getDetail(params, successCallback, errorCallback) {
    axiosRequest(`/newMaterialApplication/${params['processId']}/${params['processInstanceCode']}`, 'GET', {}, successCallback, errorCallback)
}

function getLastDaily(params, successCallback, errorCallback) {
    axiosRequest(`/daily/getLastDaily`, 'GET', params, successCallback, errorCallback)
}

export default {
    getList,
    add,
    getDetail,
    getLastDaily,
}
