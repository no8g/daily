/*
* 评论相关接口
* */
import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/comment/list`, 'GET', params, successCallback, errorCallback)
}

function add(params, successCallback, errorCallback) {
    axiosRequest(`/comment/add`, 'POST', params, successCallback, errorCallback)
}

export default {
    getList,
    add,
}
