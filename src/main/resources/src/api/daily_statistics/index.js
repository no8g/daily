import axiosRequest from '@/api/common/common.js'
import config from '@/config.js'
import request from '@/axios/config'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/api/statistics/pageList`, 'POST', params, successCallback, errorCallback)
}

function exportExcel(params, successCallback = () => {}, errorCallback = () => {}) {
    request.axios({ // 用axios发送post请求
        method: 'post',
        url: `${config.baseUrl}/api/statistics/excel`, // 请求地址
        data: {
            startDate: params.startDate,
            endDate: params.endDate,
            deptId: params.deptId,
            pageNum: 1,
            pageSize: 9999,
        }, // 参数
        responseType: 'blob' // 表明返回服务器返回的数据类型
    }).then(res => {
        console.log(res)
        if (res.status === 200) {
            try {
                const blobContent = new Blob(
                    [res.data],
                    {
                        type: 'application/vnd.ms-excel'
                    }
                )
                const blobUrl = window.URL.createObjectURL(blobContent)
                downloadFileByBlob(blobUrl, `日报统计${params.startDate}~${params.endDate}.xlsx`)
            } catch (e) {
                errorCallback('导出失败，您的浏览器好像不支持这种导出')
            }
        } else {
            errorCallback('导出失败')
        }
    })
}

// 下载文件
function downloadFileByBlob(blobUrl, filename) {
    const eleLink = document.createElement('a')
    eleLink.download = filename
    eleLink.style.display = 'none'
    eleLink.href = blobUrl
    // 触发点击
    document.body.appendChild(eleLink)
    eleLink.click()
    URL.revokeObjectURL(blobUrl) // 释放URL 对象
    // 然后移除
    document.body.removeChild(eleLink)
}

export default {
    getList,
    exportExcel
}
