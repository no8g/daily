import axiosRequest from '@/api/common/common.js'

function login(params, successCallback, errorCallback) {
    axiosRequest(`/login`, 'GET', params, successCallback, errorCallback)
}

function updatePassword(params, successCallback, errorCallback) {
    axiosRequest(`/user/updatePassword`, 'GET', params, successCallback, errorCallback)
}

function getUser(params, successCallback, errorCallback) {
    axiosRequest(`/user/getUser`, 'GET', params, successCallback, errorCallback)
}

// 获取员工列表
function getUserList(params, successCallback, errorCallback) {
    axiosRequest(`/roleConfig/userList`, 'GET', params, successCallback, errorCallback)
}

// 获取部门列表
function getDeptList(params, successCallback, errorCallback) {
    axiosRequest(`/roleConfig/deptList`, 'GET', params, successCallback, errorCallback)
}

// 绑定角色
function getRoleList(params, successCallback, errorCallback) {
    axiosRequest(`/roleConfig/pageList`, 'POST', params, successCallback, errorCallback)
}

// 删除角色
function delRole(params, successCallback, errorCallback) {
    axiosRequest(`/roleConfig/del/${params.id}`, 'GET', params, successCallback, errorCallback)
}

function getAllDept(params, successCallback, errorCallback) {
    axiosRequest(`/api/statistics/getAllDeptList`, 'GET', params, successCallback, errorCallback)
}

// 添加角色
function addRole(params, successCallback, errorCallback) {
    axiosRequest(`/roleConfig/add`, 'POST', params, successCallback, errorCallback)
}

// 修改角色
function updateRole(params, successCallback, errorCallback) {
    axiosRequest(`/roleConfig/update`, 'POST', params, successCallback, errorCallback)
}

export default {
    login,
    updatePassword,
    getUser,
    getDeptList,
    getUserList,
    getRoleList,
    delRole,
    addRole,
    updateRole,
    getAllDept
}
