import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/user/page`, 'GET', params, successCallback, errorCallback)
}

function add(params, successCallback, errorCallback) {
    axiosRequest(`/user/add`, 'POST', params, successCallback, errorCallback)
}

function update(params, successCallback, errorCallback) {
    axiosRequest(`/user/update`, 'POST', params, successCallback, errorCallback)
}

function del(params, successCallback, errorCallback) {
    axiosRequest(`/user/delete`, 'GET', params, successCallback, errorCallback)
}

function resetPWD(params, successCallback, errorCallback) {
    axiosRequest(`/user/resetPassword`, 'GET', params, successCallback, errorCallback)
}

// 获取当前登陆人信息
function getUser(params, successCallback, errorCallback) {
    axiosRequest(`/user/getUser`, 'GET', params, successCallback, errorCallback)
}

export default {
    getList,
    add,
    update,
    del,
    resetPWD,
    getUser
}
