import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/role/page`, 'GET', params, successCallback, errorCallback)
}

function add(params, successCallback, errorCallback) {
    axiosRequest(`/role/add`, 'POST', params, successCallback, errorCallback)
}

function update(params, successCallback, errorCallback) {
    axiosRequest(`/role/update`, 'POST', params, successCallback, errorCallback)
}

function del(params, successCallback, errorCallback) {
    axiosRequest(`/role/delete`, 'GET', params, successCallback, errorCallback)
}

export default {
    getList,
    add,
    update,
    del
}
