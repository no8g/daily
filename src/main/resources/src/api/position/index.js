import axiosRequest from '@/api/common/common.js'

// 获取职位详情
function getDeptList(params, successCallback, errorCallback) {
    axiosRequest(`/position/info/${params.id}`, 'GET', params, successCallback, errorCallback)
}

// 绑定职位
function getRoleList(params, successCallback, errorCallback) {
    axiosRequest(`/position/pageList`, 'POST', params, successCallback, errorCallback)
}

function getPosition(params, successCallback, errorCallback) {
    axiosRequest(`/position/page`, 'POST', params, successCallback, errorCallback)
}

// 删除职位
function delPosition(params, successCallback, errorCallback) {
    axiosRequest(`/position/del/${params.id}`, 'GET', params, successCallback, errorCallback)
}

// 添加职位
function addPosition(params, successCallback, errorCallback) {
    axiosRequest(`/position/add`, 'POST', params, successCallback, errorCallback)
}

// 修改职位
function updatePosition(params, successCallback, errorCallback) {
    axiosRequest(`/position/update`, 'POST', params, successCallback, errorCallback)
}

export default {
    getDeptList,
    getRoleList,
    getPosition,
    delPosition,
    addPosition,
    updatePosition
}
