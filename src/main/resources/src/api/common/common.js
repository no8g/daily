import request from '@/axios/config'
import config from '../../config'

// 封装请求
function axiosRequest(url, type, params, successCallback = (e) => console.log(e), errorCallback = (e) => console.log(e)) {
    const fn = {
        'POST': () => {
            request.axios.post(`${config.baseUrl + url}`, params)
                .then(response => {
                    switch (response.data.code) {
                    case 0:
                        successCallback(response.data)
                        break
                    case 320108:
                        break
                    default:
                        errorCallback(response.data.message || '操作失败，请稍后重试！')
                    }
                })
                .catch((error) => {
                    console.log(error)
                    errorCallback(error || '操作失败，请稍后重试！')
                })
        },
        'PUT': () => {
            request.axios.put(`${config.baseUrl + url}`, params)
                .then(response => {
                    switch (response.data.code) {
                    case 0:
                        successCallback(response.data)
                        break
                    case 320108:
                        break
                    default:
                        errorCallback(response.data.message || '操作失败，请稍后重试！')
                    }
                })
                .catch((error) => {
                    console.log(error)
                    errorCallback(error || '操作失败，请稍后重试！')
                })
        },
        'GET': () => {
            request.axios.get(`${config.baseUrl + url}`, { params })
                .then(response => {
                    switch (response.data.code) {
                    case 0:
                        successCallback(response.data)
                        break
                    case 320108:
                        break
                    default:
                        errorCallback(response.data.message || '操作失败，请稍后重试！')
                    }
                })
                .catch((error) => {
                    console.log(error)
                    errorCallback(error || '操作失败，请稍后重试！')
                })
        },
        'DELETE': () => {
            request.axios.delete(`${config.baseUrl + url}`, { params })
                .then(response => {
                    switch (response.data.code) {
                    case 0:
                        successCallback(response.data)
                        break
                    case 320108:
                        break
                    default:
                        errorCallback(response.data.message || '操作失败，请稍后重试！')
                    }
                })
                .catch((error) => {
                    console.log(error)
                    errorCallback(error || '操作失败，请稍后重试！')
                })
        }
    }
    // 根据请求类型进行请求
    fn[type.toUpperCase()]()
}

export default axiosRequest
