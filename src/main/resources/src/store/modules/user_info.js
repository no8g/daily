import userApi from '@/api/user/user'
import menu from '@/assets/images/menu.png'
import menu1 from '@/assets/images/menu1.png'
import menu2 from '@/assets/images/menu2.png'
import menu3 from '@/assets/images/menu3.png'
import menu4 from '@/assets/images/menu6.png'
import menu5 from '@/assets/images/menu7.png'
import menu6 from '@/assets/images/menu4.png'
import menu7 from '@/assets/images/menu5.png'
import menu8 from '@/assets/images/menu8.png'
import menu9 from '@/assets/images/menu9.png'
// import menu10 from '@/assets/images/menu10.png'
// import menu11 from '@/assets/images/menu11.png'
// initial state
const state = {
    menu: {
        'privilegeCode': '',
        'code': '',
        'icon': '',
        'comment': '',
        'sort': 20,
        'id': 452,
        'title': 'UIOT日报管理',
        'childs': [],
    },
    userInfo: {
        department: '',
        userName: '',
        trueName: ''
    },
    trueName: '',
    tagsList: [],
    authButton: {}
}

// getters
const getters = {
    isInCharge: (state, getters, rootState) => {
        return state.isInCharge
    },
}

// actions
const actions = {
    getUserInfo({ commit }) {
        return new Promise((resolve, reject) => {
            resolve(state.menu)
            // 小主机
            userApi.getUser({}, data => {
                const loginUserName = data.result.userName
                let childs = [{
                    'code': '',
                    'icon': menu1,
                    'activeIcon': menu,
                    'sort': 2,
                    'title': '填写日报',
                    'childs': [],
                    'parentId': 474,
                    'url': '/add_daily',
                    'privilegeCode': 'add-daily',
                    'comment': '',
                    'id': 476,
                    'applicationCode': 'back-operation',
                    'resourceType': 1,
                    'status': 1
                }, {
                    'code': '',
                    'icon': menu3,
                    'activeIcon': menu2,
                    'sort': 2,
                    'title': '查看日报',
                    'childs': [],
                    'parentId': 474,
                    'url': '/query_daily',
                    'privilegeCode': 'query-daily',
                    'comment': '',
                    'id': 477,
                    'applicationCode': 'back-operation',
                    'resourceType': 1,
                    'status': 1
                }, {
                    'code': '',
                    'icon': menu7,
                    'activeIcon': menu6,
                    'sort': 2,
                    'title': '日报统计',
                    'childs': [],
                    'parentId': 474,
                    'url': '/daily_statistics',
                    'privilegeCode': 'daily-statistics',
                    'comment': '',
                    'id': 478,
                    'applicationCode': 'back-operation',
                    'resourceType': 1,
                    'status': 1
                }]
                childs = state.menu.childs.concat(childs)
                if (loginUserName === '18538290858') {
                    childs.shift()
                }
                if (loginUserName === '13137737136') {
                    childs.push({
                        'code': '',
                        'icon': menu5,
                        'activeIcon': menu4,
                        'sort': 2,
                        'title': '角色管理',
                        'childs': [],
                        'parentId': 474,
                        'url': '/role_manage',
                        'privilegeCode': 'role-manage',
                        'comment': '',
                        'id': 479,
                        'applicationCode': 'back-operation',
                        'resourceType': 1,
                        'status': 1
                    })
                    childs.push({
                        'code': '',
                        'icon': menu9,
                        'activeIcon': menu8,
                        'sort': 2,
                        'title': '团队管理',
                        'childs': [],
                        'parentId': 474,
                        'url': '/team_manage',
                        'privilegeCode': 'team-manage',
                        'comment': '',
                        'id': 480,
                        'applicationCode': 'back-operation',
                        'resourceType': 1,
                        'status': 1
                    })
                    childs.push({
                        'code': '',
                        'icon': menu1,
                        'activeIcon': menu,
                        'sort': 2,
                        'title': '职位管理',
                        'childs': [],
                        'parentId': 474,
                        'url': '/position_manage',
                        'privilegeCode': 'position-manage',
                        'comment': '',
                        'id': 481,
                        'applicationCode': 'back-operation',
                        'resourceType': 1,
                        'status': 1
                    })
                }
                let menuList = {
                    menu: {
                        ...state.menu,
                        childs: childs
                    }
                }
                console.log(menuList)
                commit('setUserInfo', {
                    menu: menuList.menu,
                    userInfo: data.result
                })
                resolve(menuList.menu)
            }, err => console.log(err))
        })
    },
    setMenuType(state, type) {
        state.menuType = type
    },
}

// mutations
const mutations = {
    setUserInfo(state, data) {
        state.menu = data.menu
        state.userInfo = data.userInfo
    },
    pushTagsList(state, data) {
        state.tagsList.push(data)
    },
    spliceTagsList(state, index) {
        state.tagsList.splice(index, 1)
    },
    setAuthButton(state, data) {
        state.authButton[data.key] = data.value
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
