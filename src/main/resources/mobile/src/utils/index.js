const utils = {
    /**
    * 日期格式化
    * utils.dateFormat('2019/01/01', 'yyyy-MM-dd hh:mm:ss');
    **/
    dateFormat(date, format) {
        if (typeof date === 'string') {
            let dr = date.replace(/-/g, '/').replace(/T/g, ' ')
            date = dr.split('.')[0]
        }
        date = new Date(date)
        var map = {
            'M': date.getMonth() + 1, // 月份
            'd': date.getDate(), // 日
            'h': date.getHours(), // 小时
            'm': date.getMinutes(), // 分
            's': date.getSeconds(), // 秒
            'q': Math.floor((date.getMonth() + 3) / 3), // 季度
            'S': date.getMilliseconds() // 毫秒
        }

        format = format.replace(/([yMdhmsqS])+/g, function(all, t) {
            var v = map[t]
            if (v !== undefined) {
                if (all.length > 1) {
                    v = '0' + v
                    v = v.substr(v.length - 2)
                }
                return v
            } else if (t === 'y') {
                return (date.getFullYear() + '').substr(4 - all.length)
            }
            return all
        })
        return format
    },
    // pySegSort(["我","不","懂","爱","啊","按","已","呀","选","县"])
    pySegSort(arr, empty) {
        if (!String.prototype.localeCompare) {
            return null
        }
        var letters = '*abcdefghjklmnopqrstwxyz'.split('')
        var zh = '阿八嚓哒妸发旮哈讥咔垃痳拏噢妑七呥扨它穵夕丫帀'.split('')
        var segs = []
        var curr
        letters.forEach(function(key, i) {
            curr = {letter: key, data: []}
            arr.forEach(function(j) {
                if ((!zh[i - 1] || zh[i - 1].localeCompare(j.province, 'zh') <= 0) && j.province.localeCompare(zh[i], 'zh') === -1) {
                    curr.data.push(j)
                }
            })
            if (empty || curr.data.length) {
                segs.push(curr)
                curr.data.sort(function(a, b) {
                    return a.province.localeCompare(b.province, 'zh')
                })
            }
        })
        return segs
    },
    millisecondFormat(s) {
        // 防止传入非正常数值
        if (typeof s !== 'number') {
            s = Number(s) || 0
        }
        let m = 60 // 1分钟的毫秒数
        let h = m * 60 // 1小时的毫秒数
        let d = h * 24 // 1天的毫秒数
        if (s < m) {
            return `${s}秒`
        }
        if (s < h) {
            return `${parseInt(s / m)}分钟${s % m}秒`
        }
        if (s < d) {
            return `${parseInt(s / h)}小时${parseInt(s % h / m)}分钟${parseInt(s % h % m)}秒`
        }
        return `${parseInt(s / d)}天${parseInt(s % d / h)}小时${parseInt(s % d % h / m)}分钟${parseInt(s % d % h % m)}秒`
    },
    // 对象a的key值从对象b对应的key取
    assignKey(a, b) {
        // let objKeys = Object.keys(a)
        // objKeys.forEach(o => {
        //     if (b[o] && b[o] !== undefined && b[o] !== null && b[o].length !== 0) {
        //         a[o] = b[o]
        //     }
        // })

        const objKeys = Object.keys(a)
        const bKeys = Object.keys(b)
        objKeys.forEach(o => {
            if (bKeys.indexOf(o) !== -1) {
                a[o] = b[o]
            } else {
                if (Array.isArray(a[o])) {
                    a[o] = []
                } else {
                    a[o] = undefined
                }
            }
        })
        return a
    },
    // 验证图片的尺寸
    verifyImageAttr(file, size, that = this) {
        return new Promise((resolve, reject) => {
            let oFileReader = new FileReader()
            oFileReader.onload = function(e) {
                let base64 = e.target.result
                let img = new Image()
                img.src = base64
                img.onload = function() {
                    console.log(img.naturalWidth, img.naturalHeight)
                    size.width = parseInt(size.width)
                    size.height = parseInt(size.height)
                    console.log(size.width, size.height)
                    console.log(img.naturalWidth === size.width)
                    console.log(img.naturalHeight === size.height)
                    if (img.naturalWidth !== size.width) {
                        that.$message.error('上传的尺寸不正确，请删除并重新上传！')
                        return false
                    }
                    if (img.naturalHeight !== size.height) {
                        that.$message.error('上传的尺寸不正确，请删除并重新上传！')
                        return false
                    }
                    resolve()
                }
                img.onerror = function() {
                    reject(new Error('图片读取失败！'))
                }
            }
            oFileReader.readAsDataURL(file)
        })
    },
    // 获取文件的大小
    getFileAttr(file) {
        let oFileReader = new FileReader()
        oFileReader.onloadend = function(e) {
            fn({
                size: e.total
            })
        }
        oFileReader.readAsDataURL(file)
    },
    // 获取元素位置
    elementPosition(obj) {
        let curleft = 0
        let curtop = 0
        if (obj.offsetParent) {
            curleft = obj.offsetLeft
            curtop = obj.offsetTop
            while (obj = obj.offsetParent) {
                curleft += obj.offsetLeft
                curtop += obj.offsetTop
            }
        }
        return {
            x: curleft, y: curtop
        }
    }
}

export default utils
