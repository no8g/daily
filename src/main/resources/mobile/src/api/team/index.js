import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/project/pageList`, 'POST', params, successCallback, errorCallback)
}

function add(params, successCallback, errorCallback) {
    axiosRequest(`/project/add`, 'POST', params, successCallback, errorCallback)
}

function getDetail(params, successCallback, errorCallback) {
    axiosRequest(`/project/info/${params.id}`, 'GET', {}, successCallback, errorCallback)
}

function update(params, successCallback, errorCallback) {
    axiosRequest(`/project/update`, 'PUT', params, successCallback, errorCallback)
}

function del(params, successCallback, errorCallback) {
    axiosRequest(`/project/del/${params.id}`, 'GET', {}, successCallback, errorCallback)
}

function addBatch(params, successCallback, errorCallback) {
    axiosRequest(`/team/addBatch`, 'post', params, successCallback, errorCallback)
}

function updateTeam(params, successCallback, errorCallback) {
    axiosRequest(`/team/update`, 'post', params, successCallback, errorCallback)
}

export default {
    getList,
    add,
    getDetail,
    update,
    del,
    addBatch,
    updateTeam
}
