import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/myAgent/pageList`, 'GET', params, successCallback, errorCallback)
}

function add(params, successCallback, errorCallback) {
    axiosRequest(`/myAgent/add`, 'POST', params, successCallback, errorCallback)
}

function getDetail(params, successCallback, errorCallback) {
    axiosRequest(`/myAgent/info`, 'GET', params, successCallback, errorCallback)
}

function update(params, successCallback, errorCallback) {
    axiosRequest(`/myAgent/update`, 'PUT', params, successCallback, errorCallback)
}

function del(params, successCallback, errorCallback) {
    axiosRequest(`/myAgent/delete`, 'DELETE', params, successCallback, errorCallback)
}

export default {
    getList,
    add,
    getDetail,
    update,
    del,
}
