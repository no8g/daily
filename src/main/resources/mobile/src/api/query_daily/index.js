import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/daily/pageList`, 'POST', params, successCallback, errorCallback)
}

// 已读列表
function getReadList(params, successCallback, errorCallback) {
    axiosRequest(`/readRecord/pageRead`, 'POST', params, successCallback, errorCallback)
}

// 未读列表
function getNoReadList(params, successCallback, errorCallback) {
    axiosRequest(`/readRecord/pageNoRead`, 'POST', params, successCallback, errorCallback)
}

// 团队日报列表
function getTeamList(params, successCallback, errorCallback) {
    axiosRequest(`/team/pageList`, 'POST', params, successCallback, errorCallback)
}

// 添加日报已读记录，用于新增日报已读记录
function read(params, successCallback, errorCallback) {
    axiosRequest(`/readRecord/add/${params.id}`, 'POST', {}, successCallback, errorCallback)
}

// 职位（部门经理、项目经理、技术专家）
function positionTypes(params, successCallback, errorCallback) {
    axiosRequest(`/position/pageList`, 'POST', params, successCallback, errorCallback)
}

// 我发出的
function mySend(params, successCallback, errorCallback) {
    axiosRequest(`/daily/my/pageList`, 'POST', params, successCallback, errorCallback)
}

// 未读数量：马总
function getCountTotal(params, successCallback, errorCallback) {
    axiosRequest(`/daily/init/count`, 'GET', params, successCallback, errorCallback)
}

// 全部（已读和未读）：马总
function allCountTotal(params, successCallback, errorCallback) {
    axiosRequest(`/daily/ma/pageList`, 'POST', params, successCallback, errorCallback)
}

// 用于查看日报列表展示时，有未完成工作安排时查询未完成工作安排详情列表
function undoneList(params, successCallback, errorCallback) {
    axiosRequest(`/daily/undone/list`, 'GET', params, successCallback, errorCallback)
}

// 钉钉提醒，用于未完成工作提醒
function dingRemind(params, successCallback, errorCallback) {
    axiosRequest(`/ding/remind/undone`, 'POST', params, successCallback, errorCallback)
}
export default {
    getList,
    read,
    getReadList,
    getNoReadList,
    getTeamList,
    positionTypes,
    mySend,
    getCountTotal,
    allCountTotal,
    undoneList,
    dingRemind
}
