import axiosRequest from '@/api/common/common.js'

function getList(params, successCallback, errorCallback) {
    axiosRequest(`/role/privilegeTree`, 'GET', params, successCallback, errorCallback)
}

function bind(params, successCallback, errorCallback) {
    axiosRequest(`/role/privilege/bind`, 'POST', params, successCallback, errorCallback)
}
function getBind(params, successCallback, errorCallback) {
    axiosRequest(`/role/privilege/bind/list`, 'GET', params, successCallback, errorCallback)
}

/**
 * 新老主机菜单获取
 * */
function getSystemPrivilege(params, successCallback, errorCallback) {
    axiosRequest(`/system/privilege/list`, 'GET', params, successCallback, errorCallback)
}

export default {
    getList,
    bind,
    getBind,
    getSystemPrivilege
}
