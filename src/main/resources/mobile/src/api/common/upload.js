// import axiosRequest from '@/api/common/common.js'
import config from '@/config.js'
import request from '@/axios/config'

/**
 * 上传文件
 * */
function uploadFile(params, successCallback, errorCallback) {
    request.axios.post(`/api/upload/file/sample`, params)
        .then(response => {
            switch (response.data.success) {
            case true:
                successCallback(response.data)
                break
            default:
                break
            }
        })
        .catch(function(error) {
            errorCallback(error)
        })
}

/**
 * 下载
 * */
function downLoad(url, fileName) {
    var downType = false
    const link = document.createElement('a')
    const path = url.split('/').slice(3).join('/')
    link.setAttribute('download', '')
    link.style.display = 'none'
    var arr = ['.jpg', '.jpeg', '.bmp', '.png', '.txt', '.pdf', '.PDF']
    arr.forEach(item => {
        if (url.toLowerCase().indexOf(item) !== -1) {
            downType = true
        }
    })
    if (downType) {
        link.setAttribute('target', '_self')
        link.setAttribute('href', `${config.baseUrl}/download/file?filePath=${path}&fileName=${fileName}`)
    } else {
        link.setAttribute('target', '_self')
        link.setAttribute('href', url + '?attname=' + fileName)
    }
    link.click()
}

// /**
//  * FTP上传文件
//  * */
// function uploadFileFTP(params, successCallback, errorCallback) {
//     axiosRequest(`/ftp/uploadAttachment`, 'POST', params, successCallback, errorCallback)
// }
//
// /**
//  * FTP下载
//  * */
// function downLoadFTP(params) {
//     let form = document.createElement('form')
//     form.setAttribute('style', 'display:none')
//     form.setAttribute('method', 'post')
//     form.setAttribute('action', `${config.baseFTPUrl}/common/downloadAttachment`) // 下载文件的请求路径
//
//     let input1 = document.createElement('input')
//     input1.setAttribute('type', 'hidden')
//     input1.setAttribute('name', 'affixAddr')
//     input1.setAttribute('value', params.url)
//     let input2 = document.createElement('input')
//     input2.setAttribute('type', 'hidden')
//     input2.setAttribute('name', 'docName')
//     input2.setAttribute('value', params.name)
//     form.append(input1)
//     form.append(input2)
//
//     document.body.append(form)
//     form.submit()
// }

export default {
    uploadFile,
    downLoad
}
