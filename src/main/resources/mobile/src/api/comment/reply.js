/*
* 评论相关接口
* */
import axiosRequest from '@/api/common/common.js'

function add(params, successCallback, errorCallback) {
    axiosRequest(`/reply/add`, 'POST', params, successCallback, errorCallback)
}

export default {
    add,
}
