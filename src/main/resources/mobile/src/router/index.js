import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/',
            component: () => import('@/views/index/index.vue'),
            // redirect: '/query_daily',
            children: [{
                path: '/index',
                name: 'index',
                component: () => import('@/views/add_daily/index.vue'),
            }, {
                path: '/query_daily',
                name: 'query_daily',
                component: () => import('@/views/query_daily/index.vue'),
            }, {
                path: '/daily_statistics',
                name: 'daily_statistics',
                component: () => import('@/views/daily_statistics/index.vue'),
            }]
        }
    ]
})

// 全局路由改变前钩子
router.beforeEach((to, from, next) => {
    // 这里简单地使用localStorage来判断是否登陆
    // if (!window.sessionStorage.getItem('accessToken')) {
    //     if (to.fullPath !== '/login') {
    //         next({ name: 'login', replace: true })
    //     }
    // }
    next()
    // if (sessionStorage.getItem('accessToken') === null) {
    //     let _config
    //     /**
    //      * config 这个参数是在前台的H5文件中定义的，它的值是通过调用后台中封装好的参数来获得的
    //      */
    //     /*
    //     我们需要明白的一点是，所有的这些文件都是放在企业应用的服务器后台，和钉钉网站没有半毛钱的关系
    //     并且钉钉的jsapi中唯一的作用就是提供了对config的验证和获得code值
    //     对于其他值得获取，如access_token,ticket,sign,username,userid都是自己在后台写java代码通过get或者post方式向
    //     钉钉开发平台请求得来的，并不是从jsapi中的接口得来的
    //     */
    //     userApi.getDingConfig({}, data => {
    //         console.log(data)
    //         alert('getDingConfig' + JSON.stringify(data))
    //         _config = data.result
    //         dd.ready(() => {
    //             /*
    //             *获取容器信息，返回值为ability:版本号，也就是返回容器版本
    //             *用来表示这个版本的jsapi的能力，来决定是否使用jsapi
    //             */
    //             dd.runtime.info({
    //                 onSuccess: function(info) {
    //                     logger.e('runtime info: ' + JSON.stringify(info))
    //                 },
    //                 onFail: function(err) {
    //                     logger.e('fail: ' + JSON.stringify(err))
    //                 }
    //             })
    //             /*
    //             *获得免登授权码，需要的参数为corpid，也就是企业的ID
    //             *成功调用时返回onSuccess,返回值在function的参数info中，具体操作可以在function中实现
    //             *返回失败时调用onFail
    //             */
    //             dd.runtime.permission.requestAuthCode({
    //                 corpId: _config.corpId,
    //                 onSuccess: (info) => { // 成功获得code值,code值在info中
    //                     alert('requestAuthCode' + JSON.stringify(info))
    //                     /*
    //                     *$.ajax的是用来使得当前js页面和后台服务器交互的方法
    //                     *参数url:是需要交互的后台服务器处理代码，这里的userinfo对应WEB-INF -> classes文件中的UserInfoServlet处理程序
    //                     *参数type:指定和后台交互的方法，因为后台servlet代码中处理Get和post的doGet和doPost
    //                     *原本需要传输的参数可以用data来存储的，格式为data:{"code":info.code,"corpid":config.corpid}
    //                     *其中success方法和error方法是回调函数，分别表示成功交互后和交互失败情况下处理的方法
    //                     */
    //                     // userinfo为本企业应用服务器后台处理程序
    //                     alert('authCode:' + info.code)
    //                     userApi.dingLogin({
    //                         authCode: info.code
    //                     }, data => {
    //                         alert('dingLogin' + JSON.stringify(data))
    //                         sessionStorage.setItem('userName', data.name)
    //                         sessionStorage.setItem('trueName', data.trueName)
    //                         sessionStorage.setItem('deptId', data.deptId)
    //                         sessionStorage.setItem('accessToken', data.accessToken)
    //                         // alert("username： " + map.username);
    //                         next()
    //                     }, error => {
    //                         alert('登录失败，请联系系统服务人员。' + JSON.stringify(error))
    //                     })
    //                 },
    //                 onFail: function(err) { // 获得code值失败
    //                     alert('fail: ' + JSON.stringify(err))
    //                 }
    //             })
    //         })
    //         /*
    //         * 在dd.config函数验证没有通过下执行这个函数
    //         */
    //         dd.error(function(err) {
    //             alert('dd error: ' + JSON.stringify(err))
    //         })
    //     }, (error) => {
    //         console.log(error)
    //         alert('error111: ' + JSON.stringify(error))
    //     })
    //     // if (to.path !== '/login' && to.path !== '/404' && to.path !== '/') {
    //     //     // 检查菜单权限中有没有对应的url
    //     //     if (checkMenus(to.path)) {
    //     //         next()
    //     //     } else {
    //     //         // 没权限跳转到404页面，或执行其他操作
    //     //         next({ name: '404', replace: true })
    //     //     }
    //     // } else {
    //     //     next()
    //     // }
    // } else {
    //     next()
    // }
})

export default router
