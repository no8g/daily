// config
import axios from 'axios' // 引入axios
// import config from '../../build/url.js'
// import userApi from '@/api/user/user'

var instance = axios.create({
    async: true,
    crossDomain: true,
})

instance.defaults.withCredentials = true // 设置cross跨域，并设置访问权限，允许跨域
instance.defaults.crossDomain = true
instance.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8' // 设置post请求头
// instance.defaults.baseURL = config.baseUrl // 设置axios的默认请求地址
instance.defaults.timeout = 6000000 // 请求超时响应
instance.interceptors.response.use(response => {
    // switch (response.data.code) {
    // case 320107:
    //     if (window.sessionStorage.getItem('accessToken')) {
    //         window.sessionStorage.removeItem('accessToken')
    //     }
    //     window.location.href = '/#/login'
    //     // window.location.href = '/logout'
    //     break
    // case 320108:
    //     if (window.sessionStorage.getItem('accessToken')) {
    //         window.sessionStorage.removeItem('accessToken')
    //     }
    //     window.location.href = '/#/login'
    //     // window.location.href = '/logout'
    //     break
    // }
    return response
})

// 添加请求拦截器
instance.interceptors.request.use(config => {
// 在发送请求之前做些什么
    if (window.localStorage.getItem('accessToken')) {
        config.headers.common['accessToken'] = window.localStorage.getItem('accessToken')
    }
    // config.headers.common['accessToken'] = 'e36f505d70d34cce94878683e7155ca8'
    return config
}, error => {
// 对请求错误做些什么
    return Promise.reject(error)
})

export default {
    axios: instance
}
