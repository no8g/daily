// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import utils from '@/utils/index.js'
import 'vant/lib/index.css'
import {
    Button,
    Image as VanImage,
    Col,
    Row,
    Cell,
    Popup,
    Toast,
    Calendar,
    DatetimePicker,
    Field,
    Form,
    DropdownMenu,
    DropdownItem,
    PullRefresh,
    Collapse,
    CollapseItem,
    Empty,
    List,
    Skeleton,
    Sticky,
    Tab,
    Tabs,
    Tabbar,
    TabbarItem,
    Loading,
    IndexBar,
    IndexAnchor,
    Pagination,
    Dialog,
    Icon,
    Badge,
    Notify,
    Picker,
    Slider
} from 'vant'
import './assets/css/common.css'
import Router from 'vue-router'
import store from './store'

const originalPush = Router.prototype.push

Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

Vue.config.productionTip = false
Vue.prototype.$utils = utils
Vue.prototype.$EventBus = new Vue() // $EventBus
Vue.use(Button)
Vue.use(VanImage)
Vue.use(Col)
Vue.use(Row)
Vue.use(Cell)
Vue.use(Popup)
Vue.use(Toast)
Vue.use(Calendar)
Vue.use(DatetimePicker)
Vue.use(Field)
Vue.use(Form)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.use(PullRefresh)
Vue.use(Collapse)
Vue.use(CollapseItem)
Vue.use(Empty)
Vue.use(List)
Vue.use(Skeleton)
Vue.use(Sticky)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(Loading)
Vue.use(IndexBar)
Vue.use(IndexAnchor)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Icon)
Vue.use(Badge)
Vue.use(Notify)
Vue.use(Picker)
Vue.use(Slider)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    template: '<App/>'
})
