// initial state
const state = {
    menu: {},
    userInfo: {
        department: '',
        userName: '',
        trueName: ''
    },
    trueName: '',
    tagsList: [],
    authButton: {}
}

// getters
const getters = {
    isInCharge: (state, getters, rootState) => {
        return state.isInCharge
    },
}

// actions
const actions = {
    getUserInfo({ commit }) {
        return new Promise((resolve, reject) => {
            commit('setUserInfo', {
                userInfo: {
                    userName: localStorage.getItem('userName'),
                    trueName: localStorage.getItem('trueName'),
                    department: localStorage.getItem('deptId'),
                }
            })
            resolve(data)
        })
    },
}

// mutations
const mutations = {
    setUserInfo(state, data) {
        state.userInfo = data.userInfo
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
