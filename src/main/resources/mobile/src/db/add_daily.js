const db = (function() {
    let request
    let _DB
    const version = 2
    return {
        ready(callback = () => {}, error = () => {}) {
            request = window.indexedDB.open('UIOTDailyDB', version)
            request.onerror = function(event) {
                console.log('数据库打开报错')
                error()
            }
            request.onsuccess = function(event) {
                console.log('数据库打开成功')
                _DB = event.target.result
                callback()
            }
            // 版本不一致回调
            request.onupgradeneeded = function(event) {
                console.log('初始化')
                const db = event.target.result
                // 新增一张叫做addFlow的表格，主键是flowChartId
                let objectStore = db.createObjectStore('addDaily', { keyPath: 'id' })
                objectStore.createIndex('日报添加内容', 'dailyAddDTO', { unique: false })
                // callback()
            }
            request.onversionchange = function(event) {
                request.close()
                alert('数据库版本不一致，请重新刷新')
            }
        },
        // 获取实时保存的添加流程内容
        getInfo(id, callback = () => {}, error = () => {}) {
            // console.log('获取实时保存', id)
            let transaction = _DB.transaction('addDaily', 'readwrite')
            let objectStore = transaction.objectStore('addDaily')
            // objectStore.get()方法用于读取数据，参数是主键的值
            let _request = objectStore.get(id)

            _request.onerror = function(event) {
                console.log('事务失败')
                error()
            }

            _request.onsuccess = function(event) {
                let data
                if (_request.result) {
                    // console.log('result: ', _request.result)
                    data = _request.result.dailyAddDTO || {}
                } else {
                    console.log('未获得数据记录')
                }
                callback(data)
            }
        },
        // 设置实时保存的添加流程内容
        insert(params, callback = () => {}, error = () => {}) {
            // console.log('insert', params)
            this.getInfo(params.id, (result) => {
                let _request
                // 没有就插入，有就修改
                if (result === undefined) {
                    _request = _DB.transaction(['addDaily'], 'readwrite')
                        .objectStore('addDaily')
                        .add({ id: params.id, dailyAddDTO: params.dailyAddDTO })
                } else {
                    // console.log(params.id)
                    // put()方法自动更新了主键为1的记录
                    _request = _DB.transaction(['addDaily'], 'readwrite')
                        .objectStore('addDaily')
                        .put({ id: params.id, dailyAddDTO: params.dailyAddDTO })
                }
                _request.onsuccess = function(event) {
                    // console.log('实时保存成功')
                    callback()
                }

                _request.onerror = function(event) {
                    console.log('实时保存失败')
                    error()
                }
            })
        },
        delete(id, callback = () => {}, error = () => {}) {
            let _request = _DB.transaction(['addDaily'], 'readwrite')
                .objectStore('addDaily')
                .delete(id)

            _request.onsuccess = function(event) {
                callback()
                console.log('删除成功')
            }

            _request.onerror = function(event) {
                console.log('实时保存失败')
                error()
            }
        },
        close() {
            console.log('关闭数据库')
            _DB.close()
            _DB = null
        }
    }
})()
export default db
