package com.uiotsoft.daily.organization.web;

import com.uiotsoft.daily.common.constant.NumberConstants;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.entity.DailyRoleConfig;
import com.uiotsoft.daily.module.service.DailyRoleConfigService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * UserController 控制器
 *
 * @author lizy
 * @date 2020-07-04
 */
@Slf4j
@Api(value = "user", tags = "用户相关接口")
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Resource
    private DailyRoleConfigService dailyRoleConfigService;

    @ApiOperation(value = "获取当前登录用户")
    @GetMapping(value = "getUser")
    public JsonResult getLoginUser() {

        User user = getUser();
        if (user == null) {
            log.error("E|UserController|getLoginUser()|获取当前登录人为空！");
            return JsonResult.fail("获取当前登录人为空");
        }
        String userName = user.getUserName();
        String trueName = user.getTrueName();
        DailyRoleConfig dailyRoleConfig = dailyRoleConfigService.getDailyRoleConfigByUsername(userName);
        if (dailyRoleConfig != null) {
            user.setRoleRank(dailyRoleConfig.getRoleRank());
        } else {
            user.setRoleRank(NumberConstants.NUMBER_THREE);
        }
        log.info("E|UserController|getLoginUser()|当前登录用户 = 【手机号 = {}, 姓名 = {}】", userName, trueName);

        return JsonResult.ok(user);
    }
}