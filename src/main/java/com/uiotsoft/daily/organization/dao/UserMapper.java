package com.uiotsoft.daily.organization.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.organization.entity.User;
import org.springframework.stereotype.Repository;

/**
 * UserMapper
 * @author lizy
 * @date 2020-07-04
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户手机号查询当前用户
     *
     * @param username 用户手机号
     * @return 查询当前用户
     */
    User getUserByUsername(String username);
}