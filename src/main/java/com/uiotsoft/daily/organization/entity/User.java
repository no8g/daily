/**
 * @(#)User.java 2018年4月11日 下午5:00:36
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.organization.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * <p>User.java此类用于用户实体类</p>
 * <p>@author:wangyk</p>
 * <p>@date:2018年4月11日</p>
 * <p>@remark:</p>
 */
@ApiModel(value = "用户实体类")
@Data
@ToString
public class User implements Serializable {

    /**
     * 序列化的版本号
     */
    private static final long serialVersionUID = -130291590642515275L;

    @ApiModelProperty(value = "用户ID")
    private Integer userId;

    @ApiModelProperty(value = "用户钉钉ID")
    private String dingUserId;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "用户真实姓名")
    private String trueName;

    @ApiModelProperty(value = "手机号码")
    private String handset;

    @ApiModelProperty(value = "代理商SID")
    private String agentSid;

    @ApiModelProperty(value = "代理商SID")
    @TableField(exist = false)
    private Integer roleRank;

    /**
     * 密码
     */
    private String password;

    /**
     * 角色Id
     */
    private Integer roleId;

    /**
     * 创建日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    /**
     * 邮箱
     */
    private String orgEmail;

    /**
     * 是否领导者
     */
    private String isLeader;

    /**
     * 职位
     */
    private String position;

    /**
     * 登陆的次数
     */
    private Integer loginCount;

    /**
     * 最后登陆的日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginLastDate;

    /**
     * 登陆ip
     */
    private String loginLastIp;

    /**
     * 密码修改日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date pwdLastDate;

    /**
     * 状态
     */
    private Integer userState;

    /**
     * 是否管理员，默认不是管理员N，Y是管理员
     */
    private String isAdmin;

    /**
     * 用户级别
     */
    private Integer userLevel;

    /**
     * 是否接收报警 默认N
     */
    private String isAlarm;

    /**
     * 钉钉头像
     */
    private String headPortrait;

    /**
     * 部门
     */
    private String department;

    /**
     * 部门父id
     */
    private String fid;

    /**
     * 是否勾选自动登录
     */
    private Boolean autoLogin;

    /**
     * 角色编码
     */
    private Set<String> roleCode;

    /**
     * 角色编码字符串
     */
    private String roleCodeStr;
}
