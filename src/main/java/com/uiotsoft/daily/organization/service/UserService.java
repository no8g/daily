package com.uiotsoft.daily.organization.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.uiotsoft.daily.common.domain.vo.PageResult;
import com.uiotsoft.daily.organization.entity.User;

import java.util.Map;

/**
 * UserService
 * @author lizy
 * @date 2020-07-04
 */
public interface UserService extends IService<User> {

    /**
     * 分页查询
     * @param page
     * @param params
     * @return
     */
    PageResult<User> listByPage(Page<User> page, Map<String, Object> params);

    /**
     * 根据手机号获取用户信息
     * @param username
     * @return
     */
    User getUser(String username);
}