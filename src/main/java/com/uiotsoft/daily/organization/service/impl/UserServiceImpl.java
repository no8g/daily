package com.uiotsoft.daily.organization.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uiotsoft.daily.common.domain.vo.PageResult;
import com.uiotsoft.daily.common.exception.MisErrorCode;
import com.uiotsoft.daily.common.exception.MisException;
import com.uiotsoft.daily.organization.dao.UserMapper;
import com.uiotsoft.daily.organization.entity.User;
import com.uiotsoft.daily.organization.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * UserServiceImpl接口实现
 * @author lizy
 * @date 2020-07-04
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 分页查询
     * @param page
     * @param params
     * @return
     */
    @Override
    public PageResult listByPage(Page<User> page, Map<String, Object> params) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();

        String userName = MapUtil.get(params, "userName", String.class);
        if (StrUtil.isBlank(userName)){
            queryWrapper.like("USERNAME", userName);
        }

        String agentSid = MapUtil.get(params, "agentSid", String.class);
        if (StrUtil.isBlank(agentSid)){
            queryWrapper.likeRight("AGENT_SID", agentSid);
        }

        IPage<User> pageList = userMapper.selectPage(page, queryWrapper);
        return new PageResult(pageList.getRecords(),pageList.getTotal());
    }

    /**
     * 根据手机号获取用户信息
     * @param userName
     * @return
     */
    @Override
    public User getUser(String userName) {
        if (StrUtil.isBlank(userName)){
            throw new MisException(MisErrorCode.E_200100.getErrorCode(), MisErrorCode.E_200100.getErrorMsg());
        }
        return  userMapper.getUserByUsername(userName);
    }

}