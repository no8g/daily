package com.uiotsoft.daily.task;

import cn.hutool.core.date.DateUtil;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.dingding.domain.DingTalkMsg;
import com.uiotsoft.daily.dingding.util.DingDingMsg;
import com.uiotsoft.daily.dingding.util.DingTalkWorkNotice;
import com.uiotsoft.daily.module.dao.DailyViewUserMapper;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import com.uiotsoft.daily.module.service.DailyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>NoSubmitInformTask 此类用于：定时任务 未提交日报钉钉通知</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年08月10日 17:34</p>
 * <p>@remark：暂定晚上22点提醒 截止到22点时没有提交日报的人员</p>
 */
@Slf4j
@Component
public class NoSubmitInformTask {

    @Resource
    private DailyService dailyService;

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Resource
    private DingTalkWorkNotice dingTalkWorkNotice;

    @Async
    @Scheduled(cron = "${noSubmitInformTime}")
    public void dingInformNoSubmit() {
        // 1、查询截止到22点已提交日报的人员手机号列表
        // 2、查询研发中心应该提交日报的人员列表
        // 3、得到（步骤2 - 步骤1）中的人员列表（未提交的人员列表）
        // 4、钉钉通知

        // 1、查询截止到22点已提交日报的人员手机号列表
        List<String> usernameList = dailyService.selectUsernameListByCreateTime();

        // 2、查询研发中心应该提交日报的人员列表
        List<DailyViewUser> dailyViewUserList = dailyViewUserMapper.selectListLikeDeptId(CommonConstants.RD_CENTER_DEPT_ID, null);

        CopyOnWriteArrayList<DailyViewUser> noSubmitUserList = new CopyOnWriteArrayList<>(dailyViewUserList);

        // 3、得到（步骤2 - 步骤1）中的人员列表（未提交的人员列表）noSubmitUserList
        for (String username : usernameList) {
            noSubmitUserList.removeIf(dailyViewUser -> username.equals(dailyViewUser.getUserName()));
        }

        // 4、钉钉通知
        sendDingInform(noSubmitUserList);
    }

    /**
     * 发送钉钉通知
     *
     * @param noSubmitUserList 需要提醒的人员列表
     */
    private void sendDingInform(CopyOnWriteArrayList<DailyViewUser> noSubmitUserList) {

        for (DailyViewUser dailyViewUser : noSubmitUserList) {
            DingTalkMsg dingTalkMsg = new DingTalkMsg();
            dingTalkMsg.setAuthor(DateUtil.format(new Date(), "yyyy年MM月dd日"));
            dingTalkMsg.setTitle("日报未提交通知");
            dingTalkMsg.setMsgType("oa");

            String dingUserId = dailyViewUser.getDingUserId();
            dingTalkMsg.setUserList(Objects.isNull(dingUserId) ? dailyViewUser.getUserName() : dingUserId);
            dingTalkMsg.setFromList(getFormList());
            dingTalkWorkNotice.sendDingMsg(dingTalkMsg);
        }
    }

    /**
     * 封装FormList
     *
     * @return 封装FormList
     */
    private List<OapiMessageCorpconversationAsyncsendV2Request.Form> getFormList() {

        List<OapiMessageCorpconversationAsyncsendV2Request.Form> formList =
                DingDingMsg.create("【UIOT超级智慧家（日报服务）】提醒您，截止到目前为止，您还未提交【",
                        DateUtil.format(new Date(), "yyyy-MM-dd") + "】当天的日报，请确认！")
                        .builder("日报服务提醒时间：", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"))
                        .collect();

        log.info("E|dingInformNoSubmit|getFormList()|钉钉提醒日报未提交时，封装钉钉通知内容 = {}", formList);

        return formList;
    }
}
