package com.uiotsoft.daily.task;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.uiotsoft.daily.common.util.MicroServiceClient;
import com.uiotsoft.daily.module.dao.DailyMailContentMapper;
import com.uiotsoft.daily.module.dao.DailyNoSubmitReportMapper;
import com.uiotsoft.daily.module.dao.DailyViewUserMapper;
import com.uiotsoft.daily.module.entity.DailyMailContent;
import com.uiotsoft.daily.module.entity.DailyNoSubmitReport;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import com.uiotsoft.daily.module.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * <p>MailNoSubmitTask 此类用于：统计邮件日报未发送者</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月24日 19:32</p>
 * <p>@remark：日报邮件截止时间是第二天上午11点，周六的是周一上午11点</p>
 */
@Slf4j
@Component
public class MailNoSubmitTask {

    @Resource
    private MicroServiceClient microServiceClient;

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Resource
    private DailyMailContentMapper dailyMailContentMapper;

    @Resource
    private DailyNoSubmitReportMapper dailyNoSubmitReportMapper;

    @Resource
    private TaskService taskService;

    @Value("${unified.dailyMailSender}")
    private String dailyMailSender;

    @Value("${foxmail.host}")
    private String mailHost;

    @Value("${foxmail.username}")
    private String mailUsername;

    @Value("${foxmail.password}")
    private String mailPassword;

    @Value("${dailyMailNoSubmitDeadlineTime}")
    private String dailyMailNoSubmitDeadlineTime;

    @Value("${dailyMailFewDaysAgo}")
    private String dailyMailFewDaysAgo;

    @Async
    @Scheduled(cron = "${dailyMailNoSubmitDeadline}")
    public void queryEmailDailyNoSubmitSync() {

        try {
            Properties props = new Properties();
            Session session = Session.getDefaultInstance(props, null);
            Store store = session.getStore("imap");
            store.connect(mailHost, mailUsername, mailPassword);

            Folder folder = store.getFolder("INBOX");
            folder.open(Folder.READ_ONLY);

            Calendar calendar = Calendar.getInstance();
            // 搜索几天前到今天收到的的所有邮件，根据时间筛选邮件
            calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(dailyMailFewDaysAgo));
            ReceivedDateTerm term = new ReceivedDateTerm(ComparisonTerm.GT, new Date(calendar.getTimeInMillis()));
            Message[] message = folder.search(term);
            log.info("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|查询到的收件箱里的邮件列表总数量【邮件总数量 = {}】", message.length);

            // 收件箱里所有邮件 Message[] message = folder.getMessages();
            // 1、根据角色得到手机号列表
            // 2、根据截止时间得到本次已发送邮件的列表
            // 3、根据邮件列表查询出已通过邮件写日报的人员邮箱列表
            // 4、再根据邮箱列表查询出手机号列表
            // 5、从步骤1中减去步骤4中的人员，得到没有发送邮件的手机号差集
            // 6、根据手机号差集得到人员列表
            // 7、保存步骤6中的人员
            // 8、保存步骤2中的邮件

            // 步骤1、根据角色得到手机号列表
            List<String> usernameListByRoleCode = this.getUsernameListByRoleCode();
            log.info("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|根据角色得到手机号列表，【usernameListByRoleCode 数量 = {}，usernameListByRoleCode 列表 = {}】", usernameListByRoleCode.size(), usernameListByRoleCode);

            // 步骤2、根据发送时间得到本次已发送邮件的列表
            List<Message> thisTimeSubmitDailyList = this.getThisTimeSubmitDailyList(message);
            log.info("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|根据截止时间得到本次已发送邮件的列表，【thisTimeSubmitDailyList 邮件数量 = {}，thisTimeSubmitDailyList 邮件列表 = {}】", thisTimeSubmitDailyList.size(), thisTimeSubmitDailyList);

            // 步骤3、根据邮件列表查询出已通过邮件写日报的人员邮箱列表
            List<String> sentEmailList = this.getSentEmailList(thisTimeSubmitDailyList);
            log.info("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|根据邮件列表查询出已通过邮件写日报的人员邮箱列表，【sentEmailList 数量 = {}，sentEmailList 列表 = {}】", sentEmailList.size(), sentEmailList);

            // 步骤4、再根据邮箱列表查询出手机号列表
            List<String> sentEmailUsernameList = this.getSentUsernameList(sentEmailList);
            log.info("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|再根据邮箱列表查询出手机号列表，【sentEmailUsernameList 数量 = {}，sentEmailUsernameList 列表 = {}】", sentEmailUsernameList.size(), sentEmailUsernameList);

            // 步骤5、从步骤1中减去步骤4中的人员，得到没有发送邮件的手机号差集
            Collection noSendUsernameList = CollectionUtils.subtract(usernameListByRoleCode, sentEmailUsernameList);
            log.info("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|得到没有发送邮件的手机号差集，【noSendUsernameList 数量 = {}，noSendUsernameList 列表 = {}】", noSendUsernameList.size(), noSendUsernameList);

            // 步骤6、根据手机号差集得到人员列表
            List<DailyViewUser> noSendDailyUserList = this.getNoSendDailyUserList(noSendUsernameList);
            log.info("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|根据手机号差集得到人员列表，【noSendDailyUserList 数量 = {}，noSendDailyUserList 列表 = {}】", noSendDailyUserList.size(), noSendDailyUserList);

            // 步骤7、保存步骤6中的人员
            this.saveNoSubmitReport(noSendDailyUserList);

            // 步骤8、保存步骤2中的邮件
            this.saveDailyMailContent(thisTimeSubmitDailyList);
        } catch (Exception e) {
            log.error("E|MailNoSubmitTask|queryEmailDailyNoSubmitSync()|定时扫描邮件日报未发送人员列表失败，【原因 = {}】", e.getMessage());
        }
    }

    /**
     * 根据手机号差集得到人员列表
     *
     * @param noSendUsernameList 手机号差集
     * @return 得到人员列表
     */
    public List<DailyViewUser> getNoSendDailyUserList(Collection noSendUsernameList) {

        List<DailyViewUser> dailyViewUserList = new ArrayList<>();
        for (Object username : noSendUsernameList) {
            if (!"".equals(username)) {
                DailyViewUser dailyViewUser = dailyViewUserMapper.getDailyViewUserByUsername(username.toString());
                if (dailyViewUser != null) {
                    dailyViewUserList.add(dailyViewUser);
                }
            }
        }

        return dailyViewUserList;
    }

    /**
     * 再根据邮箱列表查询出手机号列表
     *
     * @param sentEmailList 邮箱列表
     * @return 手机号列表
     */
    public List<String> getSentUsernameList(List<String> sentEmailList) {

        List<DailyViewUser> dailyViewUserList = new ArrayList<>();
        try {
            for (String email : sentEmailList) {
                if (!"".equals(email)) {
                    DailyViewUser dailyViewUser = dailyViewUserMapper.getDailyViewUserByEmail(email);
                    if (dailyViewUser != null) {
                        dailyViewUserList.add(dailyViewUser);
                    }
                }
            }
            log.info("E|MailNoSubmitTask|getSentUsernameList()|根据邮箱查询用户详情成功！");
        } catch (Exception e) {
            log.error("E|MailNoSubmitTask|getSentUsernameList()|根据邮箱查询用户详情失败！【原因 = {}】", e.getMessage());
        }

        List<String> usernameList = dailyViewUserList.stream().map(DailyViewUser::getUserName).collect(Collectors.toList());
        log.info("E|MailNoSubmitTask|getSentUsernameList()|根据用户详情列表过滤手机号列表成功！");

        return usernameList;
    }

    /**
     * 得到已经发送邮件的邮箱列表
     *
     * @param thisTimeSubmitDailyList 本次已经发送邮件列表
     * @return 得到已经发送邮件的邮箱列表
     */
    public List<String> getSentEmailList(List<Message> thisTimeSubmitDailyList) {

        List<String> emailList = new ArrayList<>();
        try {
            for (Message message : thisTimeSubmitDailyList) {
                ShowMail showMail = new ShowMail((MimeMessage) message);
                String fromAddr = showMail.getFromAddr();
                emailList.add(fromAddr);
            }
        } catch (Exception e) {
            log.error("E|MailNoSubmitTask|getSentEmailList()|根据邮件得到邮箱列表失败，【原因 = {}】", e.getMessage());
        }

        return emailList;
    }

    /**
     * 根据角色获取到需要发送邮件写日报的人员手机号列表
     *
     * @return 需要发送邮件写日报的人员手机号列表
     */
    private List<String> getUsernameListByRoleCode() {
        // 根据角色获取到需要发送邮件写日报的人员手机号列表
        List<String> usernameList = new ArrayList<>();
        try {
            usernameList = microServiceClient.authorizationByRoleCode(dailyMailSender);
            log.info("E|MailNoSubmitTask|getUsernameListByRoleCode()|根据角色获取到需要发送邮件写日报的人员手机号列表成功！");
        } catch (Exception e) {
            log.error("E|MailNoSubmitTask|getUsernameListByRoleCode()|根据角色获取到需要发送邮件写日报的人员手机号列表失败！，【失败原因 = {}】", e.getMessage());
        }

        return usernameList;
    }

    /**
     * 获取昨天提交的日报邮件列表
     *
     * @param messageArray 所有邮件列表
     * @return 昨天提交的日报邮件列表
     */
    private List<Message> getThisTimeSubmitDailyList(Message[] messageArray) {

        List<Message> thisTimeMessageList = new ArrayList<>();

        try {
            for (Message message : messageArray) {
                ShowMail re = new ShowMail((MimeMessage) message);
                re.setDateFormat("yyyy-MM-dd HH:mm:ss");
                String sentDate = re.getSentDate();
                // 日报邮件截止时间是第二天上午11点，周六的是周一上午11点
                // 得到今天上午11点钟，定时任务开始时间
                Date todayDate = new Date();
                String format = DateUtil.format(todayDate, dailyMailNoSubmitDeadlineTime);
                // 将今天上午11点转为DateTime类型的
                DateTime todayDeadlineTime = DateUtil.parse(format);
                // 得到昨天上午11点钟，定时任务开始时间减去24小时
                DateTime yesterdayDeadlineTime = DateUtil.offsetHour(todayDeadlineTime, -24);
                // 前天上午11点钟
                DateTime theDayBeforeYesterdayDeadlineTime = DateUtil.offsetHour(todayDeadlineTime, -48);
                // 邮件发送时间
                DateTime sentDateTime = DateUtil.parse(sentDate);

                DateTime deadlineTime;
                // 查看昨天上午11点到到今天上午11点之间的日报
                int dayOfWeek = DateUtil.dayOfWeek(todayDate);
                if (dayOfWeek == 2) {
                    deadlineTime = theDayBeforeYesterdayDeadlineTime;
                } else {
                    deadlineTime = yesterdayDeadlineTime;
                }
                // 把昨天截止时间之前发送的日报全部删除，得到今天发送日报的所有邮件
                if (sentDateTime.isBefore(todayDeadlineTime) && sentDateTime.isAfter(deadlineTime)) {
                    thisTimeMessageList.add(message);
                }
            }
        } catch (Exception e) {
            log.error("E|MailNoSubmitTask|getThisTimeSubmitDailyList()|定时任务，获取昨天提交的日报邮件列表异常，【原因 = {}】", e.getMessage());
        }

        return thisTimeMessageList;
    }

    /**
     * 根据邮箱查询人员列表
     *
     * @param allOrgEmailList 邮箱列表
     * @return 人员列表
     */
    private List<DailyViewUser> getDailyViewUserListByEmailList(CopyOnWriteArrayList<String> allOrgEmailList) {

        // 去掉空的邮箱
        CopyOnWriteArrayList<String> nonNullEmailList = new CopyOnWriteArrayList<>();
        for (String email : allOrgEmailList) {
            if (!"".equals(email)) {
                nonNullEmailList.add(email);
            }
        }

        List<DailyViewUser> viewUserList = new ArrayList<>();
        if (nonNullEmailList.size() > 0) {
            viewUserList = dailyViewUserMapper.selectListByEmailList(nonNullEmailList);
        }

        return viewUserList;
    }

    /**
     * 保存日报未提交记录
     *
     * @param viewUserList 未提交日报人员信息列表
     */
    private void saveNoSubmitReport(List<DailyViewUser> viewUserList) {

        List<DailyNoSubmitReport> dailyNoSubmitReportList = new ArrayList<>();
        for (DailyViewUser dailyViewUser : viewUserList) {
            DailyNoSubmitReport dailyNoSubmitReport = new DailyNoSubmitReport();
            dailyNoSubmitReport.setUserName(dailyViewUser.getUserName());
            dailyNoSubmitReport.setTrueName(dailyViewUser.getTrueName());
            dailyNoSubmitReport.setDeptId(dailyViewUser.getAgentSid());
            dailyNoSubmitReport.setDeptName(dailyViewUser.getAgentName());
            // 1、研发人员，2、邮箱人员
            dailyNoSubmitReport.setUserType(2);
            Date today = new Date();
            int dayOfWeek = DateUtil.dayOfWeek(today);
            if (dayOfWeek == 2) {
                // 如果今天是周一，那么保存未提交日期为上周六
                dailyNoSubmitReport.setNoSubmitDate(DateUtil.offsetHour(today, -48));
                // 如果今天是周一，那么保存未提交周几为上星期六
                dailyNoSubmitReport.setDayOfWeek(taskService.getDayOfWeek(7));
            } else {
                // 保存未提交日期为昨天
                dailyNoSubmitReport.setNoSubmitDate(DateUtil.offsetHour(today, -24));
                // 保存未提交未提交周几为昨天
                dailyNoSubmitReport.setDayOfWeek(taskService.getDayOfWeek(dayOfWeek - 1));
            }

            dailyNoSubmitReport.setCreateTime(today);
            dailyNoSubmitReportList.add(dailyNoSubmitReport);
        }

        try {
            log.info("E|MailNoSubmitTask|saveNoSubmitReport()|保存邮件未发送记录数量，【数量 = {}，记录 = {}】", dailyNoSubmitReportList.size(), dailyNoSubmitReportList);
            if (dailyNoSubmitReportList.size() > 0) {
                dailyNoSubmitReportMapper.insertBatch(dailyNoSubmitReportList);
            }
        } catch (Exception e) {
            log.error("E|MailNoSubmitTask|saveNoSubmitReport()|保存日报未提交记录失败时，【原因 = {}】", e.getMessage());
        }
    }

    /**
     * 保存本次需要保存的邮件内容
     *
     * @param thisTimeSubmitDailyList 本次需要保存的邮件内容
     */
    private void saveDailyMailContent(List<Message> thisTimeSubmitDailyList) {

        try {
            for (Message messageInfo : thisTimeSubmitDailyList) {
                ShowMail re = new ShowMail((MimeMessage) messageInfo);
                DailyMailContent dailyMailContent = new DailyMailContent();
                dailyMailContent.setMailId(re.getMessageId());
                dailyMailContent.setCarbonCopyAddr(re.getMailAddress("cc"));
                dailyMailContent.setMailSubject(re.getSubject());
                dailyMailContent.setMailBodyContext(re.getBodyText());
                dailyMailContent.setCreateTime(new Date());
                re.setDateFormat("yyyy年MM月dd日 HH:mm:ss");
                dailyMailContent.setMailSendTime(DateUtil.parse(re.getSentDate()));
                dailyMailContent.setFromAddr(re.getFromAddr());

                // 获取邮箱的人员姓名
                CopyOnWriteArrayList<String> emailList = new CopyOnWriteArrayList<>(Collections.singletonList(re.getFromAddr()));
                List<DailyViewUser> listByEmailList = this.getDailyViewUserListByEmailList(emailList);
                if (listByEmailList.size() > 0) {
                    dailyMailContent.setFromName(listByEmailList.get(0).getTrueName());
                } else {
                    dailyMailContent.setFromName(re.getFromName());
                }

                boolean containAttach = re.isContainAttach((Part) messageInfo);
                if (containAttach) {
                    dailyMailContent.setIsContainAttach(1);
                } else {
                    dailyMailContent.setIsContainAttach(0);
                }
                boolean replySign = re.getReplySign();
                if (replySign) {
                    dailyMailContent.setIsReply(1);
                } else {
                    dailyMailContent.setIsReply(0);
                }
                dailyMailContent.setToAddr(re.getMailAddress("to"));
                dailyMailContent.setSecretCarbonCopyAddr(re.getMailAddress("bcc"));
                String messageId = re.getMessageId();
                List<DailyMailContent> mailContentList = dailyMailContentMapper.selectList(Wrappers.<DailyMailContent>lambdaQuery().eq(DailyMailContent::getMailId, messageId));
                if (mailContentList.size() == 0) {
                    // 保存每封邮件内容
                    dailyMailContentMapper.insert(dailyMailContent);
                } else {
                    log.info("E|MailNoSubmitTask|saveDailyMailContent()|邮件内容已存在，邮件mailId = {}", messageId);
                }
            }
        } catch (Exception e) {
            log.error("E|MailNoSubmitTask|saveDailyMailContent()|定时扫描邮件日报未发送人员列表失败，【原因 = {}】", e.getMessage());
        }
    }
}
