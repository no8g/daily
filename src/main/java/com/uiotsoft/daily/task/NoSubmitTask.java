package com.uiotsoft.daily.task;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.uiotsoft.daily.module.entity.DailyHolidayConfig;
import com.uiotsoft.daily.module.entity.HolidayRawInfo;
import com.uiotsoft.daily.module.service.DailyHolidayConfigService;
import com.uiotsoft.daily.module.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>NoSubmitTask 此类用于：定时任务</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月16日 17:10</p>
 * <p>@remark：</p>
 */
@Slf4j
@Component
public class NoSubmitTask {

    @Resource
    private TaskService taskService;

    @Resource
    private DailyHolidayConfigService holidayConfigService;

    @Value("${syncAddress}")
    private String syncAddress;

    @Async
    @Scheduled(cron = "${dailyNoSubmitDeadline}")
    public void queryDailyNoSubmitSync() {
        int dayOfWeek = DateUtil.dayOfWeek(new Date());
        log.info("每天24:01分，定时查询昨天日报未提交人员开始时间 = {}，今天是【星期{}】", DateUtil.formatDateTime(new Date()), dayOfWeek - 1);

        try {
            taskService.queryDailyNoSubmitSync();
        } catch (Exception e) {
            log.error("E|NoSubmitTask|queryDailyNoSubmitSync()|每天24:01分，今天是【星期{}】，定时查询当天日报未提交人员任务异常：{}", dayOfWeek - 1, e.getMessage());
        }

        log.info("每天24:01分，定时查询昨天日报未提交人员完成时间 = {}，今天是【星期{}】", DateUtil.formatDateTime(new Date()), dayOfWeek - 1);
    }

    @Async
    @Scheduled(cron = "${syncHolidayDeadline}")
    public void syncHoliday() {

        log.info("每年12月28凌晨1点定时同步下一年的节假日信息，同步节假日开始时间 = {}", DateUtil.formatDateTime(new Date()));

        String url = syncAddress;
        OkHttpClient client = new OkHttpClient();
        Response response;
        //解密数据
        String rsa = null;
        Request request = new Request.Builder().url(url).get()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        try {
            response = client.newCall(request).execute();
            rsa = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map map = JSONObject.parseObject(rsa, Map.class);
        if (map != null) {
            Integer code = (Integer) map.get("code");
            if (code == 0) {
                JSONObject holidayJson = (JSONObject) map.get("holiday");
                String jsonString = holidayJson.toJSONString();
                log.info("获取节假日数据内容为 jsonString = 【{}】", jsonString);
                Set<Map.Entry<String, Object>> entrySet = holidayJson.entrySet();
                List<HolidayRawInfo> rawInfoList = new ArrayList<>();
                for (Map.Entry<String, Object> entry : entrySet) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    cn.hutool.json.JSONObject jsonObject = JSONUtil.parseObj(value);
                    HolidayRawInfo holidayRawInfo = JSONUtil.toBean(jsonObject, HolidayRawInfo.class);
                    rawInfoList.add(holidayRawInfo);
                }
                // 定义节假日集合
                List<DailyHolidayConfig> holidayConfigList = new ArrayList<>();
                for (HolidayRawInfo holidayRawInfo : rawInfoList) {
                    DailyHolidayConfig holidayConfig = new DailyHolidayConfig();
                    holidayConfig.setHolidayTarget(holidayRawInfo.getTarget());
                    holidayConfig.setHolidayAfter(holidayRawInfo.getAfter());
                    holidayConfig.setHolidayDate(holidayRawInfo.getDate());
                    holidayConfig.setHolidayName(holidayRawInfo.getName());
                    holidayConfig.setHolidayRest(holidayRawInfo.getRest());
                    holidayConfig.setHolidayWage(holidayRawInfo.getWage());
                    holidayConfig.setCreateTime(new Date());
                    holidayConfigList.add(holidayConfig);
                }

                // 根据日期排序升序
                List<DailyHolidayConfig> collect = holidayConfigList.stream().sorted(Comparator.comparing(DailyHolidayConfig::getHolidayDate)).collect(Collectors.toList());

                // 批量插入节假日表中
                holidayConfigService.insertBatch(collect);
            } else {
                log.error("E|NoSubmitTask|syncHoliday()|同步节假日信息时，调用节假日网站服务出错！");
            }
        }

        log.info("每年12月28凌晨1点定时同步下一年的节假日信息，同步节假日结束时间 = {}", DateUtil.formatDateTime(new Date()));
    }
}
