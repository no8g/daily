package com.uiotsoft.daily.task;

import com.uiotsoft.daily.common.constant.CommonConstants;
import lombok.extern.slf4j.Slf4j;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>ShowMail 此类用于：邮件封装类</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月30日 8:45</p>
 * <p>@remark：</p>
 */
@Slf4j
public class ShowMail {

    private MimeMessage mimeMessage = null;

    /**
     * 附件下载后的存放目录
     */
    private String saveAttachPath = "";

    /**
     * 存放邮件内容的StringBuffer对象
     */
    private final StringBuffer bodyText = new StringBuffer();

    /**
     * 默认的日前显示格式
     */
    private String dateFormat = "yy-MM-dd HH:mm";

    /**
     * 构造函数,初始化一个MimeMessage对象
     */
    public ShowMail() {
    }

    public ShowMail(MimeMessage mimeMessage) {
        this.mimeMessage = mimeMessage;
    }

    public void setMimeMessage(MimeMessage mimeMessage) {
        this.mimeMessage = mimeMessage;
        log.info("E|ShowMail|setMimeMessage()|设置一个MimeMessage对象...");
    }

    /**
     * 获得发件人的地址和姓名
     *
     * @return 发件人的姓名
     * @throws Exception 异常
     */
    public String getFromName() throws Exception {

        InternetAddress[] address = (InternetAddress[]) mimeMessage.getFrom();

        String personal = address[0].getPersonal();
        if (personal == null) {
            personal = "";
            log.info("E|ShowMail|getFromName()|无法知道发送者的姓名...");
        }

        return personal;
    }

    /**
     * 获得发件人的地址
     *
     * @return 发件人的地址
     * @throws Exception 异常
     */
    public String getFromAddr() throws Exception {

        InternetAddress[] address = (InternetAddress[]) mimeMessage.getFrom();
        String from = address[0].getAddress();
        if (from == null) {
            from = "";
            log.info("E|ShowMail|getFromAddr()|无法知道发送者邮箱地址...");
        }

        return from;
    }

    /**
     * 获得邮件的收件人，抄送，和密送的地址和姓名，根据所传递的参数的不同
     * "to"----收件人　"cc"---抄送人地址　"bcc"---密送人地址
     *
     * @param type 邮件类型
     * @return 邮件的收件人
     * @throws Exception 异常
     */
    public String getMailAddress(String type) throws Exception {

        StringBuilder mailAddr = new StringBuilder();

        String addType = type.toUpperCase();

        InternetAddress[] address = null;

        if (CommonConstants.MAIL_TO.equals(addType) || CommonConstants.MAIL_CC.equals(addType) || CommonConstants.MAIL_BCC.equals(addType)) {

            if (CommonConstants.MAIL_TO.equals(addType)) {
                address = (InternetAddress[]) mimeMessage.getRecipients(Message.RecipientType.TO);
            } else if (CommonConstants.MAIL_CC.equals(addType)) {
                address = (InternetAddress[]) mimeMessage.getRecipients(Message.RecipientType.CC);
            } else {
                address = (InternetAddress[]) mimeMessage.getRecipients(Message.RecipientType.BCC);
            }

            if (address != null) {

                for (int i = 0; i < address.length; i++) {
                    String emailAddr = address[i].getAddress();
                    if (emailAddr == null) {
                        emailAddr = "";
                    } else {
                        emailAddr = MimeUtility.decodeText(emailAddr);
                        log.info("E|ShowMail|getMailAddress()|转换之后的emailAddr：{}", emailAddr);
                    }

                    String personal = address[i].getPersonal();

                    if (personal == null) {
                        personal = "";
                    } else {
                        personal = MimeUtility.decodeText(personal);
                        log.info("E|ShowMail|getMailAddress()|转换之后的personal：{}", personal);
                    }

                    String compositeTo = personal + "<" + emailAddr + ">";
                    log.info("E|ShowMail|getMailAddress()|完整的邮件地址：{}", compositeTo);
                    mailAddr.append(",").append(compositeTo);
                }

                mailAddr = new StringBuilder(mailAddr.substring(1));
            }
        } else {
            throw new Exception("错误的电子邮件类型!");
        }

        return mailAddr.toString();
    }

    /**
     * 获得邮件主题
     *
     * @return 邮件主题
     * @throws MessagingException 异常
     */
    public String getSubject() throws MessagingException {

        String subject = "";
        try {
            subject = MimeUtility.decodeText(mimeMessage.getSubject());
            log.info("E|ShowMail|getSubject()|转换后的subject：{}", mimeMessage.getSubject());
            if (subject == null) {
                subject = "";
            }
        } catch (Exception exec) {
            exec.printStackTrace();
        }

        return subject;
    }

    /**
     * 获得邮件发送日期
     *
     * @return 邮件发送日期
     * @throws Exception 异常
     */
    public String getSentDate() throws Exception {

        Date sentDate = mimeMessage.getSentDate();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);

        String strSentDate = format.format(sentDate);
        log.info("E|ShowMail|getSentDate()|获得邮件发送日期：{}", strSentDate);

        return strSentDate;
    }

    /**
     * 获得邮件正文内容
     *
     * @return 获得邮件正文内容
     */
    public String getBodyText() {

        return bodyText.toString();
    }

    /**
     * 解析邮件，把得到的邮件内容保存到一个StringBuffer对象中，解析邮件
     * 主要是根据MimeType类型的不同执行不同的操作，一步一步的解析
     *
     * @param part 解析邮件
     * @throws Exception 异常
     */
    public void getMailContent(Part part) throws Exception {

        String contentType = part.getContentType();

        // 获得邮件的MimeType类型
        log.info("E|ShowMail|getMailContent()|邮件的MimeType类型：{}", contentType);
        int nameIndex = contentType.indexOf(CommonConstants.MAIL_MIMETYPE_NAME);
        boolean conName = false;
        if (nameIndex != -1) {
            conName = true;
        }

        log.info("E|ShowMail|getMailContent()|邮件内容的类型：{}", contentType);
        if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_PLAIN) && !conName) {
            // text/plain类型
            bodyText.append((String) part.getContent());
        } else if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_HTML) && !conName) {
            // text/html类型
            bodyText.append((String) part.getContent());
        } else if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_MULTIPART)) {
            // multipart/*
            Multipart multipart = (Multipart) part.getContent();
            int counts = multipart.getCount();
            for (int i = 0; i < counts; i++) {
                getMailContent(multipart.getBodyPart(i));
            }
        } else if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_MESSAGE)) {
            // message/rfc822
            getMailContent((Part) part.getContent());
        } else {
            System.out.println("");
        }
    }

    /**
     * 判断此邮件是否需要回执，如果需要回执返回"true",否则返回"false"
     *
     * @return 是否需要回执
     * @throws MessagingException 异常
     */
    public boolean getReplySign() throws MessagingException {

        boolean replySign = false;
        String[] needReply = mimeMessage.getHeader("Disposition-Notification-To");
        if (needReply != null) {
            replySign = true;
        }
        if (replySign) {
            log.info("E|ShowMail|getReplySign()|该邮件需要回复！");
        } else {
            log.info("E|ShowMail|getReplySign()|该邮件不需要回复！");
        }

        return replySign;
    }

    /**
     * 获得此邮件的Message-ID
     *
     * @return 此邮件的Message-ID
     * @throws MessagingException 异常
     */
    public String getMessageId() throws MessagingException {

        String messageId = mimeMessage.getMessageID();
        log.info("E|ShowMail|getMessageId()|邮件ID = {}", messageId);

        return messageId;
    }

    /**
     * 判断此邮件是否已读，如果未读返回false,反之返回true
     *
     * @return 此邮件是否已读
     * @throws MessagingException 异常
     */
    public boolean isNew() throws MessagingException {

        boolean isNew = false;

        Flags flags = ((Message) mimeMessage).getFlags();

        Flags.Flag[] flag = flags.getSystemFlags();
        log.info("E|ShowMail|isNew()|flags的长度 = {}", flag.length);

        for (Flags.Flag value : flag) {
            if (value == Flags.Flag.SEEN) {
                isNew = true;
                break;
            }
        }

        return isNew;
    }

    /**
     * 判断此邮件是否包含附件
     *
     * @param part 此邮件
     * @return 此邮件是否包含附件
     * @throws Exception 异常
     */
    public boolean isContainAttach(Part part) throws Exception {

        boolean attachFlag = false;

        if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_MULTIPART)) {

            Multipart mp = (Multipart) part.getContent();

            for (int i = 0; i < mp.getCount(); i++) {

                BodyPart mPart = mp.getBodyPart(i);

                String disposition = mPart.getDisposition();

                if (disposition != null) {
                    if (disposition.equals(Part.ATTACHMENT) || disposition.equals(Part.INLINE)) {
                        attachFlag = true;
                    }
                } else if (mPart.isMimeType(CommonConstants.MAIL_MIMETYPE_MULTIPART)) {
                    attachFlag = isContainAttach((Part) mPart);
                } else {
                    String conType = mPart.getContentType();
                    if (conType.toLowerCase().contains("application")) {
                        attachFlag = true;
                    }
                    if (conType.toLowerCase().contains(CommonConstants.MAIL_MIMETYPE_NAME)) {
                        attachFlag = true;
                    }
                }
            }

        } else if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_MESSAGE)) {
            attachFlag = isContainAttach((Part) part.getContent());
        }

        return attachFlag;
    }

    /**
     * 保存附件
     *
     * @param part 邮件
     * @throws Exception 异常
     */
    public void saveAttachMent(Part part) throws Exception {

        String fileName = "";
        if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_MULTIPART)) {

            Multipart mp = (Multipart) part.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                BodyPart mPart = mp.getBodyPart(i);
                String disposition = mPart.getDisposition();

                if (disposition != null) {
                    if ((disposition.equals(Part.ATTACHMENT)) || (disposition.equals(Part.INLINE))) {
                        fileName = mPart.getFileName();
                        if (fileName.toLowerCase().contains("gb2312")) {
                            fileName = MimeUtility.decodeText(fileName);
                        }
                    }
                } else if (mPart.isMimeType(CommonConstants.MAIL_MIMETYPE_MULTIPART)) {
                    saveAttachMent(mPart);
                } else {
                    fileName = mPart.getFileName();
                    if ((fileName != null) && (fileName.toLowerCase().contains("GB2312"))) {
                        fileName = MimeUtility.decodeText(fileName);
                    }
                }
            }
        } else if (part.isMimeType(CommonConstants.MAIL_MIMETYPE_MESSAGE)) {
            saveAttachMent((Part) part.getContent());
        }
    }

    /**
     * 设置附件存放路径
     *
     * @param attachPath 附件
     */
    public void setAttachPath(String attachPath) {

        this.saveAttachPath = attachPath;
    }

    /**
     * 设置日期显示格式
     *
     * @param format 设置日期显示格式
     * @throws Exception 异常
     */
    public void setDateFormat(String format) throws Exception {

        this.dateFormat = format;
    }

    /**
     * 获得附件存放路径
     *
     * @return 获得附件存放路径
     */
    public String getAttachPath() {

        return saveAttachPath;
    }

    /**
     * 真正的保存附件到指定目录里
     *
     * @param fileName 文件名
     * @param in       输入流
     * @throws Exception 异常
     */
    private void saveFile(String fileName, InputStream in) throws Exception {

        String osName = System.getProperty(CommonConstants.WINDOW_SYSTEM_OS_NAME);
        String storeDir = getAttachPath();
        String separator = "";

        if (osName == null) {
            osName = "";
        }

        if (osName.toLowerCase().contains(CommonConstants.WINDOW_SYSTEM_WIN)) {
            separator = "\\";
            if (storeDir == null || "".equals(storeDir)) {
                storeDir = "c:\\tmp";
            }
        } else {
            separator = "/";
            storeDir = "/tmp";
        }

        File storeFile = new File(storeDir + separator + fileName);
        System.out.println("附件的保存地址:　" + storeFile.toString());

        BufferedOutputStream bos = null;
        BufferedInputStream bis = null;

        try {
            bos = new BufferedOutputStream(new FileOutputStream(storeFile));
            bis = new BufferedInputStream(in);
            int c;
            while ((c = bis.read()) != -1) {
                bos.write(c);
                bos.flush();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new Exception("文件保存失败!");

        } finally {
            if (bos != null) {
                bos.close();
            }
            if (bis != null) {
                bis.close();
            }
        }
    }
}