package com.uiotsoft.daily.module.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.common.constant.NumberConstants;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dao.*;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.*;
import com.uiotsoft.daily.module.service.DailyReadRecordService;
import com.uiotsoft.daily.module.service.DailyService;
import com.uiotsoft.daily.module.vo.DailyVO;
import com.uiotsoft.daily.module.vo.YesterdayJobVO;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>DailyReadRecordServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 8:50</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyReadRecordServiceImpl extends ServiceImpl<DailyReadRecordMapper, DailyJobReadRecord> implements DailyReadRecordService {

    @Resource
    private DailyReadRecordMapper dailyReadRecordMapper;

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Resource
    private DailyJobPlanMapper dailyJobPlanMapper;

    @Resource
    private DailyCommentMapper dailyCommentMapper;

    @Resource
    private DailyCommentReplyMapper dailyCommentReplyMapper;

    @Resource
    private DailyRoleConfigMapper dailyRoleConfigMapper;

    @Resource
    private DailyService dailyService;

    @Override
    public JsonResult addReadRecord(User user, Integer dailyJobId) {

        if (user == null) {
            log.error("E|DailyReadRecordServiceImpl|addReadRecord()|新增日报已读记录时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String userName = user.getUserName();
        String trueName = user.getTrueName();

        List<DailyJobReadRecord> recordList = dailyReadRecordMapper.selectList(Wrappers.<DailyJobReadRecord>lambdaQuery()
                .eq(DailyJobReadRecord::getJobId, dailyJobId).eq(DailyJobReadRecord::getReadUser, userName));
        if (recordList.size() > 0) {
            log.info("E|DailyReadRecordServiceImpl|addReadRecord()|新增日报已读记录时，数据库中已读记录已存在！");
            return JsonResult.fail("数据库中已读记录已存在！");
        }

        DailyJobReadRecord dailyJobReadRecord = new DailyJobReadRecord();
        dailyJobReadRecord.setJobId(dailyJobId);
        dailyJobReadRecord.setReadUser(userName);
        dailyJobReadRecord.setReadName(trueName);
        int insert = dailyReadRecordMapper.insert(dailyJobReadRecord);
        if (insert > 0) {
            log.info("E|DailyReadRecordServiceImpl|addReadRecord()|新增日报已读记录成功，当前登录人 trueName = {}", trueName);
            return JsonResult.ok("新增日报已读记录成功");
        }

        return JsonResult.fail("新增日报已读记录失败！");
    }

    @Override
    public JsonResult pageReadRecord(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyReadRecordServiceImpl|pageReadRecord()|分页查询日报已读列表时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String deptId = queryParamDTO.getDeptId();

        String userName = user.getUserName();
        DailyRoleConfig dailyRoleConfig = dailyRoleConfigMapper.getDailRoleConfigByUser(userName);
        if (dailyRoleConfig != null) {
            Integer roleRank = dailyRoleConfig.getRoleRank();
            String roleUser = dailyRoleConfig.getRoleUser();
            PageInfo<DailyJobReadRecord> pageInfo = new PageInfo<>();
            // 部门总监，固定写死的
            if (NumberConstants.NUMBER_ONE == roleRank) {
                if (deptId == null || "".equals(deptId)) {
                    // 研发中心CTO
                    if (CommonConstants.RD_CENTER_CTO_TEL.equals(roleUser)) {
                        deptId = CommonConstants.RD_CENTER_DEPT_ID;
                    }
                    // 其他部门总监
                }
                pageInfo = this.pageReadRecordLikeDeptId(queryParamDTO, userName, deptId);
            }
            // 总监助理
            if (NumberConstants.NUMBER_TWO == roleRank) {
                pageInfo = this.pageReadRecordLikeDeptId(queryParamDTO, userName, deptId);
            }
            // 部门经理
            if (NumberConstants.NUMBER_THREE == roleRank) {
                pageInfo = this.pageReadRecordByDeptId(queryParamDTO, userName, deptId);
            }

            return this.getReadRecordDailyVO(pageInfo.getList(), pageInfo.getTotal());
        }

        // 员工部门id
        String agentSid = user.getAgentSid();
        List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.selectList(Wrappers.<DailyRoleConfig>lambdaQuery()
                .eq(DailyRoleConfig::getFlag, 1));
        for (DailyRoleConfig roleConfig : roleConfigList) {
            String roleConfigDeptId = roleConfig.getDeptId();
            if (agentSid.contains(roleConfigDeptId) && roleConfig.getRoleRank() == NumberConstants.NUMBER_THREE) {
                PageInfo<DailyJobReadRecord> pageInfo = this.pageReadRecordByDeptId(queryParamDTO, userName, deptId);
                return this.getReadRecordDailyVO(pageInfo.getList(), pageInfo.getTotal());
            }
        }

        return JsonResult.fail("查询日报已读列表失败！");
    }

    /**
     * 分页查询日报已读记录列表，like查询
     *
     * @param queryParamDTO 查询参数
     * @param userName      当前用户手机号
     * @param deptId        部门id
     * @return 日报已读记录列表
     */
    private PageInfo<DailyJobReadRecord> pageReadRecordLikeDeptId(QueryParamDTO queryParamDTO, String userName, String deptId) {

        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();
        String createUser = queryParamDTO.getCreateUser();
        String startDate = queryParamDTO.getStartDate();
        String endDate = queryParamDTO.getEndDate();

        long start = System.currentTimeMillis();
        PageInfo<DailyJobReadRecord> pageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> dailyReadRecordMapper
                        .selectListLikeDeptId(userName, deptId, createUser, startDate, endDate));

        log.info("E|DailyReadRecordServiceImpl|pageReadRecordLikeDeptId()|分页（like）查询日报已读列表时，耗时 = 【{}毫秒】", System.currentTimeMillis() - start);

        return pageInfo;
    }

    /**
     * 分页查询日报已读记录列表，equal查询
     *
     * @param queryParamDTO 查询参数
     * @param userName      当前用户
     * @param deptId        部门id
     * @return 日报已读记录列表
     */
    private PageInfo<DailyJobReadRecord> pageReadRecordByDeptId(QueryParamDTO queryParamDTO, String userName, String deptId) {

        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();
        String createUser = queryParamDTO.getCreateUser();
        String startDate = queryParamDTO.getStartDate();
        String endDate = queryParamDTO.getEndDate();

        long start = System.currentTimeMillis();
        PageInfo<DailyJobReadRecord> pageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> dailyReadRecordMapper
                        .selectListByDeptId(userName, deptId, createUser, startDate, endDate));

        log.info("E|DailyReadRecordServiceImpl|pageReadRecordByDeptId()|分页（等于）查询日报已读列表时，耗时 = 【{}毫秒】", System.currentTimeMillis() - start);

        return pageInfo;
    }

    /**
     * 封装返回数据内容格式
     *
     * @param jobReadRecordList 返回的数据列表
     * @return 封装返回数据内容格式
     */
    private JsonResult getReadRecordDailyVO(List<DailyJobReadRecord> jobReadRecordList, long total) {
        // 日报列表
        List<DailyJob> dailyJobList = new ArrayList<>();
        DailyVO dailyVO = new DailyVO();
        for (DailyJobReadRecord dailyJobReadRecord : jobReadRecordList) {
            Integer jobId = dailyJobReadRecord.getJobId();
            DailyJob dailyJob = dailyJobMapper.selectById(jobId);
            // 此接口是查询日报已读列表，故所有日报均为已读
            dailyJob.setIsRead(CommonConstants.HAVE_READ);
            dailyJobList.add(dailyJob);
            // 封装昨天日报日程（日报完成情况）
            this.assembleYesterdayJobPlan(dailyJob);
            // 封装日报评论，回复列表
            this.queryCommentAndReply(dailyJob, jobId);
        }

        dailyVO.setDailyJobList(dailyJobList);
        dailyVO.setTotalCount(total);

        return JsonResult.ok(dailyVO);
    }

    @Override
    public JsonResult pageNoReadRecord(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyReadRecordServiceImpl|pageNoReadRecord()|分页查询日报未读列表时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String deptId = queryParamDTO.getDeptId();
        String userName = user.getUserName();

        DailyRoleConfig dailyRoleConfig = dailyRoleConfigMapper.getDailRoleConfigByUser(userName);
        if (dailyRoleConfig != null) {
            Integer roleRank = dailyRoleConfig.getRoleRank();
            String roleUser = dailyRoleConfig.getRoleUser();
            PageInfo<DailyJob> pageInfo = new PageInfo<>();
            // 部门总监，固定写死的
            if (NumberConstants.NUMBER_ONE == roleRank) {
                if (deptId == null || "".equals(deptId)) {
                    // 研发中心CTO
                    if (CommonConstants.RD_CENTER_CTO_TEL.equals(roleUser)) {
                        deptId = CommonConstants.RD_CENTER_DEPT_ID;
                        queryParamDTO.setDeptId(deptId);
                    }
                    // 其他部门总监...
                }
                pageInfo = this.pageNoReadRecordLikeDeptId(queryParamDTO, userName, deptId);
            }
            // 部门总监或者总监助理
            if (NumberConstants.NUMBER_TWO == roleRank) {
                pageInfo = this.pageNoReadRecordLikeDeptId(queryParamDTO, userName, deptId);
            }
            // 部门经理
            if (NumberConstants.NUMBER_THREE == roleRank) {
                pageInfo = this.pageNoReadRecordLikeDeptId(queryParamDTO, userName, deptId);
            }

            return this.getNoReadRecordDailyVO(pageInfo.getList(), pageInfo.getTotal());
        }

        // 员工部门id
        String agentSid = user.getAgentSid();
        List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.selectList(Wrappers.<DailyRoleConfig>lambdaQuery()
                .eq(DailyRoleConfig::getFlag, 1));

        for (DailyRoleConfig roleConfig : roleConfigList) {
            String roleConfigDeptId = roleConfig.getDeptId();
            if (agentSid.contains(roleConfigDeptId) && roleConfig.getRoleRank() == NumberConstants.NUMBER_THREE) {
                PageInfo<DailyJob> pageInfo = this.pageNoReadRecordLikeDeptId(queryParamDTO, userName, deptId);
                return this.getNoReadRecordDailyVO(pageInfo.getList(), pageInfo.getTotal());
            }
        }

        return JsonResult.fail("查询日报未读列表失败！");
    }

    /**
     * 分页查询日报未读记录列表，like查询
     *
     * @param queryParamDTO 查询参数
     * @param userName      当前用户
     * @return 日报已读记录列表
     */
    private PageInfo<DailyJob> pageNoReadRecordLikeDeptId(QueryParamDTO queryParamDTO, String userName, String deptId) {

        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();
        String createUser = queryParamDTO.getCreateUser();
        String startDate = queryParamDTO.getStartDate();
        String endDate = queryParamDTO.getEndDate();

        long start = System.currentTimeMillis();
        PageInfo<DailyJob> pageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> dailyJobMapper.selectNoReadListLikeDeptId(userName, deptId, createUser, startDate, endDate));

        log.info("E|DailyReadRecordServiceImpl|pageReadRecordLikeDeptId()|分页（like）查询日报未读列表时，耗时 = 【{}毫秒】", System.currentTimeMillis() - start);

        return pageInfo;
    }

    /**
     * 封装返回数据内容格式
     *
     * @param dailyJobList 返回的数据列表
     * @return 封装返回数据内容格式
     */
    private JsonResult getNoReadRecordDailyVO(List<DailyJob> dailyJobList, long total) {

        DailyVO dailyVO = new DailyVO();
        for (DailyJob dailyJob : dailyJobList) {
            Integer dailyJobId = dailyJob.getId();
            // 日报未读
            dailyJob.setIsRead(CommonConstants.NO_READ);
            // 封装昨天日报日程（日报完成情况）
            this.assembleYesterdayJobPlan(dailyJob);
            // 封装日报评论，回复列表
            this.queryCommentAndReply(dailyJob, dailyJobId);
        }
        // 未读日报列表
        dailyVO.setDailyJobList(dailyJobList);
        dailyVO.setTotalCount(total);

        return JsonResult.ok(dailyVO);
    }

    /**
     * 封装昨天日报日程（日报完成情况）
     *
     * @param todayDailyJob 今天日报
     */
    private void assembleYesterdayJobPlan(DailyJob todayDailyJob) {
        // 查询昨天日报日程
        String createUser = todayDailyJob.getCreateUser();
        Date createTime = todayDailyJob.getCreateTime();
        DailyJob yesterdayDailyJob = dailyJobMapper.selectYesterdayDailyJob(createUser, createTime);
        if (yesterdayDailyJob != null) {
            Integer yesterdayDailyJobId = yesterdayDailyJob.getId();
            List<DailyJobPlan> yesterdayJobPlanList = dailyJobPlanMapper.selectList(Wrappers.<DailyJobPlan>lambdaQuery()
                    .eq(DailyJobPlan::getJobId, yesterdayDailyJobId));
            YesterdayJobVO yesterdayJobVO = new YesterdayJobVO();
            yesterdayJobVO.setYesterdayPlanList(yesterdayJobPlanList);
            yesterdayJobVO.setCreateTime(yesterdayDailyJob.getCreateTime());
            yesterdayJobVO.setUndoneCount(dailyService.getUndoneDailyJobPlanListByJobId(yesterdayDailyJobId).size());
            todayDailyJob.setYesterdayPlan(yesterdayJobVO);
        }
    }

    /**
     * 封装日报评论，回复列表
     *
     * @param dailyJob   日报列表
     * @param dailyJobId 日报id
     */
    private void queryCommentAndReply(DailyJob dailyJob, Integer dailyJobId) {

        // 未读日报评论列表
        List<DailyJobPlan> dailyJobPlanList = dailyJobPlanMapper.selectList(new QueryWrapper<DailyJobPlan>()
                .lambda().eq(DailyJobPlan::getJobId, dailyJobId).orderByAsc(DailyJobPlan::getId));
        dailyJob.setTodayPlanList(dailyJobPlanList);

        // 未读日报评论列表
        List<DailyJobComment> dailyCommentList = dailyCommentMapper.selectList(new QueryWrapper<DailyJobComment>()
                .lambda().eq(DailyJobComment::getJobId, dailyJobId).orderByDesc(DailyJobComment::getId));
        dailyJob.setCommentList(dailyCommentList);

        for (DailyJobComment dailyJobComment : dailyCommentList) {
            Integer commentId = dailyJobComment.getId();
            // 未读日报评论回复列表
            List<DailyJobCommentReply> replyList = dailyCommentReplyMapper.selectList(new QueryWrapper<DailyJobCommentReply>()
                    .lambda().eq(DailyJobCommentReply::getCommentId, commentId).orderByDesc(DailyJobCommentReply::getId));
            dailyJobComment.setReplyList(replyList);
        }
    }
}
