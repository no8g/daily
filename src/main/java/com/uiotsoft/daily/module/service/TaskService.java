package com.uiotsoft.daily.module.service;

/**
 * <p>TaskService 此接口用于：定时任务</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月22日 14:32</p>
 * <p>@remark：</p>
 */
public interface TaskService {

    /**
     * 定时查询日报未提交人员
     */
    void queryDailyNoSubmitSync();

    /**
     * 获取今天是每周几
     *
     * @param dayOfWeek 今天
     * @return 每周几
     */
    String getDayOfWeek(int dayOfWeek);
}
