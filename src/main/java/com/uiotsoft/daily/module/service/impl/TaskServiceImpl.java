package com.uiotsoft.daily.module.service.impl;

import cn.hutool.core.date.DateUtil;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.module.dao.*;
import com.uiotsoft.daily.module.entity.*;
import com.uiotsoft.daily.module.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>TaskServiceImpl 此类用于：定时任务实现类</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月22日 14:33</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class TaskServiceImpl implements TaskService {

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Resource
    private DailyRoleConfigMapper dailyRoleConfigMapper;

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Resource
    private DailyNoSubmitReportMapper dailyNoSubmitReportMapper;

    @Resource
    private DailyHolidayConfigMapper dailyHolidayConfigMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void queryDailyNoSubmitSync() {

        // 查询出角色配置表中角色层级为2的所有部门(助理所在部门的部门id)
        List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.selectListByRoleRank();

        // 定义当日未提交日报人员集合
        List<DailyNoSubmitReport> noSubmitReportList = new ArrayList<>();

        Date todayDate = new Date();
        Date yesterdayDate = DateUtil.yesterday();
        // 获取昨天是周几
        String dayOfWeek = this.getDayOfWeek(DateUtil.dayOfWeek(todayDate) - 1);
        List<DailyJob> dailyJobList = this.getDailyJobList(yesterdayDate);

        for (DailyRoleConfig dailyRoleConfig : roleConfigList) {
            String roleConfigDeptId = dailyRoleConfig.getDeptId();
            // 当前部门下所有人员
            List<DailyViewUser> dailyViewUserList = dailyViewUserMapper.selectListLikeDeptId(roleConfigDeptId, null);
            // 所有人员(CopyOnWriteArrayList线程安全删除 重复的)
            CopyOnWriteArrayList<DailyViewUser> viewUserList = new CopyOnWriteArrayList<>(dailyViewUserList);
            for (DailyViewUser viewUser : viewUserList) {
                for (DailyJob dailyJob : dailyJobList) {
                    if (dailyJob.getCreateUser().equals(viewUser.getUserName())) {
                        viewUserList.remove(viewUser);
                    }
                }
            }
            // 此时viewUserList已全部是未提交人员列表
            for (DailyViewUser viewUser : viewUserList) {
                DailyNoSubmitReport noSubmitReport = new DailyNoSubmitReport();
                noSubmitReport.setCreateTime(todayDate);
                noSubmitReport.setDayOfWeek(dayOfWeek);
                noSubmitReport.setDeptId(viewUser.getAgentSid());
                noSubmitReport.setDeptName(viewUser.getAgentName());
                noSubmitReport.setNoSubmitDate(yesterdayDate);
                noSubmitReport.setUserName(viewUser.getUserName());
                noSubmitReport.setTrueName(viewUser.getTrueName());
                noSubmitReport.setUserType(1);
                noSubmitReportList.add(noSubmitReport);
            }
        }

        try {
            int insert = dailyNoSubmitReportMapper.insertBatch(noSubmitReportList);
            if (insert > 0) {
                log.info("E|TaskServiceImpl|queryDailyNoSubmitSync()|批量插入日报未提交记录成功，" +
                        "日期 = 【{}】", DateUtil.format(new Date(), "yyyy-MM-dd"));
            }
        } catch (Exception e) {
            log.error("E|TaskServiceImpl|queryDailyNoSubmitSync()|批量插入日报未提交记录失败！" +
                    "【 日期 = {}，原因 = {}】", DateUtil.format(new Date(), "yyyy-MM-dd"), e.getMessage());
        }
    }

    /**
     * 非工作日，不限制日报提交时间，定时任务查询全天任何时间段内提交的日报都表示已提交日报
     *
     * @param yesterdayDate 昨天日期
     * @return 获取已提交日报列表
     */
    private List<DailyJob> getDailyJobList(Date yesterdayDate) {

        List<DailyJob> dailyJobList = new ArrayList<>();
        // 非工作日不限制日报提交时间，那么定时任务查询日报提交时间应该是查询当日所有时间段内提交的日报，而不是只查询18点到24点之间提交的了
        // 1、先判断昨天是否是节假日
        DailyHolidayConfig dailyHolidayConfig = dailyHolidayConfigMapper.getHolidayConfigInfo();
        if (dailyHolidayConfig != null) {
            dailyJobList = dailyJobMapper.listByCreateTime(dailyHolidayConfig.getHolidayDate());
            return dailyJobList;
        }
        // 2、再判断昨天是否是周六、周日，提交日报不限制，定时任务执行也不限制
        int yesterday = DateUtil.dayOfWeek(yesterdayDate);
        String dayOfWeek = this.getDayOfWeek(yesterday);
        if (CommonConstants.CN_SATURDAY.equals(dayOfWeek) || CommonConstants.CN_SUNDAY.equals(dayOfWeek)) {
            dailyJobList = dailyJobMapper.listByCreateTime(yesterdayDate);
            return dailyJobList;
        }
        // 3、按工作日判断
        // 查询当日已提交日报人员列表（时间段：18:00-24:00）
        dailyJobList = dailyJobMapper.selectListByCreateTime();

        return dailyJobList;
    }

    /**
     * 获取今天是每周周几
     *
     * @param dayOfWeek 数字
     * @return 每周周几
     */
    @Override
    public String getDayOfWeek(int dayOfWeek) {

        String dayOfWeekStr;
        switch (dayOfWeek) {
            case 1:
                dayOfWeekStr = "星期日";
                break;
            case 2:
                dayOfWeekStr = "星期一";
                break;
            case 3:
                dayOfWeekStr = "星期二";
                break;
            case 4:
                dayOfWeekStr = "星期三";
                break;
            case 5:
                dayOfWeekStr = "星期四";
                break;
            case 6:
                dayOfWeekStr = "星期五";
                break;
            default:
                dayOfWeekStr = "星期六";
                break;
        }

        return dayOfWeekStr;
    }
}
