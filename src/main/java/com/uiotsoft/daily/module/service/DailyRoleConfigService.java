package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyRoleConfig;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyRoleConfigService 此接口用于：日报角色配置服务</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 17:10</p>
 * <p>@remark：</p>
 */
public interface DailyRoleConfigService {

    /**
     * 添加角色
     *
     * @param user            当前用户
     * @param dailyRoleConfig 角色信息
     * @return 是否添加成功
     */
    JsonResult addDailyRoleConfig(User user, DailyRoleConfig dailyRoleConfig);

    /**
     * 根据id逻辑删除日报角色配置表数据
     *
     * @param user     当前用户
     * @param configId 日报角色配置表id
     * @return 是否删除成功
     */
    JsonResult deleteDailyRoleConfig(User user, Integer configId);

    /**
     * 修改角色
     *
     * @param user            当前用户
     * @param dailyRoleConfig 角色信息
     * @return 是否修改成功
     */
    JsonResult updateDailyRoleConfig(User user, DailyRoleConfig dailyRoleConfig);

    /**
     * 分页查询角色配置信息
     *
     * @param user          当前用户
     * @param queryParamDTO 查询参数
     * @return 分页查询角色配置信息
     */
    JsonResult pageDailyRoleConfig(User user, QueryParamDTO queryParamDTO);

    /**
     * 根据当前角色获取部门列表
     *
     * @param user 当前用户
     * @return 部门列表
     */
    JsonResult getDeptList(User user);

    /**
     * 查询员工列表，用于查看日报根据员工筛选查询日报列表
     *
     * @param user     当前用户
     * @param deptId   部门id
     * @param trueName 员工姓名（可模糊查询）
     * @param isTeam   是否是团队日报
     * @return 员工列表
     */
    JsonResult getUserList(User user, String deptId, String trueName, Boolean isTeam);

    /**
     * 根据用户名查询角色配置用户
     *
     * @param userName 用户名
     * @return 角色配置用户
     */
    DailyRoleConfig getDailyRoleConfigByUsername(String userName);
}
