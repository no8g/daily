package com.uiotsoft.daily.module.service.impl;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dao.DailyCommentReplyMapper;
import com.uiotsoft.daily.module.entity.DailyJobCommentReply;
import com.uiotsoft.daily.module.service.DailyCommentReplyService;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>DailyCommentReplyServiceImpl 此类用于：日报评论回复服务层实现</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 9:54</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyCommentReplyServiceImpl implements DailyCommentReplyService {

    @Resource
    private DailyCommentReplyMapper dailyCommentReplyMapper;

    @Override
    public JsonResult addReply(User user, DailyJobCommentReply dailyJobCommentReply) {

        if (user == null) {
            log.error("E|DailyCommentReplyServiceImpl|addReply()|新增评论回复时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String userName = user.getUserName();
        String trueName = user.getTrueName();
        // commentId 和pid 前端应该传入
        dailyJobCommentReply.setCreateUser(userName);
        dailyJobCommentReply.setCreateName(trueName);
        dailyJobCommentReply.setCreateTime(new Date());
        int insert = dailyCommentReplyMapper.insert(dailyJobCommentReply);
        if (insert > 0) {
            log.info("E|DailyCommentReplyServiceImpl|addReply()|新增评论回复成功，当前登录人 trueName = {}", trueName);
            return JsonResult.ok("新增评论回复成功");
        }

        return JsonResult.fail("新增评论回复失败！");
    }
}
