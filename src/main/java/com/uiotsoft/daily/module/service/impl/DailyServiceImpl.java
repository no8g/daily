package com.uiotsoft.daily.module.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.common.constant.NumberConstants;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dao.*;
import com.uiotsoft.daily.module.dto.DailyAddDTO;
import com.uiotsoft.daily.module.dto.DailyDTO;
import com.uiotsoft.daily.module.dto.DailyJobPlanDTO;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.*;
import com.uiotsoft.daily.module.service.DailyRoleConfigService;
import com.uiotsoft.daily.module.service.DailyService;
import com.uiotsoft.daily.module.service.TaskService;
import com.uiotsoft.daily.module.vo.DailyVO;
import com.uiotsoft.daily.module.vo.YesterdayJobVO;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>DailyServiceImpl 此类用于：日报相关接口实现</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 9:00</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyServiceImpl extends ServiceImpl<DailyJobMapper, DailyJob> implements DailyService {

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Resource
    private DailyJobPlanMapper dailyJobPlanMapper;

    @Resource
    private DailyRoleConfigMapper dailyRoleConfigMapper;

    @Resource
    private DailyCommentMapper dailyCommentMapper;

    @Resource
    private DailyCommentReplyMapper dailyCommentReplyMapper;

    @Resource
    private DailyReadRecordMapper dailyReadRecordMapper;

    @Resource
    private DailyHolidayConfigMapper dailyHolidayConfigMapper;

    @Resource
    private TaskService taskService;

    @Resource
    private DailyRoleConfigService dailyRoleConfigService;

    @Resource
    private DailyPositionMapper dailyPositionMapper;

    @Resource
    private DailyTeamMemberMapper dailyTeamMemberMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult addDaily(User user, DailyDTO dailyDTO) {

        if (user == null) {
            log.error("E|DailyServiceImpl|addDaily()|新增日报工作时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        // 今日日程安排
        DailyAddDTO dailyAddDTO = dailyDTO.getDailyAddDTO();
        // 昨天工作完成情况
        DailyJobPlanDTO dailyJobPlanDTO = dailyDTO.getDailyJobPlanDTO();

        // 0 得到当前登录人和部门
        String userName = user.getUserName();
        String trueName = user.getTrueName();
        log.info("E|DailyServiceImpl|addDaily()|插入日报工作表，获取当前登录人 userName = {}, trueName = {}", userName, trueName);

        // 1 插入日报工作表
        JsonResult jsonResult = this.addTodayDaily(user, dailyAddDTO);
        if (jsonResult.isFail()) {
            return jsonResult;
        }
        // 2 更新日报完成情况
        this.updateYesterdayDaily(dailyJobPlanDTO);

        return JsonResult.ok("新增日报成功！");
    }

    /**
     * 新增今天工作日报日程
     *
     * @param user        当前登录用户
     * @param dailyAddDTO 今天工作日报日程
     */
    private JsonResult addTodayDaily(User user, DailyAddDTO dailyAddDTO) {

        String userName = user.getUserName();
        String trueName = user.getTrueName();
        // 1 新增日报工作
        DailyJob dailyJob = new DailyJob();
        dailyJob.setCreateUser(userName);
        dailyJob.setCreateName(trueName);

        DailyRoleConfig dailyRoleConfig = dailyRoleConfigMapper.getDailRoleConfigByUser(userName);
        // 如果当前用户为总监、总监助理或者部门经理角色，则直接使用当前部门id和部门名称
        if (dailyRoleConfig != null) {
            dailyJob.setDeptId(dailyRoleConfig.getDeptId());
            dailyJob.setDeptName(dailyRoleConfig.getDeptName());
        } else {
            // 如果当前用户为普通员工，则判断出当前员工属于哪个部门，则将当前部门赋值给日报所属部门id和部门名称
            List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.selectList(Wrappers.<DailyRoleConfig>lambdaQuery()
                    .eq(DailyRoleConfig::getFlag, 1));
            for (DailyRoleConfig roleConfig : roleConfigList) {
                String roleConfigDeptId = roleConfig.getDeptId();
                String agentSid = user.getAgentSid();
                if (agentSid.contains(roleConfigDeptId) && roleConfig.getRoleRank() == NumberConstants.NUMBER_THREE) {
                    dailyJob.setDeptId(roleConfigDeptId);
                    dailyJob.setDeptName(roleConfig.getDeptName());
                }
            }
        }

        // 如果是非工作日，那么当天任何一个时间段内都可提交日报
        if (isWorkDay()) {
            // 如果是工作日，请在规定的时间段内提交日报
            if (!isRightTime()) {
                return JsonResult.fail("请在规定的时间段内（18:00:00--24:00:00）提交日报！");
            }
        }

        // 日报创建时间
        dailyJob.setCreateTime(new Date());
        int insert = dailyJobMapper.insertDaily(dailyJob);
        if (insert > 0) {
            // 2 得到当前日报的id，插入日报工作日程表
            Integer dailyJobId = dailyJob.getId();
            log.info("E|DailyServiceImpl|addDaily()|新增日报成功，得到当前日报【主键id = {}，操作人 = {}】", dailyJobId, trueName);
            List<DailyJobPlan> dailyJobPlanList = dailyAddDTO.getDailyJobPlanList();
            for (DailyJobPlan dailyJobPlan : dailyJobPlanList) {
                dailyJobPlan.setJobId(dailyJobId);
                dailyJobPlanMapper.insert(dailyJobPlan);
            }
        }

        return JsonResult.ok("新增日报成功！");
    }

    /**
     * 判断是否是工作日
     *
     * @return true 工作日，false 节假日
     */
    private boolean isWorkDay() {

        // 从节假日表中查询节假日详情
        DailyHolidayConfig holidayConfigInfo = dailyHolidayConfigMapper.getHolidayConfigInfo();
        if (holidayConfigInfo != null) {
            return false;
        }

        // 判断今天是否是周六、周日
        Date todayDate = new Date();
        int dayOfWeek = DateUtil.dayOfWeek(todayDate);
        String weekDay = taskService.getDayOfWeek(dayOfWeek);
        if (CommonConstants.CN_SATURDAY.equals(weekDay) || CommonConstants.CN_SUNDAY.equals(weekDay)) {
            return false;
        }

        return true;
    }

    /**
     * 判断当前时间是否在18点和24点之间，如果是，则可以提交日报，否则不让提交
     *
     * @return 判断当前时间是否在18点和24点之间
     */
    private boolean isRightTime() {

        LocalTime now = LocalTime.now();
        LocalTime startTime = LocalTime.of(18, 0, 0);
        LocalTime endTime = LocalTime.of(23, 59, 59);

        return now.isBefore(endTime) && now.isAfter(startTime);
    }

    /**
     * 更新昨日完成情况
     *
     * @param dailyJobPlanDTO 昨日日报信息
     */
    private void updateYesterdayDaily(DailyJobPlanDTO dailyJobPlanDTO) {

        if (dailyJobPlanDTO != null) {

            // 1 更新完成情况
            List<DailyJobPlan> dailyJobPlanList = dailyJobPlanDTO.getDailyJobPlanList();
            for (DailyJobPlan dailyJobPlan : dailyJobPlanList) {
                // 这里主要是为增加临时工作而判断的
                if (dailyJobPlan.getId() != null) {
                    dailyJobPlanMapper.updateById(dailyJobPlan);
                } else {
                    // 没有id，表明是新增的临时工作
                    dailyJobPlanMapper.insert(dailyJobPlan);
                }
            }

            // 2 同步更改日报更新时间，此处主要是为了员工第一次填写日报时增加的健壮性判断
            Integer dailyJobId = dailyJobPlanDTO.getDailyJobId();
            if (dailyJobId != null) {
                DailyJob dailyJob = dailyJobMapper.selectById(dailyJobId);
                if (dailyJob != null) {
                    dailyJob.setUpdateTime(new Date());
                    dailyJobMapper.updateById(dailyJob);
                }
            }
        }
    }

    @Override
    public JsonResult getLastDaily(User user, String createDate) {

        if (user == null) {
            log.error("E|DailyServiceImpl|getLastDaily()|获取昨天日报时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String userName = user.getUserName();
        String trueName = user.getTrueName();
        log.info("E|DailyServiceImpl|getLastDaily()|获取昨天日报时，根据日期查询昨天日报传入日期参数：【createDate = {}】，当前登录人：【trueName = {}】", createDate, trueName);
        DailyJob dailyJob;
        if (createDate == null || "".equals(createDate)) {
            // 根据当前登录人查询出最新一条，即昨日完成情况查询（不用考虑单双休和假期情况）
            dailyJob = dailyJobMapper.selectOneByCreateUser(userName);
        } else {
            // 根据日期查询出一条日报信息，即昨日完成情况查询（不用考虑单双休和假期情况）
            dailyJob = dailyJobMapper.selectByCreateTime(userName, createDate);
        }

        if (dailyJob != null) {
            Integer dailyJobId = dailyJob.getId();
            List<DailyJobPlan> dailyJobPlanList = this.getDailyJobPlanListByJobId(dailyJobId);

            DailyJobPlanDTO dailyJobPlanDTO = new DailyJobPlanDTO();
            dailyJobPlanDTO.setDailyJobId(dailyJobId);
            dailyJobPlanDTO.setDailyCreateTime(dailyJob.getCreateTime());
            dailyJobPlanDTO.setDailyJobPlanList(dailyJobPlanList);

            return JsonResult.ok(dailyJobPlanDTO);
        }

        return JsonResult.ok("暂无数据！");
    }

    @Override
    public JsonResult pageDailyList(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyServiceImpl|getDailyListByDept()|查询部门日报列表，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }
        log.info("E|DailyServiceImpl|getDailyListByDept()|查询部门日报列表，请求参数【queryParamDTO = {}】", queryParamDTO);
        // 当前用户所在部门，查看当前用户角色
        String userName = user.getUserName();
        // 根据手机号查询角色配置表，得到当前用户角色，如果查到一条，表明属于总监或部门经理
        DailyRoleConfig dailyRoleConfig = dailyRoleConfigMapper.getDailRoleConfigByUser(userName);

        if (dailyRoleConfig != null) {
            log.info("E|DailyServiceImpl|getDailyListByDept()| 根据手机号查询角色配置表，得到当前用户角色信息：【dailyRoleConfig = {}】", dailyRoleConfig);
            Integer roleRank = dailyRoleConfig.getRoleRank();
            String roleConfigDeptId = dailyRoleConfig.getDeptId();
            String roleUser = dailyRoleConfig.getRoleUser();
            PageInfo<DailyJob> dailyJobPageInfo = new PageInfo<>();
            // 公司领导，单独处理
            if (roleRank == NumberConstants.NUMBER_ONE) {
                // 得到当前传入参数的部门id
                String deptId = queryParamDTO.getDeptId();
                if (deptId == null || "".equals(deptId)) {
                    // 研发中心CTO手机号和部门id
                    if (roleUser.equals(CommonConstants.RD_CENTER_CTO_TEL)) {
                        queryParamDTO.setDeptId(CommonConstants.RD_CENTER_DEPT_ID);
                    }
                    // 其他部门总监CTO手机号和部门id....
                }
                dailyJobPageInfo = this.dailyJobListLikeDeptId(queryParamDTO, userName);
            }
            // 总监助理角色，默认查看当前部门下的所有部门的日报
            if (roleRank == NumberConstants.NUMBER_TWO) {
                queryParamDTO.setDeptId(roleConfigDeptId);
                dailyJobPageInfo = this.dailyJobListLikeDeptId(queryParamDTO, userName);

            }
            // 部门经理或者部门副经理角色，只查看当前部门所有的日报
            if (roleRank == NumberConstants.NUMBER_THREE) {
                queryParamDTO.setDeptId(roleConfigDeptId);
                dailyJobPageInfo = this.dailyJobListLikeDeptId(queryParamDTO, userName);
            }

            return JsonResult.ok(this.getDailyVO(dailyJobPageInfo, userName));
        }

        // 如果查询不到，则表明是普通员工，默认查看本部门下的所有人的日报
        String agentSid = user.getAgentSid();
        // 查询配置角色表里所有部门id列表，循环和当前用户部门id比较，startWith 则表明是这个部门的，以这个部门id查询日报列表
        List<DailyRoleConfig> dailyRoleConfigList = dailyRoleConfigMapper.selectList(Wrappers.<DailyRoleConfig>lambdaQuery()
                .eq(DailyRoleConfig::getFlag, 1));
        for (DailyRoleConfig roleConfig : dailyRoleConfigList) {
            String roleConfigDeptId = roleConfig.getDeptId();
            Integer roleRank = roleConfig.getRoleRank();
            String deptName = roleConfig.getDeptName();
            if (agentSid.startsWith(roleConfigDeptId) && roleRank == NumberConstants.NUMBER_THREE) {
                log.info("E|DailyServiceImpl|getDailyListByDept()| 查询到当前用户部门为：【部门ID = {}，部门名称 = {}】", roleConfigDeptId, deptName);
                queryParamDTO.setDeptId(roleConfigDeptId);
                PageInfo<DailyJob> dailyJobPageInfo = this.dailyJobListLikeDeptId(queryParamDTO, userName);
                return JsonResult.ok(this.getDailyVO(dailyJobPageInfo, userName));
            }
        }

        return JsonResult.fail("查询日报列表失败！");
    }

    /**
     * 根据部门id查询工作日报列表（分页），like查询
     *
     * @param queryParamDTO 部门id
     * @param userName      当前登录人手机号
     * @return 工作日报列表（分页）
     */
    private PageInfo<DailyJob> dailyJobListLikeDeptId(QueryParamDTO queryParamDTO, String userName) {

        long startTimeMillis = System.currentTimeMillis();
        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();
        String deptId = queryParamDTO.getDeptId();
        String createUser = queryParamDTO.getCreateUser();
        String startDate = queryParamDTO.getStartDate();
        String endDate = queryParamDTO.getEndDate();

        PageInfo<DailyJob> pageInfo = PageHelper.startPage(pageNum, pageSize, "id desc")
                .doSelectPageInfo(() -> dailyJobMapper.listLikeDeptId(deptId, createUser, userName, startDate, endDate));

        log.info("E|DailyServiceImpl|pageDailyListByDeptId()|分页查询当前部门日报列表耗时：【{}毫秒】", System.currentTimeMillis() - startTimeMillis);
        log.info("E|DailyServiceImpl|pageDailyListByDeptId()|分页Like查询当前部门日报列表：【pageInfo = {}】", pageInfo);

        return pageInfo;
    }

    /**
     * 封装查看日报返回数据内容
     *
     * @param pageDailyJob 分页的日报列表
     * @param userName     当前登录人
     * @return 日报返回数据内容
     */
    @Override
    public DailyVO getDailyVO(PageInfo<DailyJob> pageDailyJob, String userName) {

        long start = System.currentTimeMillis();

        DailyVO dailyVO = new DailyVO();
        // 得到日报列表
        List<DailyJob> dailyJobList = pageDailyJob.getList();
        if (CollUtil.isEmpty(dailyJobList)) {
            log.info("E|DailyServiceImpl|getDailyVO()|封装查看日报返回数据内容时，日报列表为空！");
            dailyVO.setTotalCount(0L);
            return dailyVO;
        }

        // 根据日报列表查询日报评论列表
        for (DailyJob todayDailyJob : dailyJobList) {
            // 得到当前日报id
            Integer todayDailyJobId = todayDailyJob.getId();
            // 查看日报是否已读
            DailyJobReadRecord dailyJobReadRecord = dailyReadRecordMapper.selectOneByUserNameAndJobId(userName, todayDailyJobId);
            if (dailyJobReadRecord != null) {
                todayDailyJob.setIsRead(CommonConstants.HAVE_READ);
            } else {
                todayDailyJob.setIsRead(CommonConstants.NO_READ);
            }

            // 查看日报日程安排列表
            List<DailyJobPlan> todayDailyJobPlanList = this.getDailyJobPlanListByJobId(todayDailyJobId);
            todayDailyJob.setTodayPlanList(todayDailyJobPlanList);
            // 根据今天的日报时间和日报创建人查询昨天的日报计划
            String createUser = todayDailyJob.getCreateUser();
            Date createTime = todayDailyJob.getCreateTime();
            DailyJob yesterdayDailyJob = dailyJobMapper.selectYesterdayDailyJob(createUser, createTime);
            if (yesterdayDailyJob != null) {
                Integer yesterdayDailyJobId = yesterdayDailyJob.getId();
                // 查询昨天日报日程列表
                List<DailyJobPlan> yesterdayDailyJobPlanList = this.getDailyJobPlanListByJobId(yesterdayDailyJobId);
                // 添加昨天日报日程
                YesterdayJobVO yesterdayJobVO = new YesterdayJobVO();
                yesterdayJobVO.setCreateTime(yesterdayDailyJob.getCreateTime());
                yesterdayJobVO.setYesterdayPlanList(yesterdayDailyJobPlanList);
                // 获取昨天未完成日报日程安排事项列表
                yesterdayJobVO.setUndoneCount(this.getUndoneDailyJobPlanListByJobId(yesterdayDailyJobId).size());
                todayDailyJob.setYesterdayPlan(yesterdayJobVO);
            }

            // 查询日报评论列表
            List<DailyJobComment> dailyJobCommentList = dailyCommentMapper.selectList(Wrappers.<DailyJobComment>lambdaQuery()
                    .eq(DailyJobComment::getJobId, todayDailyJobId));
            todayDailyJob.setCommentList(dailyJobCommentList);
            // 得到评论列表，然后再根据评论id查询评论回复列表
            for (DailyJobComment dailyJobComment : dailyJobCommentList) {
                Integer commentId = dailyJobComment.getId();
                List<DailyJobCommentReply> dailyJobCommentReplyList = dailyCommentReplyMapper.selectList(Wrappers.<DailyJobCommentReply>lambdaQuery()
                        .eq(DailyJobCommentReply::getCommentId, commentId));
                dailyJobComment.setReplyList(dailyJobCommentReplyList);
            }
        }
        // 未读数
        long total = pageDailyJob.getTotal();
        dailyVO.setTotalCount(total);
        dailyVO.setDailyJobList(dailyJobList);

        log.info("E|DailyServiceImpl|getDailyVO()|封装查看日报返回数据内容耗时：【{}毫秒】", System.currentTimeMillis() - start);

        return dailyVO;
    }

    /**
     * 过虑掉昨天日报未完成的日程事项列表
     *
     * @param jobId 日报id
     * @return 未完成的日程事项列表
     */
    @Override
    public List<DailyJobPlan> getUndoneDailyJobPlanListByJobId(Integer jobId) {

        List<DailyJobPlan> dailyJobPlanList = this.getDailyJobPlanListByJobId(jobId);

        // 过滤出所有未完成的日程安排列表，以下代码完成百分比，0表示未开始，100表示已完成
        List<DailyJobPlan> undoneJobPlanList = dailyJobPlanList.stream().filter(item -> {
            String finishStatus = item.getFinishStatus();
            return "0".equals(finishStatus) || "10".equals(finishStatus) || "20".equals(finishStatus) ||
                    "30".equals(finishStatus) || "40".equals(finishStatus) || "50".equals(finishStatus) ||
                    "60".equals(finishStatus) || "70".equals(finishStatus) || "80".equals(finishStatus) ||
                    "90".equals(finishStatus);
        }).collect(Collectors.toList());

        log.info("E|DailyServiceImpl|getUndoneDailyJobPlanListByJobId()|过虑掉昨天日报未完成的日程事项列表，undoneJobPlanList = {}", undoneJobPlanList);

        return undoneJobPlanList;
    }

    /**
     * 根据日报id查询日报日程列表
     *
     * @param jobId 日报id
     * @return 日报日程列表
     */
    private List<DailyJobPlan> getDailyJobPlanListByJobId(Integer jobId) {

        List<DailyJobPlan> dailyJobPlanList = dailyJobPlanMapper.selectList(Wrappers.<DailyJobPlan>lambdaQuery()
                .eq(DailyJobPlan::getJobId, jobId).orderByAsc(DailyJobPlan::getId));

        log.info("E|DailyServiceImpl|getDailyJobPlanListByJobId()|根据日报id查询日报日程列表，日报id = {}", jobId);

        return dailyJobPlanList;
    }

    @Override
    public JsonResult myPageDailyList(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyServiceImpl|myPageDailyList()|查询我发出的日报列表，获取当前登录人失败！");
            return JsonResult.fail("查询我发出的日报列表，获取当前登录人失败！");
        }
        log.info("E|DailyServiceImpl|myPageDailyList()|查询我发出的日报列表，请求参数【queryParamDTO = {}】", queryParamDTO);

        String userName = user.getUserName();
        String createUser = queryParamDTO.getCreateUser();
        String queryCreateUser = Objects.isNull(createUser) ? userName : createUser;
        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();

        try {
            PageInfo<DailyJob> pageInfo = PageHelper.startPage(pageNum, pageSize, "id desc")
                    .doSelectPageInfo(() -> dailyJobMapper.listByCreateUser(queryCreateUser));
            // 封装日报返回格式
            DailyVO dailyVO = this.getDailyVO(pageInfo, userName);

            return JsonResult.ok(dailyVO);
        } catch (Exception e) {
            log.error("E|DailyServiceImpl|myPageDailyList()|查询我发出的日报列表失败！【原因 = {}】", e.getMessage());
        }

        return JsonResult.fail("查询我发出的日报列表失败！");
    }

    @Override
    public JsonResult maPageDailyList(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyServiceImpl|maPageDailyList()|分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签，获取当前登录人失败！");
            return JsonResult.fail("分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签，获取当前登录人失败！");
        }
        log.info("E|DailyServiceImpl|maPageDailyList()|分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签，请求参数【queryParamDTO = {}】", queryParamDTO);
        // 当前登录人手机号
        String userName = user.getUserName();
        // 日报创建人手机号
        String createUser = queryParamDTO.getCreateUser();
        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();
        String startDate = queryParamDTO.getStartDate();
        String endDate = queryParamDTO.getEndDate();

        try {
            PageInfo<DailyJob> pageInfo = PageHelper.startPage(pageNum, pageSize, "id desc")
                    .doSelectPageInfo(() -> dailyJobMapper
                            .selectAllDailyListLikeDeptId(createUser, CommonConstants.RD_CENTER_DEPT_ID, startDate, endDate));
            // 封装日报返回格式
            DailyVO dailyVO = this.getDailyVO(pageInfo, userName);
            return JsonResult.ok(dailyVO);
        } catch (Exception e) {
            log.error("E|DailyServiceImpl|maPageDailyList()|分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签失败！【原因 = {}】", e.getMessage());
        }

        return JsonResult.fail("分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签失败！");
    }

    @Override
    public JsonResult initNoReadCount(User user, String createUser, String startDate, String endDate) {

        if (user == null) {
            log.error("E|DailyServiceImpl|maPageDailyList()|查询研发中心各部门下的未读日报数量，用于查看日报中页面初始化未读数量显示，获取当前登录人失败！");
            return JsonResult.fail("查询研发中心各部门下的未读日报数量，用于查看日报中页面初始化未读数量显示，获取当前登录人失败！");
        }

        // 当前登录人手机号
        String userName = user.getUserName();

        try {
            NoReadCount noReadCount = new NoReadCount();
            // 各个部门的日报未读数量与部门的对应关系列表
            this.assembleDeptNoReadCount(user, userName, noReadCount, createUser, startDate, endDate);
            // 部门经理、项目经理、技术专家三类人员的日报未读数量
            this.assemblePositionNoReadCount(userName, noReadCount, createUser, startDate, endDate);
            // 团队未读数量日报
            this.assembleTeamNoReadCount(user, noReadCount, createUser, startDate, endDate);
            // 当前登录人所有未读数量
            this.assembleAllNoReadCount(user, noReadCount, createUser, startDate, endDate);

            return JsonResult.ok(noReadCount);
        } catch (Exception e) {
            log.error("E|DailyServiceImpl|maPageDailyList()|查询研发中心各部门下的未读日报数量，用于查看日报中页面初始化未读数量显示失败！【原因 = {}】", e.getMessage());
        }

        return JsonResult.fail("初始化研发中心各部门下的未读日报数量失败！");
    }

    /**
     * 封装当前登录人所有未读（包含自己未读自己的）的日报数量(马总查看时不会展示未读标签)
     *
     * @param user        当前登录人
     * @param noReadCount 返回前端的对象
     */
    private void assembleAllNoReadCount(User user, NoReadCount noReadCount, String createUser, String startDate, String endDate) {

        String userName = user.getUserName();
        Long allNoReadCount = 0L;
        DailyRoleConfig roleConfigByUser = dailyRoleConfigMapper.getDailRoleConfigByUser(userName);
        // 如果不等于空，则表示当前登录用户是部门经理或者CTO助理或者是CTO
        if (roleConfigByUser != null) {
            Integer roleRank = roleConfigByUser.getRoleRank();
            String deptId = roleConfigByUser.getDeptId();
            // CTO或者CTO助理
            if (NumberConstants.NUMBER_ONE == roleRank || NumberConstants.NUMBER_TWO == roleRank) {
                allNoReadCount = dailyJobMapper.selectAllNoReadCount(userName, CommonConstants.RD_CENTER_DEPT_ID, createUser, startDate, endDate);
            } else {
                // 部门经理
                allNoReadCount = dailyJobMapper.selectAllNoReadCount(userName, deptId, createUser, startDate, endDate);
            }
        }

        String agentSid = user.getAgentSid();
        List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.getRoleConfigList();
        for (DailyRoleConfig dailyRoleConfig : roleConfigList) {
            String roleConfigDeptId = dailyRoleConfig.getDeptId();
            Integer roleRank = dailyRoleConfig.getRoleRank();
            if (NumberConstants.NUMBER_THREE == roleRank && agentSid.contains(roleConfigDeptId)) {
                allNoReadCount = dailyJobMapper.selectAllNoReadCount(userName, roleConfigDeptId, createUser, startDate, endDate);
            }
        }

        // 查询不是当前登录人创建的，并且是当前登录人所在项目成员列表的日报
        noReadCount.setNoReadCount(allNoReadCount);
    }

    /**
     * 封装当前登录人未读的团队日报数量
     *
     * @param user        当前登录人
     * @param noReadCount 返回前端的对象
     */
    private void assembleTeamNoReadCount(User user, NoReadCount noReadCount, String createUser, String startDate, String endDate) {

        String userName = user.getUserName();
        List<DailyTeamMember> memberList = dailyTeamMemberMapper.selectList(Wrappers.<DailyTeamMember>lambdaQuery()
                .eq(DailyTeamMember::getUserName, userName));

        Set<String> usernameSet = new HashSet<>();
        // 得到当前登录人所在的所有项目id集合
        List<Integer> projectIdList = memberList.stream().map(DailyTeamMember::getProjectId).collect(Collectors.toList());
        for (Integer projectId : projectIdList) {
            List<DailyTeamMember> teamMemberList = dailyTeamMemberMapper.selectList(Wrappers.<DailyTeamMember>lambdaQuery()
                    .eq(DailyTeamMember::getProjectId, projectId));
            List<String> dailyUsernameList = teamMemberList.stream().map(DailyTeamMember::getUserName).collect(Collectors.toList());
            usernameSet.addAll(dailyUsernameList);
        }

        Long teamNoReadCount = 0L;
        // 查询不是当前登录人创建的，并且是当前登录人所在项目成员列表的日报
        if (usernameSet.size() > 0) {
            teamNoReadCount = dailyJobMapper.selectTeamCountInUsernameList(userName, usernameSet, createUser, startDate, endDate);
        }

        noReadCount.setTeamNoReadCount(teamNoReadCount);
    }

    /**
     * 封装每个部门下的日报未读数量
     *
     * @param user        当前登录人
     * @param userName    当前登录人手机号
     * @param noReadCount 返回前端的对象
     * @param createUser  日报创建人手机号
     */
    private void assembleDeptNoReadCount(User user, String userName, NoReadCount noReadCount, String createUser, String startDate, String endDate) {

        // 得到部门列表
        JsonResult jsonResult = dailyRoleConfigService.getDeptList(user);
        List<DailyRoleConfig> dailyRoleConfigList = (List<DailyRoleConfig>) jsonResult.getResult();
        List<DeptAndCount> deptAndCountList = new ArrayList<>();
        // 遍历部门列表，封装部门与部门下的未读日报数量
        for (DailyRoleConfig dailyRoleConfig : dailyRoleConfigList) {
            String roleConfigDeptId = dailyRoleConfig.getDeptId();
            String deptName = dailyRoleConfig.getDeptName();
            Long deptCount = dailyJobMapper.selectDeptCountLikeDeptId(userName, roleConfigDeptId, createUser, startDate, endDate);
            DeptAndCount deptAndCount = new DeptAndCount();
            deptAndCount.setDeptId(roleConfigDeptId);
            deptAndCount.setDeptName(deptName);
            deptAndCount.setNoReadCount(deptCount);
            deptAndCountList.add(deptAndCount);
        }
        noReadCount.setDeptAndCountList(deptAndCountList);
    }

    /**
     * 封装职位类型日报未读数量
     *
     * @param username    当前登录人手机号
     * @param noReadCount 返回前端的对象
     */
    private void assemblePositionNoReadCount(String username, NoReadCount noReadCount, String createUser, String startDate, String endDate) {

        List<DailyPositionConfig> positionConfigList = dailyPositionMapper.selectList(Wrappers.<DailyPositionConfig>lambdaQuery()
                .eq(DailyPositionConfig::getIsDel, 1));
        // 得到职位类型集合
        Set<Integer> positionTypeSet = new HashSet<>();
        for (DailyPositionConfig dailyPositionConfig : positionConfigList) {
            Integer positionType = dailyPositionConfig.getPositionType();
            positionTypeSet.add(positionType);
        }

        List<PositionAndCount> positionAndCountList = new ArrayList<>();
        // 遍历得到每个职位类型下的未读数量
        for (Integer positionType : positionTypeSet) {
            Long positionCount = dailyJobMapper.selectPositionCountByCreateUser(username, positionType, createUser, startDate, endDate);
            PositionAndCount positionAndCount = new PositionAndCount();
            positionAndCount.setPositionType(positionType);
            positionAndCount.setNoReadCount(positionCount);
            String position = dailyPositionMapper.getPositionByPositionType(positionType);
            positionAndCount.setPosition(position);
            positionAndCountList.add(positionAndCount);
        }
        noReadCount.setPositionAndCountList(positionAndCountList);
    }

    @Override
    public JsonResult getLastDailyUndoneList(User user, Integer jobId) {

        List<DailyJobPlan> undoneDailyJobPlanList = this.getUndoneDailyJobPlanListByJobId(jobId);

        return JsonResult.ok(undoneDailyJobPlanList);
    }

    @Override
    public List<String> selectUsernameListByCreateTime() {

        List<String> usernameList = new ArrayList<>();
        try {
            String format = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
            usernameList = dailyJobMapper.selectUsernameListByCreateTime();
            log.info("E|DailyServiceImpl|selectUsernameListByCreateTime()|获取截止到22点已提交的日报手机号列表，【截止到时间 = {}，已提交日报的人员列表 = {}】！", format, usernameList);
        } catch (Exception e) {
            log.error("E|DailyServiceImpl|selectUsernameListByCreateTime()|获取截止到22点已提交的日报手机号列表失败，【原因 = {}】", e.getMessage());
        }

        return usernameList;
    }
}
