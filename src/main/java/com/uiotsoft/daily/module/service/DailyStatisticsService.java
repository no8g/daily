package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyStatisticsService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月24日 10:03</p>
 * <p>@remark：</p>
 */
public interface DailyStatisticsService {

    /**
     * 获取日报未提交人员列表，用于Portal页面日报统计未提交人员列表
     *
     * @param user 当前登录人
     * @return 未提交人员列表
     */
    JsonResult getNoSubmitList(User user);

    /**
     * 分页查询日报未提交人员列表，用于日报统计页面未提交人员列表
     *
     * @param user          登录用户
     * @param queryParamDTO 分页查询参数
     * @return
     */
    JsonResult pageList(User user, QueryParamDTO queryParamDTO);

    /**
     * 获取所有部门列表，用于日报统计页面未提交人员列表的部门查询
     *
     * @param user 登录用户
     * @return 所有部门列表
     */
    JsonResult getAllDeptList(User user);

    /**
     * 导出EXCEL
     *
     * @param user          当前登录用户
     * @param queryParamDTO 查询参数
     * @return 分页对象
     */
    JsonResult exportExcel(User user, QueryParamDTO queryParamDTO);
}
