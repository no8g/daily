package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.entity.DailyJobCommentReply;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyCommentReplyService 此接口用于：日报评论回复服务层</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 9:53</p>
 * <p>@remark：</p>
 */
public interface DailyCommentReplyService {

    /**
     * 新增评论回复
     *
     * @param user                 当前用户
     * @param dailyJobCommentReply 评论回复内容
     * @return 是否回复成功
     */
    JsonResult addReply(User user, DailyJobCommentReply dailyJobCommentReply);
}
