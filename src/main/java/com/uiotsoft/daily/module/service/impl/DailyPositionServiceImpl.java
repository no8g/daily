package com.uiotsoft.daily.module.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dao.DailyJobMapper;
import com.uiotsoft.daily.module.dao.DailyPositionMapper;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyJob;
import com.uiotsoft.daily.module.entity.DailyPositionConfig;
import com.uiotsoft.daily.module.service.DailyPositionService;
import com.uiotsoft.daily.module.service.DailyService;
import com.uiotsoft.daily.module.vo.DailyVO;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>DailyPositionServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月03日 10:00</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyPositionServiceImpl extends ServiceImpl<DailyPositionMapper, DailyPositionConfig> implements DailyPositionService {

    @Resource
    private DailyPositionMapper dailyPositionMapper;

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Resource
    private DailyService dailyService;

    @Override
    public JsonResult pageDailyList(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyPositionServiceImpl|pageDailyList()|分页查询部门经理、项目经理、技术专家等日报时，获取当前登录人失败！");
            return JsonResult.fail("分页查询部门经理、项目经理、技术专家等日报时，获取当前登录人失败！");
        }

        String userName = user.getUserName();
        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();
        Integer positionType = queryParamDTO.getPositionType();
        String deptId = queryParamDTO.getDeptId();
        String createUser = queryParamDTO.getCreateUser();
        String startDate = queryParamDTO.getStartDate();
        String endDate = queryParamDTO.getEndDate();

        try {
            List<DailyPositionConfig> positionConfigList = dailyPositionMapper.selectList(Wrappers.<DailyPositionConfig>lambdaQuery()
                    .eq(DailyPositionConfig::getPositionType, positionType)
                    .eq(DailyPositionConfig::getIsDel, 1));

            log.info("E|DailyPositionServiceImpl|pageDailyList()|分页查询部门经理、项目经理、技术专家等日报时，获取到的职位列表【positionConfigList = {}】", positionConfigList);
            List<String> usernameList = positionConfigList.stream().map(DailyPositionConfig::getUserName).collect(Collectors.toList());

            PageInfo<DailyJob> pageInfo = PageHelper.startPage(pageNum, pageSize, "id desc")
                    .doSelectPageInfo(() -> dailyJobMapper
                            .listInUsernameList(usernameList, deptId, createUser, userName, startDate, endDate));

            DailyVO dailyVO = dailyService.getDailyVO(pageInfo, userName);

            return JsonResult.ok(dailyVO);
        } catch (Exception e) {
            log.error("E|DailyPositionServiceImpl|pageDailyList()|分页查询部门经理、项目经理、技术专家等日报失败，原因【e = {}】", e.getMessage());
        }

        return JsonResult.fail("分页查询部门经理、项目经理、技术专家等日报失败！");
    }

    @Override
    public JsonResult pageList(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyPositionServiceImpl|pageList()|分页查询部门经理、项目经理、技术专家等职位列表时，获取当前登录人失败！");
            return JsonResult.fail("分页查询部门经理、项目经理、技术专家等职位列表时，获取当前登录人失败！");
        }

        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();

        try {
            PageInfo<DailyPositionConfig> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(
                    () -> dailyPositionMapper.selectList(Wrappers.<DailyPositionConfig>lambdaQuery()
                            .eq(DailyPositionConfig::getIsDel, 1))
            );
            log.info("E|DailyPositionServiceImpl|pageList()|分页查询部门经理、项目经理、技术专家等职位列表成功！");
            return JsonResult.ok(pageInfo);
        } catch (Exception e) {
            log.error("E|DailyPositionServiceImpl|pageList()|分页查询部门经理、项目经理、技术专家等职位列表时失败！【原因 = {}】", e.getMessage());
        }

        return JsonResult.fail("分页查询部门经理、项目经理、技术专家等职位列表失败！");
    }

    @Override
    public JsonResult addPositionConfig(User user, DailyPositionConfig dailyPositionConfig) {

        if (user == null) {
            log.error("E|DailyPositionServiceImpl|addPositionConfig()|添加部门经理、项目经理、技术专家职位时，获取当前登录人失败！");
            return JsonResult.fail("添加部门经理、项目经理、技术专家职位时，获取当前登录人失败！");
        }
        String userName = user.getUserName();
        String trueName = user.getTrueName();
        dailyPositionConfig.setIsDel(1);
        dailyPositionConfig.setCreateUser(userName);
        dailyPositionConfig.setCreateName(trueName);
        dailyPositionConfig.setCreateTime(new Date());

        try {
            int insert = dailyPositionMapper.insert(dailyPositionConfig);
            if (insert > 0) {
                log.info("E|DailyPositionServiceImpl|addPositionConfig()|" +
                        "添加部门经理、项目经理、技术专家职位时成功，【操作人：userName = {}, trueName = {}】", userName, trueName);

                return JsonResult.ok("添加部门经理、项目经理、技术专家职位成功！");
            }
        } catch (Exception e) {
            log.error("E|DailyPositionServiceImpl|addPositionConfig()|" +
                    "添加部门经理、项目经理、技术专家职位时失败，【原因：e = {}】", e.getMessage());
        }

        return JsonResult.fail("添加部门经理、项目经理、技术专家职位失败！");
    }

    @Override
    public JsonResult deletePositionConfig(User user, Integer positionId) {

        if (user == null) {
            log.error("E|DailyPositionServiceImpl|deletePositionConfig()|删除部门经理、项目经理、技术专家等职位时，获取当前登录人失败！");
            return JsonResult.fail("删除部门经理、项目经理、技术专家等职位时，获取当前登录人失败！");
        }

        String userName = user.getUserName();
        String trueName = user.getTrueName();

        try {
            DailyPositionConfig dailyPositionConfig = dailyPositionMapper.selectById(positionId);
            dailyPositionConfig.setIsDel(-1);
            int update = dailyPositionMapper.updateById(dailyPositionConfig);
            if (update > 0) {
                log.info("E|DailyPositionServiceImpl|deletePositionConfig()|" +
                        "删除部门经理、项目经理、技术专家职位时成功，【操作人：userName = {}, trueName = {}】", userName, trueName);

                return JsonResult.ok("删除部门经理、项目经理、技术专家职位成功！");
            }
        } catch (Exception e) {
            log.error("E|DailyPositionServiceImpl|deletePositionConfig()|" +
                    "删除部门经理、项目经理、技术专家职位时失败，【原因：e = {}】", e.getMessage());
        }


        return JsonResult.fail("删除部门经理、项目经理、技术专家职位时失败！");
    }

    @Override
    public JsonResult getPositionConfigInfo(User user, Integer positionId) {

        if (user == null) {
            log.error("E|DailyPositionServiceImpl|getPositionConfigInfo()|获取部门经理、项目经理、技术专家等职位详情时，获取当前登录人失败！");
            return JsonResult.fail("获取部门经理、项目经理、技术专家等职位详情时，获取当前登录人失败！");
        }

        DailyPositionConfig dailyPositionConfig = dailyPositionMapper.selectById(positionId);
        log.info("E|DailyPositionServiceImpl|getPositionConfigInfo()|获取部门经理、项目经理、技术专家等职位详情，详情内容为 = {}", dailyPositionConfig);

        return JsonResult.ok(dailyPositionConfig);
    }

    @Override
    public JsonResult updatePositionConfig(User user, DailyPositionConfig dailyPositionConfig) {

        if (user == null) {
            log.error("E|DailyPositionServiceImpl|updatePositionConfig()|更新部门经理、项目经理、技术专家等职位时，获取当前登录人失败！");
            return JsonResult.fail("更新部门经理、项目经理、技术专家等职位时，获取当前登录人失败！");
        }

        String userName = user.getUserName();
        String trueName = user.getTrueName();

        try {
            int update = dailyPositionMapper.updateById(dailyPositionConfig);
            if (update > 0) {
                log.info("E|DailyPositionServiceImpl|updatePositionConfig()|" +
                        "更新部门经理、项目经理、技术专家职位时成功，【操作人：userName = {}, trueName = {}】", userName, trueName);

                return JsonResult.ok("更新部门经理、项目经理、技术专家职位成功！");
            }
        } catch (Exception e) {
            log.error("E|DailyPositionServiceImpl|updatePositionConfig()|" +
                    "更新部门经理、项目经理、技术专家职位时失败，【原因：e = {}】", e.getMessage());
        }

        return JsonResult.fail("更新部门经理、项目经理、技术专家职位时失败！");
    }
}
