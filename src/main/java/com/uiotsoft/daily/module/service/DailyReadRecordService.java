package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyReadRecordService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 8:50</p>
 * <p>@remark：</p>
 */
public interface DailyReadRecordService {

    /**
     * 添加日报已读记录
     *
     * @param user       当前用户
     * @param dailyJobId 日报id
     * @return 是否不回成功
     */
    JsonResult addReadRecord(User user, Integer dailyJobId);

    /**
     * 分页查询日报已读列表
     *
     * @param user          当前用户
     * @param queryParamDTO 查询参数
     * @return 分页查询日报已读列表
     */
    JsonResult pageReadRecord(User user, QueryParamDTO queryParamDTO);

    /**
     * 日报未读列表（分页）
     *
     * @param user          当前用户
     * @param queryParamDTO 查询参数
     * @return 日报未读列表（分页）
     */
    JsonResult pageNoReadRecord(User user, QueryParamDTO queryParamDTO);
}
