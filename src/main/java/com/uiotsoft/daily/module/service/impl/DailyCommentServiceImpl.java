package com.uiotsoft.daily.module.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.dingding.domain.DingTalkMsg;
import com.uiotsoft.daily.dingding.util.DingDingMsg;
import com.uiotsoft.daily.dingding.util.DingTalkWorkNotice;
import com.uiotsoft.daily.module.dao.DailyCommentMapper;
import com.uiotsoft.daily.module.dao.DailyCommentReplyMapper;
import com.uiotsoft.daily.module.dao.DailyJobMapper;
import com.uiotsoft.daily.module.dao.DailyViewUserMapper;
import com.uiotsoft.daily.module.entity.DailyJob;
import com.uiotsoft.daily.module.entity.DailyJobComment;
import com.uiotsoft.daily.module.entity.DailyJobCommentReply;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import com.uiotsoft.daily.module.service.DailyCommentService;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>DailyCommentServiceImpl 此类用于：日报评论服务层实现</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 9:35</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyCommentServiceImpl extends ServiceImpl<DailyCommentMapper, DailyJobComment> implements DailyCommentService {

    @Resource
    private DailyCommentMapper dailyCommentMapper;

    @Resource
    private DailyCommentReplyMapper dailyCommentReplyMapper;

    @Resource
    private DingTalkWorkNotice dingTalkWorkNotice;

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Override
    public JsonResult addComment(User user, DailyJobComment dailyJobComment) {

        if (user == null) {
            log.error("E|DailyCommentServiceImpl|addComment()|新增日报评论时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String userName = user.getUserName();
        String trueName = user.getTrueName();
        dailyJobComment.setCreateUser(userName);
        dailyJobComment.setCreateName(trueName);
        dailyJobComment.setCreateTime(new Date());

        int insert = dailyCommentMapper.insert(dailyJobComment);
        if (insert > 0) {
            log.info("E|DailyCommentServiceImpl|addComment()|新增日报评论成功，当前登录人 trueName = {}", trueName);
            this.executeDingInform(dailyJobComment);
            return JsonResult.ok("新增日报评论成功");
        }

        return JsonResult.fail("新增日报评论失败！");
    }

    /**
     * 执行钉钉通知
     *
     * @param dailyJobComment 日报评论内容
     */
    private void executeDingInform(DailyJobComment dailyJobComment) {

        // 获取钉钉用户id
        Integer jobId = dailyJobComment.getJobId();
        DailyJob dailyJob = dailyJobMapper.selectById(jobId);
        DailyViewUser dailyViewUser = dailyViewUserMapper.getDailyViewUserByUsername(dailyJob.getCreateUser());
        String userName = dailyViewUser.getUserName();
        String dingUserId = dailyViewUser.getDingUserId();
        String createName = dailyJobComment.getCreateName();
        // 评论创建时间
        Date createTime = dailyJobComment.getCreateTime();
        ThreadUtil.excAsync(new Runnable() {
            @Override
            public void run() {
                DingTalkMsg dingTalkMsg = new DingTalkMsg();
                dingTalkMsg.setAuthor(createName + " " + DateUtil.format(createTime, "yyyy年MM月dd日 HH:mm:ss"));
                dingTalkMsg.setContent("");
                dingTalkMsg.setTitle("日报评论通知");
                dingTalkMsg.setMsgType("oa");
                dingTalkMsg.setUserList(Objects.isNull(dingUserId) ? userName : dingUserId);
                dingTalkMsg.setFromList(getFormList());
                dingTalkWorkNotice.sendDingMsg(dingTalkMsg);
            }

            private List<OapiMessageCorpconversationAsyncsendV2Request.Form> getFormList() {
                List<OapiMessageCorpconversationAsyncsendV2Request.Form> formList = DingDingMsg.create("【" + createName + "】评论了你【",
                        DateUtil.format(dailyJob.getCreateTime(), "yyyy-MM-dd HH:mm:ss") + "】创建的日报")
                        .builder("评论内容：", dailyJobComment.getContent())
                        .builder("评论时间：", DateUtil.format(createTime, "yyyy-MM-dd HH:mm:ss"))
                        .collect();

                log.info("E|DailyCommentServiceImpl|getDingInformContent()|新增日报评论成功时，封装钉钉通知内容 = {}", formList);

                return formList;
            }

        }, true);
    }

    @Override
    public JsonResult getCommentList(User user, Integer jobId) {

        if (user == null) {
            log.error("E|DailyCommentServiceImpl|getCommentList()|查询日报评论列表时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        List<DailyJobComment> commentList = dailyCommentMapper.selectList(Wrappers.<DailyJobComment>lambdaQuery()
                .eq(DailyJobComment::getJobId, jobId).orderByAsc(DailyJobComment::getId));

        // 添加回复
        this.getCommentReplyList(commentList);

        return JsonResult.ok(commentList);
    }

    /**
     * 日报评论添加回复列表
     *
     * @param commentList 评论列表
     */
    private void getCommentReplyList(List<DailyJobComment> commentList) {

        for (DailyJobComment dailyJobComment : commentList) {
            Integer commentId = dailyJobComment.getId();
            List<DailyJobCommentReply> replyList = dailyCommentReplyMapper.selectList(Wrappers.<DailyJobCommentReply>lambdaQuery()
                    .eq(DailyJobCommentReply::getCommentId, commentId).orderByAsc(DailyJobCommentReply::getId));
            dailyJobComment.setReplyList(replyList);
        }
    }
}
