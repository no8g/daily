package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.dto.TeamMemberDTO;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyTeamMemberService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 15:01</p>
 * <p>@remark：</p>
 */
public interface DailyTeamMemberService {

    /**
     * 批量添加项目成员
     *
     * @param user          当前登录人
     * @param teamMemberDTO 项目成员列表
     * @return 是否成功
     */
    JsonResult addBatch(User user, TeamMemberDTO teamMemberDTO);

    /**
     * 批量修改项目成员
     *
     * @param user          当前登录人
     * @param teamMemberDTO 项目成员列表
     * @return 是否成功
     */
    JsonResult updateTeamMember(User user, TeamMemberDTO teamMemberDTO);

    /**
     * 分页查看团队成员日报接口
     *
     * @param user          当前登录人
     * @param queryParamDTO 查询参数
     * @return 团队成员日报列表
     */
    JsonResult pageList(User user, QueryParamDTO queryParamDTO);
}
