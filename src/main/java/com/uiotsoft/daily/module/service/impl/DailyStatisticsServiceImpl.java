package com.uiotsoft.daily.module.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.common.constant.NumberConstants;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.util.ExcelUtil;
import com.uiotsoft.daily.module.dao.DailyJobMapper;
import com.uiotsoft.daily.module.dao.DailyNoSubmitReportMapper;
import com.uiotsoft.daily.module.dao.DailyRoleConfigMapper;
import com.uiotsoft.daily.module.dao.DailyViewUserMapper;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyJob;
import com.uiotsoft.daily.module.entity.DailyNoSubmitReport;
import com.uiotsoft.daily.module.entity.DailyRoleConfig;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import com.uiotsoft.daily.module.service.DailyStatisticsService;
import com.uiotsoft.daily.module.vo.DailyStatisticsVO;
import com.uiotsoft.daily.module.vo.DailyUserVO;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>DailyStatisticsServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月24日 10:05</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyStatisticsServiceImpl implements DailyStatisticsService {

    @Resource
    private DailyNoSubmitReportMapper dailyNoSubmitReportMapper;

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Resource
    private DailyRoleConfigMapper dailyRoleConfigMapper;

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Override
    public JsonResult getNoSubmitList(User user) {

        if (user == null) {
            log.error("E|DailyStatisticsServiceImpl|getNoSubmitList()|获取日报未提交人员列表，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        log.info("E|DailyStatisticsServiceImpl|getNoSubmitList()|获取日报未提交人员列表，当前登录人【user = {}】", user);

        // 当前登录人用户名
        String userName = user.getUserName();
        DailyRoleConfig dailyRoleConfig = dailyRoleConfigMapper.getDailRoleConfigByUser(userName);
        DailyStatisticsVO dailyStatisticsVo = new DailyStatisticsVO();
        String queryDate = this.getQueryDate();
        // 当前角色为总监/总监助理/部门经理
        if (dailyRoleConfig != null) {
            Integer roleRank = dailyRoleConfig.getRoleRank();
            String roleConfigDeptId = dailyRoleConfig.getDeptId();

            if (userName.equals(CommonConstants.RD_CENTER_CTO_TEL)) {
                // 得到已提交日报的人员列表
                List<DailyJob> dailyJobList = dailyJobMapper.selectListLikeDeptIdAndCreateTime(CommonConstants.RD_CENTER_DEPT_ID, queryDate);
                dailyStatisticsVo = this.assembleDailyStatisticsVo(dailyJobList, CommonConstants.RD_CENTER_DEPT_ID);
            }
            if (roleRank == NumberConstants.NUMBER_TWO) {
                // 得到已提交日报的人员列表
                List<DailyJob> dailyJobList = dailyJobMapper.selectListLikeDeptIdAndCreateTime(roleConfigDeptId, queryDate);
                dailyStatisticsVo = this.assembleDailyStatisticsVo(dailyJobList, roleConfigDeptId);
            }
            if (roleRank == NumberConstants.NUMBER_THREE) {
                // 得到已提交日报的人员列表
                List<DailyJob> dailyJobList = dailyJobMapper.selectListLikeDeptIdAndCreateTime(roleConfigDeptId, queryDate);
                dailyStatisticsVo = this.assembleDailyStatisticsVo(dailyJobList, roleConfigDeptId);
            }

            return JsonResult.ok(dailyStatisticsVo);
        }

        // 当前角色为普通员工
        String agentSid = user.getAgentSid();
        List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.getRoleConfigList();
        for (DailyRoleConfig roleConfig : roleConfigList) {
            Integer roleRank = roleConfig.getRoleRank();
            String deptId = roleConfig.getDeptId();
            if (roleRank == NumberConstants.NUMBER_THREE && agentSid.contains(deptId)) {
                // 得到已提交日报的人员列表
                List<DailyJob> dailyJobList = dailyJobMapper.selectListLikeDeptIdAndCreateTime(deptId, queryDate);
                dailyStatisticsVo = this.assembleDailyStatisticsVo(dailyJobList, deptId);
                return JsonResult.ok(dailyStatisticsVo);
            }

        }

        return JsonResult.fail("获取日报未提交人员列表失败！");
    }

    /**
     * 封装日报统计VO
     *
     * @param dailyJobList 已提交日报人员列表
     * @param deptId       部门id
     * @return 日报统计VO
     */
    private DailyStatisticsVO assembleDailyStatisticsVo(List<DailyJob> dailyJobList, String deptId) {

        DailyStatisticsVO dailyStatisticsVo = new DailyStatisticsVO();
        // 得到当前部门下的所有需要写日报的人员列表
        List<DailyViewUser> dailyViewUserList = dailyViewUserMapper.selectListLikeDeptId(deptId, null);
        // 所有人员(CopyOnWriteArrayList线程安全删除 重复的)
        CopyOnWriteArrayList<DailyViewUser> viewUserList = new CopyOnWriteArrayList<>(dailyViewUserList);
        for (DailyJob dailyJob : dailyJobList) {
            // 从所有需要写日报的人员列表中删除已提交日报的人员，得到未提交日报人员列表
            viewUserList.removeIf(dailyViewUser -> dailyJob.getCreateUser().equals(dailyViewUser.getUserName()));
        }
        // 未提交日报人数
        int noSubmitCount = viewUserList.size();
        dailyStatisticsVo.setNoSubmit(noSubmitCount);
        int haveSubmitCount = dailyViewUserList.size() - noSubmitCount;
        dailyStatisticsVo.setHaveSubmit(haveSubmitCount);
        // 遍历未提交人员列表
        List<DailyUserVO> dailyUserVOList = new ArrayList<>();
        for (DailyViewUser dailyViewUser : viewUserList) {
            DailyUserVO dailyUserVO = new DailyUserVO();
            dailyUserVO.setUserName(dailyViewUser.getUserName());
            dailyUserVO.setTrueName(dailyViewUser.getTrueName());
            dailyUserVOList.add(dailyUserVO);
        }
        dailyStatisticsVo.setNoSubmitList(dailyUserVOList);

        return dailyStatisticsVo;
    }

    /**
     * 获取查询日期
     *
     * @return 查询日期
     */
    private String getQueryDate() {

        LocalTime now = LocalTime.now();
        LocalTime startTime = LocalTime.of(18, 0, 0);
        LocalTime endTime = LocalTime.of(23, 59, 59);

        String queryDate = DateUtil.formatDate(DateUtil.yesterday());
        // 查询日期如果在18点到24点之间，查询今天已提交的日报，否则就查询昨天已提交的日报
        if (now.isAfter(startTime) && now.isBefore(endTime)) {
            queryDate = DateUtil.formatDate(new Date());
        }

        return queryDate;
    }

    @Override
    public JsonResult pageList(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyStatisticsServiceImpl|pageList()|分页查询日报未提交人员列表时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }
        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();

        log.info("E|DailyStatisticsServiceImpl|pageList()|分页查询日报未提交人员列表时，查询参数为【queryParamDTO = {}】", queryParamDTO);
        try {
            PageInfo<Object> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() ->
                    dailyNoSubmitReportMapper.pageList(queryParamDTO));
            return JsonResult.ok(pageInfo);
        } catch (Exception e) {
            log.error("E|DailyStatisticsServiceImpl|pageList()|分页查询日报未提交人员列表失败！原因 = {}", e.getMessage());
        }

        return JsonResult.fail("分页查询日报未提交人员列表失败！");
    }

    @Override
    public JsonResult getAllDeptList(User user) {

        if (user == null) {
            log.error("E|DailyStatisticsServiceImpl|getAllDeptList()|获取所有部门列表，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String userName = user.getUserName();
        String trueName = user.getTrueName();
        List<DailyRoleConfig> dailyRoleConfigList = dailyRoleConfigMapper.selectList(Wrappers.<DailyRoleConfig>lambdaQuery()
                .eq(DailyRoleConfig::getFlag, 1).ne(DailyRoleConfig::getRoleRank, 1));

        log.info("E|DailyStatisticsServiceImpl|getAllDeptList()|获取所有部门列表，当前登录人【userName = {}, trueName = {}】", userName, trueName);
        return JsonResult.ok(dailyRoleConfigList);
    }

    @Override
    public JsonResult exportExcel(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyStatisticsServiceImpl|exportExcel()|分页导出Excel日报未提交人员列表时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }
        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();

        try {

            PageInfo<DailyNoSubmitReport> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() ->
                    dailyNoSubmitReportMapper.pageList(queryParamDTO));

            List<DailyNoSubmitReport> reportList = pageInfo.getList();
            if (reportList.size() > 0) {
                ExcelUtil.exports2007("日报未提交人员列表", pageInfo.getList());
                return JsonResult.ok("导出日报未提交人员列表成功！");
            }
            return JsonResult.fail("暂时没有数据，请改变筛选条件后重试！");
        } catch (Exception e) {
            log.error("E|DailyStatisticsServiceImpl|exportExcel()|分页导出Excel日报未提交人员列表失败！原因 = {}", e.getMessage());
        }

        return JsonResult.fail("分页导出Excel日报未提交人员列表时失败！");
    }
}
