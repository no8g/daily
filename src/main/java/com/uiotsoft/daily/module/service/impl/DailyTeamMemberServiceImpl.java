package com.uiotsoft.daily.module.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dao.DailyJobMapper;
import com.uiotsoft.daily.module.dao.DailyTeamMemberMapper;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.dto.TeamMemberDTO;
import com.uiotsoft.daily.module.entity.DailyJob;
import com.uiotsoft.daily.module.entity.DailyTeamMember;
import com.uiotsoft.daily.module.service.DailyService;
import com.uiotsoft.daily.module.service.DailyTeamMemberService;
import com.uiotsoft.daily.module.vo.DailyVO;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>DailyTeamMemberServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 15:01</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyTeamMemberServiceImpl extends ServiceImpl<DailyTeamMemberMapper, DailyTeamMember> implements DailyTeamMemberService {

    @Resource
    private DailyTeamMemberMapper dailyTeamMemberMapper;

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Resource
    private DailyService dailyService;

    @Override
    public JsonResult addBatch(User user, TeamMemberDTO teamMemberDTO) {

        if (user == null) {
            log.error("E|DailyTeamMemberServiceImpl|addBatch()|批量新增项目成员时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        Integer projectId = teamMemberDTO.getProjectId();
        List<DailyTeamMember> teamMemberList = teamMemberDTO.getTeamMemberList();
        List<DailyTeamMember> memberList = teamMemberList.stream().peek(dailyTeamMember -> dailyTeamMember.setProjectId(projectId)).collect(Collectors.toList());

        int save = dailyTeamMemberMapper.saveList(memberList);
        if (save > 0) {
            log.info("E|DailyTeamMemberServiceImpl|addBatch()|批量新增项目成员时，保存成功！操作人 = 【{}】", user.getTrueName());
            return JsonResult.ok("保存项目成员成功！");
        }

        return JsonResult.fail("保存项目成员失败！");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult updateTeamMember(User user, TeamMemberDTO teamMemberDTO) {

        if (user == null) {
            log.error("E|DailyTeamMemberServiceImpl|updateTeamMember()|批量修改项目成员时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        Integer projectId = teamMemberDTO.getProjectId();
        List<DailyTeamMember> teamMemberList = teamMemberDTO.getTeamMemberList();
        List<DailyTeamMember> memberList = teamMemberList.stream().peek(dailyTeamMember -> dailyTeamMember.setProjectId(projectId)).collect(Collectors.toList());

        if (projectId == null) {
            log.error("E|DailyTeamMemberServiceImpl|updateTeamMember()|批量修改项目成员时，传入参数为空！");
            return JsonResult.fail("项目id不能为这空！");
        }

        try {
            // 先删除后新增
            dailyTeamMemberMapper.delete(Wrappers.<DailyTeamMember>lambdaQuery().eq(DailyTeamMember::getProjectId, projectId));
            int save = dailyTeamMemberMapper.saveList(memberList);
            if (save > 0) {
                log.info("E|DailyTeamMemberServiceImpl|updateTeamMember()|批量修改项目成员时，修改成功！操作人 = 【{}】", user.getTrueName());
                return JsonResult.ok("修改成功！");
            }
        } catch (Exception e) {
            log.error("E|DailyTeamMemberServiceImpl|updateTeamMember()|批量修改项目成员时，修改失败！原因 = 【{}】", e.getMessage());
        }

        return JsonResult.fail("修改失败！");
    }

    @Override
    public JsonResult pageList(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyTeamMemberServiceImpl|pageList()|分页查看团队成员日报时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();
        String createUser = queryParamDTO.getCreateUser();
        String startDate = queryParamDTO.getStartDate();
        String endDate = queryParamDTO.getEndDate();
        log.info("E|DailyTeamMemberServiceImpl|pageList()|分页查看团队成员日报时，查询参数：【queryParamDTO = {}】", queryParamDTO);

        // 当前登录人手机号
        String userName = user.getUserName();
        try {
            // 根据手机号查出用户所属团队
            List<DailyTeamMember> memberList = dailyTeamMemberMapper.selectList(Wrappers.<DailyTeamMember>lambdaQuery()
                    .eq(DailyTeamMember::getUserName, userName));
            // 查询出用户参与的所有项目id集合
            List<Integer> projectIdList = memberList.stream().map(DailyTeamMember::getProjectId).collect(Collectors.toList());

            if (projectIdList.size() > 0) {
                // 得到所有项目下所有用户列表
                List<DailyTeamMember> teamMemberList = dailyTeamMemberMapper.selectList(Wrappers.<DailyTeamMember>lambdaQuery()
                        .in(DailyTeamMember::getProjectId, projectIdList));
                // 得到所有项目下的所有用户手机号集合，并且排除掉当前登录人的手机号
                List<String> userNameList = teamMemberList.stream().map(DailyTeamMember::getUserName)
                        .filter(item -> !item.equals(userName)).collect(Collectors.toList());
                // 日报创建人，团队日报里，筛选条件使用
                if (StrUtil.isNotEmpty(createUser)) {
                    userNameList = userNameList.stream().filter(item -> item.equals(createUser)).collect(Collectors.toList());
                }

                List<String> finalUserNameList = userNameList;
                PageInfo<DailyJob> pageInfo = new PageInfo<>();
                if (userNameList.size() > 0) {

                    pageInfo = PageHelper.startPage(pageNum, pageSize, "id desc")
                            .doSelectPageInfo(() -> dailyJobMapper.selectListInUserNameList(finalUserNameList, createUser, userName, startDate, endDate));
                }

                DailyVO dailyVO = dailyService.getDailyVO(pageInfo, userName);

                return JsonResult.ok(dailyVO);
            }

            return JsonResult.ok("您暂时没有加入团队！");
        } catch (Exception e) {
            log.error("E|DailyTeamMemberServiceImpl|pageList()|分页查看团队成员日报时，查询失败！原因 = 【{}】", e.getMessage());
        }

        return JsonResult.fail("查询团队日报列表失败！");
    }
}
