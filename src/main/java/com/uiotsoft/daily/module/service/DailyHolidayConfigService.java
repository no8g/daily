package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.module.entity.DailyHolidayConfig;

import java.util.List;

/**
 * <p>DailyHolidayConfigService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月22日 20:24</p>
 * <p>@remark：</p>
 */
public interface DailyHolidayConfigService {

    /**
     * 批量插入节假日配置数据
     *
     * @param holidayConfigList 被插入的数据列表
     */
    void insertBatch(List<DailyHolidayConfig> holidayConfigList);
}
