package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyPositionConfig;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyPositionService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月03日 10:00</p>
 * <p>@remark：</p>
 */
public interface DailyPositionService {

    /**
     * 分页查询部门经理、项目经理、技术专家等日报
     *
     * @param user          当前登录用户
     * @param queryParamDTO 查询参数
     * @return 分页日报列表
     */
    JsonResult pageDailyList(User user, QueryParamDTO queryParamDTO);

    /**
     * 分页查询职位列表，用于职位管理中部门经理、项目经理、技术专家等职位的配置列表显示
     *
     * @param user          当前登录用户
     * @param queryParamDTO 查询参数
     * @return 分页查询职位列表
     */
    JsonResult pageList(User user, QueryParamDTO queryParamDTO);

    /**
     * 添加职位配置信息
     *
     * @param user                当前登录用户
     * @param dailyPositionConfig 职位配置信息
     * @return 是否成功
     */
    JsonResult addPositionConfig(User user, DailyPositionConfig dailyPositionConfig);

    /**
     * 逻辑删除职位信息
     *
     * @param user       当前登录用户
     * @param positionId 职位id
     * @return 是否成功
     */
    JsonResult deletePositionConfig(User user, Integer positionId);

    /**
     * 获取职位详情
     *
     * @param user       当前登录用户
     * @param positionId 职位id
     * @return 职位详情
     */
    JsonResult getPositionConfigInfo(User user, Integer positionId);

    /**
     * 更新职位信息
     *
     * @param user                当前登录用户
     * @param dailyPositionConfig 职位配置信息
     * @return 是否成功
     */
    JsonResult updatePositionConfig(User user, DailyPositionConfig dailyPositionConfig);
}
