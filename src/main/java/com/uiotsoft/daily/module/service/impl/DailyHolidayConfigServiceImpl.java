package com.uiotsoft.daily.module.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uiotsoft.daily.module.dao.DailyHolidayConfigMapper;
import com.uiotsoft.daily.module.entity.DailyHolidayConfig;
import com.uiotsoft.daily.module.service.DailyHolidayConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>DailyHolidayConfigServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月22日 20:24</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyHolidayConfigServiceImpl extends ServiceImpl<DailyHolidayConfigMapper, DailyHolidayConfig> implements DailyHolidayConfigService {

    @Resource
    private DailyHolidayConfigMapper dailyHolidayConfigMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertBatch(List<DailyHolidayConfig> holidayConfigList) {

        try {
            int insert = dailyHolidayConfigMapper.insertBatch(holidayConfigList);
            if (insert > 0) {
                log.info("E|DailyHolidayConfigServiceImpl|insertBatch()|批量插入节假日数据，定时执行成功！");
            }
        } catch (Exception e) {
            log.error("E|DailyHolidayConfigServiceImpl|insertBatch()|批量插入节假日数据，定时执行失败！原因 = 【{}】", e.getMessage());
        }
    }
}
