package com.uiotsoft.daily.module.service;

import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dto.DailyDTO;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyJob;
import com.uiotsoft.daily.module.entity.DailyJobPlan;
import com.uiotsoft.daily.module.vo.DailyVO;
import com.uiotsoft.daily.organization.entity.User;

import java.util.List;

/**
 * <p>DailyService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 8:59</p>
 * <p>@remark：</p>
 */
public interface DailyService {

    /**
     * 添加日报
     *
     * @param user     当前登录用户
     * @param dailyDTO 日报的日程安排列表
     * @return 是否添加成功
     */
    JsonResult addDaily(User user, DailyDTO dailyDTO);

    /**
     * 根据当前登录人查询当前用户最新一条日报内容
     *
     * @param user       当前登录人
     * @param createDate 日报创建日期
     * @return 当前用户最新一条日报内容
     */
    JsonResult getLastDaily(User user, String createDate);

    /**
     * 根据部门id分页查询部门日报列表
     *
     * @param user          当前用户
     * @param queryParamDTO 查询参数
     * @return 分页日报列表
     */
    JsonResult pageDailyList(User user, QueryParamDTO queryParamDTO);

    /**
     * 封装日报返回参数
     *
     * @param pageDailyJob 分页日报对象
     * @param userName     当前登录人
     * @return 日报返回参数
     */
    DailyVO getDailyVO(PageInfo<DailyJob> pageDailyJob, String userName);

    /**
     * 查询我发出的日报列表，用于查看日报中的我发出的标签
     *
     * @param user          当前登录用户
     * @param queryParamDTO 查询参数
     * @return 我发出的日报列表
     */
    JsonResult myPageDailyList(User user, QueryParamDTO queryParamDTO);

    /**
     * 分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签
     *
     * @param user          当前登录用户
     * @param queryParamDTO 查询参数
     * @return 所有已读和未读的日报列表
     */
    JsonResult maPageDailyList(User user, QueryParamDTO queryParamDTO);

    /**
     * 查询研发中心各部门下的未读日报数量，用于查看日报中页面初始化未读数量显示
     *
     * @param user       当前登录用户
     * @param createUser 日报创建人
     * @param startDate  开始日期
     * @param endDate    结束日期
     * @return 研发中心各部门下的未读日报数量
     */
    JsonResult initNoReadCount(User user, String createUser, String startDate, String endDate);

    /**
     * 查询昨日未完成日报，用于查看日报列表展示时，有未完成工作安排时查询未完成工作安排详情列表
     *
     * @param user  当前登录用户
     * @param jobId 昨天的日报id
     * @return 未完成工作安排详情列表
     */
    JsonResult getLastDailyUndoneList(User user, Integer jobId);

    /**
     * 根据日报id获取未完成日程列表
     *
     * @param jobId 日报id
     * @return 未完成日程列表
     */
    List<DailyJobPlan> getUndoneDailyJobPlanListByJobId(Integer jobId);

    /**
     * 获取截止到22点已提交的日报手机号列表，用于定时任务，钉钉通知提醒未提交
     *
     * @return 截止到22点已提交的日报列表
     */
    List<String> selectUsernameListByCreateTime();
}
