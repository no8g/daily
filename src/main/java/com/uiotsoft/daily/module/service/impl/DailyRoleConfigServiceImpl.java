package com.uiotsoft.daily.module.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.common.constant.NumberConstants;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dao.DailyRoleConfigMapper;
import com.uiotsoft.daily.module.dao.DailyViewUserMapper;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyRoleConfig;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import com.uiotsoft.daily.module.service.DailyRoleConfigService;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>DailyRoleConfigServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 17:11</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyRoleConfigServiceImpl extends ServiceImpl<DailyRoleConfigMapper, DailyRoleConfig> implements DailyRoleConfigService {

    @Resource
    private DailyRoleConfigMapper dailyRoleConfigMapper;

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Override
    public JsonResult addDailyRoleConfig(User user, DailyRoleConfig dailyRoleConfig) {

        if (user == null) {
            log.error("E|DailyRoleConfigServiceImpl|addDailyRoleConfig()|添加角色时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String trueName = user.getTrueName();
        dailyRoleConfig.setCreateUser(user.getUserName());
        dailyRoleConfig.setCreateName(trueName);
        dailyRoleConfig.setCreateTime(new Date());
        dailyRoleConfig.setFlag(1);

        int insert = dailyRoleConfigMapper.insert(dailyRoleConfig);
        if (insert > 0) {
            log.info("E|DailyRoleConfigServiceImpl|addDailyRoleConfig()|添加角色成功，【创建人 = {}】", trueName);
            return JsonResult.ok("添加角色成功");
        }

        return JsonResult.ok("添加角色失败！");
    }

    @Override
    public JsonResult deleteDailyRoleConfig(User user, Integer configId) {

        if (user == null) {
            log.error("E|DailyRoleConfigServiceImpl|deleteDailyRoleConfig()|逻辑删除角色时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String trueName = user.getTrueName();
        DailyRoleConfig dailyRoleConfig = dailyRoleConfigMapper.selectById(configId);
        // 当前角色状态：1启用，-1未启用
        dailyRoleConfig.setFlag(-1);
        int update = dailyRoleConfigMapper.updateById(dailyRoleConfig);
        if (update > 0) {
            log.info("E|DailyRoleConfigServiceImpl|deleteDailyRoleConfig()|逻辑删除角色成功，【操作人 = {}】", trueName);
            return JsonResult.ok("逻辑删除角色成功！");
        }

        return JsonResult.fail("逻辑删除角色失败！");
    }

    @Override
    public JsonResult updateDailyRoleConfig(User user, DailyRoleConfig dailyRoleConfig) {

        if (user == null) {
            log.error("E|DailyRoleConfigServiceImpl|updateDailyRoleConfig()|修改角色时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String trueName = user.getTrueName();
        dailyRoleConfig.setUpdateUser(user.getUserName());
        dailyRoleConfig.setUpdateName(trueName);
        dailyRoleConfig.setUpdateTime(new Date());
        dailyRoleConfig.setFlag(1);

        int update = dailyRoleConfigMapper.updateById(dailyRoleConfig);
        if (update > 0) {
            log.info("E|DailyRoleConfigServiceImpl|updateDailyRoleConfig()|修改角色成功，【修改人 = {}】", trueName);
            return JsonResult.ok("修改角色成功");
        }

        return JsonResult.ok("修改角色失败！");
    }

    @Override
    public JsonResult pageDailyRoleConfig(User user, QueryParamDTO queryParamDTO) {

        if (user == null) {
            log.error("E|DailyRoleConfigServiceImpl|updateDailyRoleConfig()|分页查询角色时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String userName = user.getUserName();
        int pageNum = queryParamDTO.getPageNum();
        int pageSize = queryParamDTO.getPageSize();

        PageInfo<DailyRoleConfig> pageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> dailyRoleConfigMapper.selectList(Wrappers.<DailyRoleConfig>lambdaQuery()
                        .eq(DailyRoleConfig::getFlag, 1)));

        return JsonResult.ok(pageInfo);
    }

    @Override
    public JsonResult getDeptList(User user) {

        if (user == null) {
            log.error("E|DailyRoleConfigServiceImpl|getDeptList()|根据当前角色获取部门列表时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        // 查询出角色配置表中所有数据
        List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.getRoleConfigList();

        String userName = user.getUserName();
        for (DailyRoleConfig dailyRoleConfig : roleConfigList) {
            // 如果当前用户等于其中一条，则表示用户是总监或者经理
            String roleUser = dailyRoleConfig.getRoleUser();
            Integer roleRank = dailyRoleConfig.getRoleRank();
            if (roleUser.equals(userName)) {
                // 如果角色等于总监或者总监助理，返回当前部门下所有部门
                if (roleRank == NumberConstants.NUMBER_ONE || roleRank == NumberConstants.NUMBER_TWO) {
                    List<DailyRoleConfig> configList = roleConfigList.stream()
                            .filter(item -> NumberConstants.NUMBER_THREE == item.getRoleRank()).collect(Collectors.toList());
                    return JsonResult.ok(configList);
                } else {
                    // 否则返回当前角色部门
                    return JsonResult.ok(Collections.singletonList(dailyRoleConfig));
                }
            }
        }

        String agentSid = user.getAgentSid();
        for (DailyRoleConfig dailyRoleConfig : roleConfigList) {
            if (agentSid.contains(dailyRoleConfig.getDeptId()) && dailyRoleConfig.getRoleRank() == NumberConstants.NUMBER_THREE) {
                return JsonResult.ok(Collections.singletonList(dailyRoleConfig));
            }
        }

        return JsonResult.fail("查询部门列表失败！");
    }

    @Override
    public JsonResult getUserList(User user, String deptId, String trueName, Boolean isTeam) {

        if (user == null) {
            log.error("E|DailyRoleConfigServiceImpl|getDeptList()|查询员工列表，用于查看日报根据员工筛选查询日报列表时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        List<DailyViewUser> userList;
        // 兼容团队日报里的人员筛选，团队日报中人员列表需要是整个研发中心的
        if (Objects.nonNull(isTeam) && isTeam) {
            userList = this.getUserListLikeDeptId(CommonConstants.RD_CENTER_DEPT_ID, trueName);
            return JsonResult.ok(userList);
        }
        if (deptId != null && !"".equals(deptId)) {
            userList = this.getUserListLikeDeptId(deptId, trueName);
            return JsonResult.ok(userList);
        }

        // 查询出角色配置表中所有数据
        List<DailyRoleConfig> roleConfigList = dailyRoleConfigMapper.selectList(Wrappers.<DailyRoleConfig>lambdaQuery()
                .eq(DailyRoleConfig::getFlag, 1));
        String userName = user.getUserName();
        for (DailyRoleConfig dailyRoleConfig : roleConfigList) {
            if (userName.equals(dailyRoleConfig.getRoleUser())) {
                Integer roleRank = dailyRoleConfig.getRoleRank();
                if (roleRank == NumberConstants.NUMBER_THREE) {
                    userList = this.getUserListLikeDeptId(dailyRoleConfig.getDeptFid(), trueName);
                } else if (roleRank == NumberConstants.NUMBER_ONE) {
                    userList = this.getUserListLikeDeptId(CommonConstants.RD_CENTER_DEPT_ID, trueName);
                    DailyViewUser dailyViewUser = dailyViewUserMapper.getDailyViewUserByUsername(userName);
                    userList.add(dailyViewUser);
                } else {
                    userList = this.getUserListLikeDeptId(dailyRoleConfig.getDeptId(), trueName);
                }
                return JsonResult.ok(userList);
            }
        }

        String agentSid = user.getAgentSid();
        for (DailyRoleConfig dailyRoleConfig : roleConfigList) {
            if (agentSid.contains(dailyRoleConfig.getDeptId()) && dailyRoleConfig.getRoleRank() == NumberConstants.NUMBER_THREE) {
                userList = this.getUserListLikeDeptId(dailyRoleConfig.getDeptId(), trueName);
                return JsonResult.ok(userList);
            }
        }

        return JsonResult.fail("查询用户失败！");
    }

    @Override
    public DailyRoleConfig getDailyRoleConfigByUsername(String userName) {

        DailyRoleConfig roleConfigByUser = dailyRoleConfigMapper.getDailRoleConfigByUser(userName);
        log.info("E|DailyRoleConfigServiceImpl|getDailyRoleConfigByUsername()|" +
                "根据用户名查询角色配置信息【userName = {}, roleConfigByUser = {}】", userName, roleConfigByUser);

        return roleConfigByUser;
    }

    /**
     * 根据部门id查询用户列表
     *
     * @param deptId 部门id
     * @return 用户列表
     */
    private List<DailyViewUser> getUserListLikeDeptId(String deptId, String trueName) {

        long start = System.currentTimeMillis();

        List<DailyViewUser> userList = dailyViewUserMapper.selectListLikeDeptId(deptId, trueName);

        log.info("E|DailyRoleConfigServiceImpl|getUserListLikeDeptId()|根据部门id查询部门下的用户列表，" +
                "部门id = 【{}】, 耗时 = 【{}ms】", deptId, System.currentTimeMillis() - start);

        return userList;
    }
}
