package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dto.ProjectDTO;
import com.uiotsoft.daily.module.dto.QueryProjectDTO;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyProjectService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 9:50</p>
 * <p>@remark：</p>
 */
public interface DailyProjectService {

    /**
     * 分页查询项目列表
     *
     * @param user            当前登录人
     * @param queryProjectDTO 查询参数
     * @return 分页查询项目列表
     */
    JsonResult pageList(User user, QueryProjectDTO queryProjectDTO);

    /**
     * 新增项目
     *
     * @param user       当前登录人
     * @param projectDTO 项目dto
     * @return 是否成功
     */
    JsonResult addProject(User user, ProjectDTO projectDTO);

    /**
     * 新增项目
     *
     * @param user       当前登录人
     * @param projectDTO 项目dto
     * @return 是否成功
     */
    JsonResult updateProject(User user, ProjectDTO projectDTO);

    /**
     * 根据id查询项目详情
     *
     * @param user 当前登录人
     * @param id   id
     * @return 项目详情
     */
    JsonResult getProjectInfo(User user, Integer id);

    /**
     * 根据id删除项目
     *
     * @param user 当前登录人
     * @param id   id
     * @return 是否删除成功
     */
    JsonResult delProjectById(User user, Integer id);
}
