package com.uiotsoft.daily.module.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uiotsoft.daily.common.constant.NumberConstants;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.dao.DailyProjectMapper;
import com.uiotsoft.daily.module.dao.DailyTeamMemberMapper;
import com.uiotsoft.daily.module.dto.ProjectDTO;
import com.uiotsoft.daily.module.dto.QueryProjectDTO;
import com.uiotsoft.daily.module.entity.DailyProjectInfo;
import com.uiotsoft.daily.module.entity.DailyTeamMember;
import com.uiotsoft.daily.module.service.DailyProjectService;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>DailyProjectServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 9:50</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyProjectServiceImpl extends ServiceImpl<DailyProjectMapper, DailyProjectInfo> implements DailyProjectService {

    @Resource
    private DailyProjectMapper dailyProjectMapper;

    @Resource
    private DailyTeamMemberMapper dailyTeamMemberMapper;

    @Override
    public JsonResult pageList(User user, QueryProjectDTO queryProjectDTO) {

        if (user == null) {
            log.error("E|DailyProjectServiceImpl|pageList()|分页查询项目时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        int pageNum = queryProjectDTO.getPageNum();
        int pageSize = queryProjectDTO.getPageSize();

        try {
            PageInfo<List<DailyProjectInfo>> pageInfo = PageHelper.startPage(pageNum, pageSize)
                    .doSelectPageInfo(() -> dailyProjectMapper.pageList(queryProjectDTO));

            return JsonResult.ok(pageInfo);
        } catch (Exception e) {
            log.error("E|DailyProjectServiceImpl|pageList()|分页查询项目时，查询失败！原因 = 【{}】", e.getMessage());
        }

        return JsonResult.fail("分页查询项目失败！");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult addProject(User user, ProjectDTO projectDTO) {

        if (user == null) {
            log.error("E|DailyProjectServiceImpl|addProject()|新增项目时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String trueName = user.getTrueName();

        DailyProjectInfo projectInfo = new DailyProjectInfo();
        BeanUtil.copyProperties(projectDTO, projectInfo);
        projectInfo.setCreateTime(new Date());
        projectInfo.setIsDel(NumberConstants.NUMBER_ONE);
        projectInfo.setCreateUser(user.getUserName());
        projectInfo.setCreateName(trueName);

        try {
            int insert = dailyProjectMapper.save(projectInfo);
            if (insert > 0) {
                log.info("E|DailyProjectServiceImpl|addProject()|新增项目时，项目添加成功，操作人 = 【{}】", trueName);
                Integer projectId = projectInfo.getId();
                List<DailyTeamMember> teamMemberList = projectDTO.getTeamMemberList();
                for (DailyTeamMember dailyTeamMember : teamMemberList) {
                    dailyTeamMember.setProjectId(projectId);
                }
                int save = dailyTeamMemberMapper.saveList(teamMemberList);
                if (save > 0) {
                    log.info("E|DailyProjectServiceImpl|addProject()|新增项目时，项目成员列表成功，操作人 = 【{}】", trueName);
                    return JsonResult.ok("项目和项目成员添加成功！");
                }
            }

            return JsonResult.fail("添加失败！");
        } catch (Exception e) {
            log.error("E|DailyProjectServiceImpl|addProject()|新增项目时，添加失败，原因 = 【{}】", e.getMessage());
        }

        return JsonResult.fail("添加失败！");
    }

    @Override
    public JsonResult updateProject(User user, ProjectDTO projectDTO) {

        if (user == null) {
            log.error("E|DailyProjectServiceImpl|updateProject()|修改项目时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        String trueName = user.getTrueName();

        DailyProjectInfo projectInfo = new DailyProjectInfo();
        BeanUtil.copyProperties(projectDTO, projectInfo);
        projectInfo.setUpdateTime(new Date());
        projectInfo.setUpdateUser(user.getUserName());
        projectInfo.setUpdateName(trueName);

        try {
            int update = dailyProjectMapper.updateById(projectInfo);
            if (update > 0) {
                log.info("E|DailyProjectServiceImpl|updateProject()|修改项目时，修改成功，操作人 = 【{}】", trueName);
                return JsonResult.ok("修改成功！");
            }
        } catch (Exception e) {
            log.error("E|DailyProjectServiceImpl|updateProject()|修改项目时，修改失败，原因 = 【{}】", e.getMessage());
        }

        return JsonResult.fail("修改失败！");
    }

    @Override
    public JsonResult getProjectInfo(User user, Integer id) {

        if (user == null) {
            log.error("E|DailyProjectServiceImpl|getProjectInfo()|查询项目详情时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        if (id == null) {
            log.error("E|DailyProjectServiceImpl|getProjectInfo()|查询项目详情时，传入id参数为空！");
            return JsonResult.fail("查询项目详情时，id不能为空！");
        }

        try {
            DailyProjectInfo dailyProjectInfo = dailyProjectMapper.selectById(id);
            List<DailyTeamMember> teamMemberList = dailyTeamMemberMapper.selectList(Wrappers.<DailyTeamMember>lambdaQuery().eq(DailyTeamMember::getProjectId, id));
            ProjectDTO projectDTO = new ProjectDTO();
            BeanUtil.copyProperties(dailyProjectInfo, projectDTO);
            projectDTO.setTeamMemberList(teamMemberList);

            return JsonResult.ok(projectDTO);

        } catch (Exception e) {
            log.error("E|DailyProjectServiceImpl|updateProject()|查询项目详情时，查询详情失败，原因 = 【{}】", e.getMessage());
        }

        return JsonResult.fail("查询详情失败！");
    }

    @Override
    public JsonResult delProjectById(User user, Integer id) {

        if (user == null) {
            log.error("E|DailyProjectServiceImpl|delProjectById()|删除项目时，获取当前登录人失败！");
            return JsonResult.fail("获取当前登录人失败！");
        }

        if (id == null) {
            log.error("E|DailyProjectServiceImpl|delProjectById()|删除项目时，传入id参数为空！");
            return JsonResult.fail("删除项目时，id不能为空！");
        }

        try {
            DailyProjectInfo dailyProjectInfo = dailyProjectMapper.selectById(id);
            dailyProjectInfo.setIsDel(NumberConstants.NUMBER_MINUS_ONE);
            int update = dailyProjectMapper.updateById(dailyProjectInfo);
            if (update > 0) {
                log.info("E|DailyProjectServiceImpl|delProjectById()|删除项目时，逻辑删除成功，操作人 = 【{}】", user.getTrueName());
                return JsonResult.ok("删除成功！");
            }
        } catch (Exception e) {
            log.error("E|DailyProjectServiceImpl|delProjectById()|删除项目时，逻辑删除失败，原因 = 【{}】", e.getMessage());
        }

        return JsonResult.fail("删除失败！");
    }
}
