package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.module.entity.DailyMailContent;

/**
 * <p>DailyMailContentService 此类用于：记录日报收件箱每一条邮件内容</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月24日 20:52</p>
 * <p>@remark：</p>
 */
public interface DailyMailContentService {

    /**
     * 添加日报收件箱每一条邮件内容
     *
     * @param dailyMailContent 邮箱邮件内容
     * @return 是否添加成功
     */
    int insertDailyMailContent(DailyMailContent dailyMailContent);
}
