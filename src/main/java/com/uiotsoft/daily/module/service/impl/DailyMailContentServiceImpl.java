package com.uiotsoft.daily.module.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uiotsoft.daily.module.dao.DailyMailContentMapper;
import com.uiotsoft.daily.module.entity.DailyMailContent;
import com.uiotsoft.daily.module.service.DailyMailContentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>DailyMailContentServiceImpl 此类用于：记录日报收件箱每一条邮件内容</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月24日 20:53</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DailyMailContentServiceImpl extends ServiceImpl<DailyMailContentMapper, DailyMailContent> implements DailyMailContentService {

    @Resource
    private DailyMailContentMapper dailyMailContentMapper;

    @Override
    public int insertDailyMailContent(DailyMailContent dailyMailContent) {

        log.info("E|DailyMailContentServiceImpl|insertDailyMailContent()|添加邮件记录成功！【内容 dailyMailContent = {}】", dailyMailContent);

        return dailyMailContentMapper.insert(dailyMailContent);
    }
}
