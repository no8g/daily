package com.uiotsoft.daily.module.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.module.entity.DailyJobComment;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DailyCommentService 此接口用于：日报评论服务层</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 9:35</p>
 * <p>@remark：</p>
 */
public interface DailyCommentService {

    /**
     * 新增日报评论
     *
     * @param user            当前用户
     * @param dailyJobComment 评论实例
     * @return 是否添加成功
     */
    JsonResult addComment(User user, DailyJobComment dailyJobComment);

    /**
     * 根据日报id查询日报评论列表
     *
     * @param user  当前用户
     * @param jobId 日报id
     * @return
     */
    JsonResult getCommentList(User user, Integer jobId);
}
