package com.uiotsoft.daily.module.dto;

import com.uiotsoft.daily.module.entity.DailyJobPlan;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>DailyAddDTO 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 8:49</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "今日新增的日报DTO")
@Data
public class DailyAddDTO implements Serializable {

    @ApiModelProperty(value = "日程安排列表")
    private List<DailyJobPlan> dailyJobPlanList;
}
