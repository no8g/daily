package com.uiotsoft.daily.module.dto;

import com.uiotsoft.daily.module.entity.DailyProjectInfo;
import com.uiotsoft.daily.module.entity.DailyTeamMember;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>ProjectDTO 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 9:47</p>
 * <p>@remark：</p>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "项目新增和修改时传入的参数")
public class ProjectDTO extends DailyProjectInfo implements Serializable {

    @ApiModelProperty(value = "团队成员列表")
    private List<DailyTeamMember> teamMemberList;
}
