package com.uiotsoft.daily.module.dto;

import com.uiotsoft.daily.module.entity.DailyTeamMember;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>TeamMemberDTO 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 15:03</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "项目成员DTO")
@Data
public class TeamMemberDTO implements Serializable {

    @ApiModelProperty(value = "项目id")
    private Integer projectId;

    @ApiModelProperty(value = "项目成员列表")
    private List<DailyTeamMember> teamMemberList;
}
