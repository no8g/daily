package com.uiotsoft.daily.module.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.uiotsoft.daily.module.entity.DailyJobPlan;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>DailyJobPlanDTO 此类用于：日报完成情况传入参数或回显参数</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 15:22</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "日报完成情况传入参数或回显参数")
@Data
public class DailyJobPlanDTO implements Serializable {

    @ApiModelProperty(value = "日报工作主键", example = "1")
    private Integer dailyJobId;

    @ApiModelProperty(value = "日报创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dailyCreateTime;

    @ApiModelProperty(value = "日程安排列表")
    private List<DailyJobPlan> dailyJobPlanList;
}
