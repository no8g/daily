package com.uiotsoft.daily.module.dto;

import com.uiotsoft.daily.common.base.BasePage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>QueryProjectDTO 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 10:55</p>
 * <p>@remark：</p>
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "查询参数")
@Data
public class QueryProjectDTO extends BasePage implements Serializable {

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "项目经理名字")
    private String projectManagerName;

    @ApiModelProperty(value = "产品经理人名字")
    private String productManagerName;

    @ApiModelProperty(value = "项目状态 项目状态(1、规划中；2、立荐中；3、研发中；4、小试中；5、小批中；6、已上市）")
    private Integer projectState;
}
