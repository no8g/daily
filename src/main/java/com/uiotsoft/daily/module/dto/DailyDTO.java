package com.uiotsoft.daily.module.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>DailyDTO 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 17:24</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "提交日报传入参数")
@Data
public class DailyDTO implements Serializable {

    @ApiModelProperty(value = "今日日程安排")
    private DailyAddDTO dailyAddDTO;

    @ApiModelProperty(value = "工作完成情况")
    private DailyJobPlanDTO dailyJobPlanDTO;
}
