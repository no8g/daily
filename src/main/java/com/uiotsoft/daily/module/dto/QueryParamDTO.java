package com.uiotsoft.daily.module.dto;

import com.uiotsoft.daily.common.base.BasePage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>QueryParamDTO 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月12日 15:40</p>
 * <p>@remark：</p>
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "查询参数")
@Data
public class QueryParamDTO extends BasePage implements Serializable {

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @ApiModelProperty(value = "创建日报的用户手机号")
    private String createUser;

    @ApiModelProperty(value = "开始日期")
    private String startDate;

    @ApiModelProperty(value = "结束日期")
    private String endDate;

    @ApiModelProperty(value = "职位类型：1、部门经理，2、项目经理，3、技术专家")
    private Integer positionType;

    @ApiModelProperty(value = "人员类型：1、研发人员；2、邮箱人员")
    private Integer userType;

    @ApiModelProperty(value = "姓名")
    private String trueName;
}
