package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyRoleConfig;
import com.uiotsoft.daily.module.service.DailyRoleConfigService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyRoleConfigController 此类用于：日报角色配置控制器</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 17:05</p>
 * <p>@remark：</p>
 */
@Api(value = "roleConfig", tags = "日报角色配置相关接口")
@RestController
@RequestMapping(value = "roleConfig")
public class DailyRoleConfigController extends BaseController {

    @Resource
    private DailyRoleConfigService dailyRoleConfigService;

    @ApiOperation("添加日报角色，用于新增角色配置")
    @PostMapping(value = "add")
    public JsonResult addDailyRoleConfig(@RequestBody DailyRoleConfig dailyRoleConfig) {

        User user = getUser();
        JsonResult jsonResult = dailyRoleConfigService.addDailyRoleConfig(user, dailyRoleConfig);

        return jsonResult;
    }

    @ApiOperation(value = "根据id逻辑删除日报角色配置表数据")
    @GetMapping(value = "del/{configId}")
    public JsonResult deleteDailyRoleConfig(@PathVariable Integer configId) {

        User user = getUser();
        JsonResult jsonResult = dailyRoleConfigService.deleteDailyRoleConfig(user, configId);

        return jsonResult;
    }

    @ApiOperation("修改日报角色，用于修改角色配置")
    @PostMapping(value = "update")
    public JsonResult updateDailyRoleConfig(@RequestBody DailyRoleConfig dailyRoleConfig) {

        User user = getUser();
        JsonResult jsonResult = dailyRoleConfigService.updateDailyRoleConfig(user, dailyRoleConfig);

        return jsonResult;
    }

    @ApiOperation("分页查询日报角色，用于修改角色配置")
    @PostMapping(value = "pageList")
    public JsonResult pageDailyRoleConfig(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyRoleConfigService.pageDailyRoleConfig(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation("查询日报角色部门列表，用于查看日报根据部门筛选查询日报列表")
    @GetMapping(value = "deptList")
    public JsonResult getDeptList() {

        User user = getUser();
        JsonResult jsonResult = dailyRoleConfigService.getDeptList(user);

        return jsonResult;
    }

    @ApiOperation("查询员工列表，用于查看日报团队日报、已读和未读以及团队中根据员工筛选查询日报列表")
    @GetMapping(value = "userList")
    public JsonResult getUserList(String deptId, String trueName, Boolean isTeam) {

        User user = getUser();
        JsonResult jsonResult = dailyRoleConfigService.getUserList(user, deptId, trueName, isTeam);

        return jsonResult;
    }
}
