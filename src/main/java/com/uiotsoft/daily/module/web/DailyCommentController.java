package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.entity.DailyJobComment;
import com.uiotsoft.daily.module.service.DailyCommentService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyCommentController 此类用于：日报评论控制器</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 9:13</p>
 * <p>@remark：</p>
 */
@Api(value = "comment", tags = "日报评论相关接口")
@RestController
@RequestMapping(value = "comment")
public class DailyCommentController extends BaseController {

    @Resource
    private DailyCommentService dailyCommentService;

    @ApiOperation("添加日报评论，用于新增日报的评论")
    @PostMapping(value = "add")
    public JsonResult addComment(@RequestBody DailyJobComment dailyJobComment) {

        User user = getUser();
        JsonResult jsonResult = dailyCommentService.addComment(user, dailyJobComment);

        return jsonResult;
    }

    @ApiOperation("查询日报评论列表，用于展示日报的评论列表")
    @GetMapping(value = "list")
    public JsonResult getCommentList(@RequestParam("jobId") Integer jobId) {

        User user = getUser();
        JsonResult jsonResult = dailyCommentService.getCommentList(user, jobId);

        return jsonResult;
    }
}
