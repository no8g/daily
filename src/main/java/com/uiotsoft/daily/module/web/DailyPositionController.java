package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyPositionConfig;
import com.uiotsoft.daily.module.service.DailyPositionService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyPositionController 此类用于：职位相关接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月03日 9:54</p>
 * <p>@remark：</p>
 */
@Api(value = "dailyPosition", tags = "职位相关接口")
@RestController
@RequestMapping(value = "position")
public class DailyPositionController extends BaseController {

    @Resource
    private DailyPositionService dailyPositionService;

    @ApiOperation("分页查询日报列表，用于查看日报中的部门经理、项目经理、技术专家、CTO助理等日报")
    @PostMapping(value = "pageList")
    public JsonResult pageDailyList(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyPositionService.pageDailyList(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation("分页查询职位列表，用于职位管理中部门经理、项目经理、技术专家、CTO助理等职位的配置列表显示")
    @PostMapping(value = "page")
    public JsonResult pageList(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyPositionService.pageList(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation("添加职位配置，用于新增职位配置")
    @PostMapping(value = "add")
    public JsonResult addPositionConfig(@RequestBody DailyPositionConfig dailyPositionConfig) {

        User user = getUser();
        JsonResult jsonResult = dailyPositionService.addPositionConfig(user, dailyPositionConfig);

        return jsonResult;
    }

    @ApiOperation(value = "根据id逻辑删除职位配置表数据")
    @GetMapping(value = "del/{positionId}")
    public JsonResult deletePositionConfig(@PathVariable Integer positionId) {

        User user = getUser();
        JsonResult jsonResult = dailyPositionService.deletePositionConfig(user, positionId);

        return jsonResult;
    }

    @ApiOperation(value = "根据id查询职位配置详情")
    @GetMapping(value = "info/{positionId}")
    public JsonResult getPositionConfigInfo(@PathVariable Integer positionId) {

        User user = getUser();
        JsonResult jsonResult = dailyPositionService.getPositionConfigInfo(user, positionId);

        return jsonResult;
    }

    @ApiOperation("修改职位，用于修改职位配置")
    @PostMapping(value = "update")
    public JsonResult updatePositionConfig(@RequestBody DailyPositionConfig dailyPositionConfig) {

        User user = getUser();
        JsonResult jsonResult = dailyPositionService.updatePositionConfig(user, dailyPositionConfig);

        return jsonResult;
    }
}
