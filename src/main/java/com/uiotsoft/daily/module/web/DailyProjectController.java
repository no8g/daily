package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.dto.ProjectDTO;
import com.uiotsoft.daily.module.dto.QueryProjectDTO;
import com.uiotsoft.daily.module.service.DailyProjectService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyProjectController 此类用于：日报项目控制层</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 9:41</p>
 * <p>@remark：</p>
 */
@Api(value = "dailyProject", tags = "日报项目控制层")
@RestController
@RequestMapping(value = "project")
public class DailyProjectController extends BaseController {

    @Resource
    private DailyProjectService dailyProjectService;

    @ApiOperation(value = "查询项目列表接口，用于分页查询项目")
    @PostMapping(value = "pageList")
    public JsonResult pageList(@RequestBody QueryProjectDTO queryProjectDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyProjectService.pageList(user, queryProjectDTO);

        return jsonResult;
    }

    @ApiOperation(value = "添加项目接口，用于配置项目时添加项目")
    @PostMapping(value = "add")
    public JsonResult addProject(@RequestBody ProjectDTO projectDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyProjectService.addProject(user, projectDTO);

        return jsonResult;
    }

    @ApiOperation(value = "修改项目接口，用于配置项目时修改项目")
    @PostMapping(value = "update")
    public JsonResult updateProject(@RequestBody ProjectDTO projectDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyProjectService.updateProject(user, projectDTO);

        return jsonResult;
    }

    @ApiOperation(value = "查询项目详情，用于修改项目时回显数据")
    @GetMapping(value = "info/{id}")
    public JsonResult getProjectInfo(@PathVariable(value = "id") Integer id) {

        User user = getUser();
        JsonResult jsonResult = dailyProjectService.getProjectInfo(user, id);

        return jsonResult;
    }

    @ApiOperation(value = "删除项目，用于删除项目")
    @GetMapping(value = "del/{id}")
    public JsonResult delProjectById(@PathVariable(value = "id") Integer id) {

        User user = getUser();
        JsonResult jsonResult = dailyProjectService.delProjectById(user, id);

        return jsonResult;
    }
}
