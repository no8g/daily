package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.service.DailyStatisticsService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyStatisticsController 此类用于：日报统计相关接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月24日 9:14</p>
 * <p>@remark：</p>
 */
@Slf4j
@Api(tags = "日报统计相关接口", value = "statistics")
@RestController
@RequestMapping(value = "/api/statistics")
public class DailyStatisticsController extends BaseController {

    @Resource
    private DailyStatisticsService dailyStatisticsService;

    @ApiOperation(value = "获取日报未提交人员列表，用于Portal页面日报统计未提交人员列表")
    @PostMapping(value = "/getNoSubmitList")
    public JsonResult getNoSubmitList(@RequestBody User user) {
        log.info("E|DailyStatisticsController|getNoSubmitList()|获取日报未提交人员列表，用于Portal页面日报统计未提交人员列表！");
        JsonResult jsonResult = dailyStatisticsService.getNoSubmitList(user);
        return jsonResult;
    }

    @ApiOperation(value = "分页查询日报未提交人员列表，用于日报统计页面未提交人员列表")
    @PostMapping(value = "/pageList")
    public JsonResult pageList(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyStatisticsService.pageList(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation(value = "获取所有部门列表，用于日报统计页面未提交人员列表的部门查询")
    @GetMapping(value = "/getAllDeptList")
    public JsonResult getAllDeptList() {

        User user = getUser();
        JsonResult jsonResult = dailyStatisticsService.getAllDeptList(user);

        return jsonResult;
    }

    @ApiOperation(value = "分页查询日报未提交人员列表，用于日报统计页面Excel导出文件")
    @PostMapping(value = "/excel")
    public JsonResult exportExcel(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyStatisticsService.exportExcel(user, queryParamDTO);

        return jsonResult;
    }
}
