package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.dto.TeamMemberDTO;
import com.uiotsoft.daily.module.service.DailyTeamMemberService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyProjectController 此类用于：项目成员控制层</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 9:41</p>
 * <p>@remark：</p>
 */
@Api(value = "dailyProject", tags = "项目成员控制层")
@RestController
@RequestMapping(value = "team")
public class DailyTeamMemberController extends BaseController {

    @Resource
    private DailyTeamMemberService dailyTeamMemberService;

    @ApiOperation(value = "添加项目成员接口，用于配置项目时添加项目成员列表使用")
    @PostMapping(value = "addBatch")
    public JsonResult addTeamMember(@RequestBody TeamMemberDTO teamMemberDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyTeamMemberService.addBatch(user, teamMemberDTO);

        return jsonResult;
    }

    @ApiOperation(value = "修改项目接口，用于配置项目时修改项目")
    @PostMapping(value = "update")
    public JsonResult updateTeamMember(@RequestBody TeamMemberDTO teamMemberDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyTeamMemberService.updateTeamMember(user, teamMemberDTO);

        return jsonResult;
    }

    @ApiOperation(value = "分页查看团队成员日报接口，用于分页显示团队成员日报列表")
    @PostMapping(value = "pageList")
    public JsonResult pageList(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyTeamMemberService.pageList(user, queryParamDTO);

        return jsonResult;
    }
}
