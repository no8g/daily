package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.service.DailyReadRecordService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyReadRecordController 此类用于：日报已读控制器</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 8:45</p>
 * <p>@remark：</p>
 */
@Api(value = "readRecord", tags = "新增日报已读记录相关接口")
@RestController
@RequestMapping(value = "readRecord")
public class DailyReadRecordController extends BaseController {

    @Resource
    private DailyReadRecordService dailyReadRecordService;

    @ApiOperation("添加日报已读记录，用于新增日报已读记录")
    @PostMapping(value = "add/{dailyJobId}")
    public JsonResult addReadRecord(@PathVariable Integer dailyJobId) {

        User user = getUser();
        JsonResult jsonResult = dailyReadRecordService.addReadRecord(user, dailyJobId);

        return jsonResult;
    }

    @ApiOperation("分页查询日报已读列表，用于日报已读功能（分页）")
    @PostMapping(value = "pageRead")
    public JsonResult pageReadRecord(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyReadRecordService.pageReadRecord(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation("分页查询日报未读列表，用于日报未读列表（分页）")
    @PostMapping(value = "pageNoRead")
    public JsonResult pageNoReadRecord(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyReadRecordService.pageNoReadRecord(user, queryParamDTO);

        return jsonResult;
    }
}
