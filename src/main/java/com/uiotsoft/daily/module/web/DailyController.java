package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.dto.DailyDTO;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.service.DailyService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyController 此类用于：日报添加控制器</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 8:45</p>
 * <p>@remark：</p>
 */
@Api(value = "daily", tags = "日报相关接口")
@RestController
@RequestMapping(value = "daily")
public class DailyController extends BaseController {

    @Resource
    private DailyService dailyService;

    @ApiOperation(value = "今日日程添加和昨日完成情况更新接口，用于添加日报")
    @PostMapping(value = "add")
    public JsonResult addDaily(@RequestBody DailyDTO dailyDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyService.addDaily(user, dailyDTO);

        return jsonResult;
    }

    @ApiOperation(value = "查询昨日日报，用于新增日报时展示昨天工作完成情况")
    @GetMapping(value = "getLastDaily")
    public JsonResult getLastDaily(String createDate) {

        User user = getUser();
        JsonResult jsonResult = dailyService.getLastDaily(user, createDate);

        return jsonResult;
    }

    @ApiOperation("查询部门日报列表，用于查看日报中的部门日报")
    @PostMapping(value = "pageList")
    public JsonResult pageDailyList(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyService.pageDailyList(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation("查询我发出的日报列表，用于查看日报中的我发出的标签")
    @PostMapping(value = "/my/pageList")
    public JsonResult myPageDailyList(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyService.myPageDailyList(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation("分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签")
    @PostMapping(value = "/ma/pageList")
    public JsonResult maPageDailyList(@RequestBody QueryParamDTO queryParamDTO) {

        User user = getUser();
        JsonResult jsonResult = dailyService.maPageDailyList(user, queryParamDTO);

        return jsonResult;
    }

    @ApiOperation("查询研发中心各部门下的未读日报数量，用于查看日报中页面初始化未读数量显示")
    @GetMapping(value = "/init/count")
    public JsonResult initNoReadCount(String createUser, String startDate, String endDate) {

        User user = getUser();
        JsonResult jsonResult = dailyService.initNoReadCount(user, createUser, startDate, endDate);

        return jsonResult;
    }

    @ApiOperation(value = "查询昨日未完成日报，用于查看日报列表展示时，有未完成工作安排时查询未完成工作安排详情列表")
    @GetMapping(value = "/undone/list")
    public JsonResult getLastDailyUndoneList(Integer jobId) {

        User user = getUser();
        JsonResult jsonResult = dailyService.getLastDailyUndoneList(user, jobId);

        return jsonResult;
    }
}
