package com.uiotsoft.daily.module.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.module.entity.DailyJobCommentReply;
import com.uiotsoft.daily.module.service.DailyCommentReplyService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>DailyCommentReplyController 此类用于：日报评论回复控制器</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 9:50</p>
 * <p>@remark：</p>
 */
@Api(value = "reply", tags = "日报评论回复相关接口")
@RestController
@RequestMapping(value = "reply")
public class DailyCommentReplyController extends BaseController {

    @Resource
    private DailyCommentReplyService dailyCommentReplyService;

    @ApiOperation("添加回复，用于日报的评论回复")
    @PostMapping(value = "add")
    public JsonResult addReply(@RequestBody DailyJobCommentReply dailyJobCommentReply) {

        User user = getUser();
        JsonResult jsonResult = dailyCommentReplyService.addReply(user, dailyJobCommentReply);

        return jsonResult;
    }
}
