package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>DailyJobComment 此类用于：工作日报评论实体</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月06日 17:48</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "工作日报评论实体")
@Data
@TableName("daily_job_comment")
public class DailyJobComment implements Serializable {

    @ApiModelProperty(value = "评论id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "日报id")
    private Integer jobId;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论人手机号")
    private String createUser;

    @ApiModelProperty(value = "评论人姓名")
    private String createName;

    @ApiModelProperty(value = "评论创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "评论回复列表")
    @TableField(exist = false)
    private List<DailyJobCommentReply> replyList;
}
