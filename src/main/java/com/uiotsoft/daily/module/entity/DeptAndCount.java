package com.uiotsoft.daily.module.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>DeptAndCount 此类用于：部门与部门下的日报未读数量</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月16日 15:46</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "部门与部门下的日报未读数量")
@Data
public class DeptAndCount {

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "部门下的日报未读数量")
    private Long noReadCount;
}
