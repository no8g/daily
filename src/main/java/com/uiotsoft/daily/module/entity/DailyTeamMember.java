package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>DailyTeamMember 此类用于：团队成员实体类</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 9:05</p>
 * <p>@remark：</p>
 */
@Data
@ApiModel(value = "团队成员实体类")
@TableName("daily_team_member")
public class DailyTeamMember implements Serializable {
    
    @ApiModelProperty(value = "编号id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "项目id")
    private Integer projectId;

    @ApiModelProperty(value = "手机号")
    private String userName;

    @ApiModelProperty(value = "用户姓名")
    private String trueName;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;
}
