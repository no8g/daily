package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>DailyJobComment 此类用于：工作日报评论回复实体</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月06日 17:48</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "工作日报评论回复实体")
@Data
@TableName("daily_job_comment_reply")
public class DailyJobCommentReply implements Serializable {

    @ApiModelProperty(value = "回复编号id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "回复的父编号")
    private Integer pid;

    @ApiModelProperty(value = "评论id")
    private Integer commentId;

    @ApiModelProperty(value = "回复内容")
    private String content;

    @ApiModelProperty(value = "被回复人手机号")
    private String toUser;

    @ApiModelProperty(value = "被回复人姓名")
    private String toName;

    @ApiModelProperty(value = "回复创建人手机号")
    private String createUser;

    @ApiModelProperty(value = "回复创建人姓名")
    private String createName;

    @ApiModelProperty(value = "回复创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
