package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>DailyHolidayConfig 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月22日 21:01</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "日报节假日配置表")
@Data
public class DailyHolidayConfig implements Serializable {

    @ApiModelProperty(value = "节假日编号id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "节假日的日期")
    private Date holidayDate;

    @ApiModelProperty(value = "节假日的中文名")
    private String holidayName;

    @ApiModelProperty(value = "薪资的倍数，3表示是3倍工资")
    private Integer holidayWage;

    @ApiModelProperty(value = "距离目标节假日的天数（表示当前时间距离目标还有多少天。比如今天是 2018-09-28，距离 2018-10-01 还有3天）")
    private Integer holidayRest;

    @ApiModelProperty(value = "是否节前调休（用 1 代表 TRUE，0 代表 FALSE）--true表示放完假后调休，false表示先调休再放假")
    private Boolean holidayAfter;

    @ApiModelProperty(value = "调休的节假日（即表示所属的节假日）")
    private String holidayTarget;

    @ApiModelProperty(value = "创建人手机号")
    private String createUser;

    @ApiModelProperty(value = "创建人姓名")
    private String createName;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
