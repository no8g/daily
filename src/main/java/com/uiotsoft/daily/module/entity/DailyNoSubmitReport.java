package com.uiotsoft.daily.module.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.uiotsoft.daily.annotation.ExcelField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>DailyNoSubmitReport 此类用于：未提交日报实体</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月21日 16:12</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "未提交日报实体")
@Data
public class DailyNoSubmitReport implements Serializable {

    @ApiModelProperty(value = "未提交日报id", example = "1")
    @ExcelField("编号")
    private Integer id;

    @ApiModelProperty(value = "手机号")
    @ExcelField("手机号")
    private String userName;

    @ApiModelProperty(value = "姓名")
    @ExcelField("姓名")
    private String trueName;

    @ApiModelProperty(value = "部门id")
    @ExcelField("部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    @ExcelField("部门名称")
    private String deptName;

    @ExcelField(value = "未提交日期", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "未提交日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date noSubmitDate;

    @ExcelField("每周周几")
    @ApiModelProperty(value = "每周周几")
    private String dayOfWeek;

    @ExcelField("人员类型：1、研发人员；2、邮箱人员")
    @ApiModelProperty(value = "人员类型")
    private Integer userType;

    @ExcelField(value = "创建日期", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
