package com.uiotsoft.daily.module.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>HolidayRawInfo 此类用于：从网站上获取到的节假日原始数据类型</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月22日 20:45</p>
 * <p>@remark：</p>
 */
@Data
public class HolidayRawInfo implements Serializable {

    /**
     * 节假日的日期
     */
    private Date date;

    /**
     * 表示当前时间距离目标还有多少天。比如今天是 2018-09-28，距离 2018-10-01 还有3天
     */
    private Integer rest;

    /**
     * 节假日的中文名
     */
    private String name;

    /**
     * true表示放完假后调休，false表示先调休再放假
     */
    private Boolean after;

    /**
     * 该字段一定为true
     */
    private Boolean holiday;

    /**
     * 薪资倍数，3表示是3倍工资
     */
    private Integer wage;

    /**
     * 表示调休的节假日
     */
    private String target;
}
