package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>DailyViewUser 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月21日 10:30</p>
 * <p>@remark：</p>
 */
@Data
@TableName("daily_v_user")
public class DailyViewUser implements Serializable {

    private Integer userId;
    private String dingUserId;
    private String userName;
    private String trueName;
    private String handset;
    private String agentSid;
    private String password;
    private Integer roleId;
    private Date createDate;
    private String orgEmail;
    private String isLeader;
    private String position;
    private Integer loginCount;
    private Date loginLastDate;
    private String loginLastIp;
    private Date pwdLastDate;
    private Integer userState;
    private String isAdmin;
    private Integer userLevel;
    private String isAlarm;
    private String headPortrait;
    private String agentName;
    private String fid;
}
