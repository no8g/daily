package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>DailyMailContent 此类用于：记录日报收件箱每一条邮件内容实体</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月24日 20:46</p>
 * <p>@remark：</p>
 */
@Data
@TableName("daily_mail_content")
public class DailyMailContent {

    @ApiModelProperty(value = "编号id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "邮件主题")
    private String mailSubject;

    @ApiModelProperty(value = "邮件发送时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date mailSendTime;

    @ApiModelProperty(value = "是否需要回复：0、否，1、是")
    private Integer isReply;

    @ApiModelProperty(value = "是否包含附件：0、否，1、是")
    private Integer isContainAttach;

    @ApiModelProperty(value = "发送人地址")
    private String fromAddr;

    @ApiModelProperty(value = "发送人姓名")
    private String fromName;

    @ApiModelProperty(value = "收件人地址")
    private String toAddr;

    @ApiModelProperty(value = "抄送人地址")
    private String carbonCopyAddr;

    @ApiModelProperty(value = "密送人地址")
    private String secretCarbonCopyAddr;

    @ApiModelProperty(value = "邮件id")
    private String mailId;

    @ApiModelProperty(value = "邮件正文")
    private String mailBodyContext;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
