package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>DailyJobPlan 此类用于：日报工作安排表</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月06日 17:44</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "日报工作安排表")
@Data
@TableName("daily_job_plan")
public class DailyJobPlan implements Serializable {

    @ApiModelProperty(value = "工作安排日程id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "日报id")
    private Integer jobId;

    @ApiModelProperty(value = "工作内容")
    private String jobContent;

    @ApiModelProperty(value = "工作开始时间")
    private String jobTime;

    @ApiModelProperty(value = "为什么要做")
    private String whyDoJob;

    @ApiModelProperty(value = "如何做")
    private String howDoJob;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "完成情况：完成和未完成")
    private String finishStatus;
}
