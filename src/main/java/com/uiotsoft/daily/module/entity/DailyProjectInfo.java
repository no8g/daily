package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>DailyProjectInfo 此类用于：项目实体类</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 8:48</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "项目实体类")
@Data
@TableName("daily_project_info")
public class DailyProjectInfo implements Serializable {

    @ApiModelProperty(value = "项目编号id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "项目说明")
    private String projectRemark;

    @ApiModelProperty(value = "项目经理人")
    private String projectManager;

    @ApiModelProperty(value = "项目经理名字")
    private String projectManagerName;

    @ApiModelProperty(value = "产品经理人")
    private String productManager;

    @ApiModelProperty(value = "产品经理人名字")
    private String productManagerName;

    @ApiModelProperty(value = "项目状态 项目状态(1、规划中；2、立荐中；3、研发中；4、小试中；5、小批中；6、已上市）")
    private Integer projectState;

    @ApiModelProperty(value = "项目创建人手机号")
    private String createUser;

    @ApiModelProperty(value = "项目创建人姓名")
    private String createName;

    @ApiModelProperty(value = "项目创建日期 默认为当前时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "项目修改人手机号")
    private String updateUser;

    @ApiModelProperty(value = "项目修改人姓名")
    private String updateName;

    @ApiModelProperty(value = "项目创建日期 默认为当前时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(value = "是否删除 是否删除，默认为1，-1删除")
    private Integer isDel;
}
