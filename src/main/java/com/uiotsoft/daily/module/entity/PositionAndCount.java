package com.uiotsoft.daily.module.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>PositionAndCount 此类用于：职位与职位下的日报未读数量</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月16日 16:18</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "职位与职位下的日报未读数量")
@Data
public class PositionAndCount {

    @ApiModelProperty(value = "职位类型：1、部门经理，2、项目经理，3、技术专家")
    private Integer positionType;

    @ApiModelProperty(value = "用户职位")
    private String position;

    @ApiModelProperty(value = "职位下的日报未读数量")
    private Long noReadCount;
}
