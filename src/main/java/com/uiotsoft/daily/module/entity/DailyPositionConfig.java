package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>DailyPositionConfig 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月03日 10:02</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "职位配置表")
@Data
@TableName("daily_position_config")
public class DailyPositionConfig implements Serializable {

    @ApiModelProperty(value = "编号id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户手机号")
    private String userName;

    @ApiModelProperty(value = "用户真实姓名")
    private String trueName;

    @ApiModelProperty(value = "职位类型：1、部门经理，2、项目经理，3、技术专家")
    private Integer positionType;

    @ApiModelProperty(value = "用户职位")
    private String position;

    @ApiModelProperty(value = "创建人手机号")
    private String createUser;

    @ApiModelProperty(value = "创建人姓名")
    private String createName;

    @ApiModelProperty(value = "配置创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "是否删除")
    private Integer isDel;
}
