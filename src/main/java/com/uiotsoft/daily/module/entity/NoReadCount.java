package com.uiotsoft.daily.module.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>NoReadCount 此类用于：封装前端需要的查看日报中每个标签未读数量(马总查看使用)</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月10日 16:28</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "封装前端需要的查看日报中每个标签未读数量")
@Data
public class NoReadCount {

    @ApiModelProperty(value = "职位与职位下的日报未读数量")
    private List<PositionAndCount> positionAndCountList;

    @ApiModelProperty(value = "各部门与各部门下的日报未读数量")
    private List<DeptAndCount> deptAndCountList;

    @ApiModelProperty(value = "团队日报未读数量")
    private Long teamNoReadCount;

    @ApiModelProperty(value = "未读日报数量")
    private Long noReadCount;
}
