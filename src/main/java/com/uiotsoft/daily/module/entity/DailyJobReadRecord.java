package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>DailyJobReadRecord 此类用于：工作日报已经记录实体</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月06日 17:55</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "工作日报已经记录实体")
@Data
@TableName("daily_job_read_record")
public class DailyJobReadRecord implements Serializable {

    @ApiModelProperty(value = "日报已读记录id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "日报工作id")
    private Integer jobId;

    @ApiModelProperty(value = "日报已读人手机号")
    private String readUser;

    @ApiModelProperty(value = "日报已读人姓名")
    private String readName;
}
