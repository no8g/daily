package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.uiotsoft.daily.module.vo.YesterdayJobVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>DailyJob 此类用于：日报工作表实体</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月06日 17:33</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "日报工作表实体")
@Data
@TableName("daily_job")
public class DailyJob implements Serializable {

    @ApiModelProperty(value = "日报id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "创建人手机号")
    private String createUser;

    @ApiModelProperty(value = "创建人姓名")
    private String createName;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "日报创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "日报更新时间（主要更新完成情况）")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(value = "今日的工作日报列表")
    @TableField(exist = false)
    private List<DailyJobPlan> todayPlanList;

    @ApiModelProperty(value = "昨天的工作日报情况")
    @TableField(exist = false)
    private YesterdayJobVO yesterdayPlan;

    @ApiModelProperty(value = "日报评论列表")
    @TableField(exist = false)
    private List<DailyJobComment> commentList;

    @ApiModelProperty(value = "是否已读：(在返回日报列表时赋值)")
    @TableField(exist = false)
    private String isRead;
}
