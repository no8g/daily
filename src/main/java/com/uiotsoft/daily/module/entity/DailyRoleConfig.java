package com.uiotsoft.daily.module.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>DailyRoleConfig 此类用于：角色配置实体</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月06日 17:57</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "角色配置实体")
@Data
@TableName("daily_role_config")
public class DailyRoleConfig implements Serializable {

    @ApiModelProperty(value = "编号id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "角色层级")
    private Integer roleRank;

    @ApiModelProperty(value = "角色层级说明")
    private String roleRankName;

    @ApiModelProperty(value = "角色账号")
    private String roleUser;

    @ApiModelProperty(value = "角色账号姓名")
    private String roleUserName;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "父级部门id")
    private String deptFid;

    @ApiModelProperty(value = "父级部门名称")
    private String deptFname;

    @ApiModelProperty(value = "当前角色创建账号")
    private String createUser;

    @ApiModelProperty(value = "当前角色创建人姓名")
    private String createName;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "当前角色修改账号")
    private String updateUser;

    @ApiModelProperty(value = "当前角色修改人姓名")
    private String updateName;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(value = "状态：1启用，-1未启用")
    private Integer flag;
}
