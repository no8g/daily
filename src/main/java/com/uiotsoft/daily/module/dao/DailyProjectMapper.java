package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.dto.QueryProjectDTO;
import com.uiotsoft.daily.module.entity.DailyProjectInfo;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>DailyProjectMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 9:53</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyProjectMapper extends BaseMapper<DailyProjectInfo> {

    /**
     * 查询项目列表
     *
     * @param queryProjectDTO 查询参数
     * @return 项目列表
     */
    List<DailyProjectInfo> pageList(@Param("queryProjectDTO") QueryProjectDTO queryProjectDTO);

    /**
     * 新增项目
     *
     * @param projectInfo 项目信息
     * @return 成功条数
     */
    int save(DailyProjectInfo projectInfo);
}
