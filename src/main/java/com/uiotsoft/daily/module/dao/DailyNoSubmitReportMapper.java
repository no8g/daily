package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.dto.QueryParamDTO;
import com.uiotsoft.daily.module.entity.DailyNoSubmitReport;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>DailyNoSubmitReportMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月22日 14:57</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyNoSubmitReportMapper extends BaseMapper<DailyNoSubmitReport> {

    /**
     * 批量插入日报未提交记录
     *
     * @param list 需要插入的日报未提交记录
     * @return 插入条数
     */
    int insertBatch(@Param("list") List<DailyNoSubmitReport> list);

    /**
     * 根据日报未提交日期查询人员列表
     *
     * @param queryDate 日报未提交日期
     * @return 人员列表
     */
    List<DailyNoSubmitReport> selectListByNoSubmitDate(@Param("queryDate") String queryDate);

    /**
     * 获取日报未提交列表，日报统计页面使用
     *
     * @param queryParamDTO 查询参数
     * @return 日报未提交列表
     */
    List<DailyNoSubmitReport> pageList(@Param("queryParamDTO") QueryParamDTO queryParamDTO);
}
