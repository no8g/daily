package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyJobCommentReply;
import org.springframework.stereotype.Repository;

/**
 * <p>DailyCommentReplyMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 9:56</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyCommentReplyMapper extends BaseMapper<DailyJobCommentReply> {
}
