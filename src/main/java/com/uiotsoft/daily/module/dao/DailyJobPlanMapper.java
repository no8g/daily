package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyJobPlan;
import org.springframework.stereotype.Repository;

/**
 * <p>DailyJobPlanMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 11:24</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyJobPlanMapper extends BaseMapper<DailyJobPlan> {
}
