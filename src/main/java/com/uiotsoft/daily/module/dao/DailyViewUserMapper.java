package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>DailyViewUserMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月21日 10:27</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyViewUserMapper extends BaseMapper<DailyViewUser> {

    /**
     * 根据部门id Like查询部门人员列表
     *
     * @param deptId   部门id
     * @param trueName 员工姓名
     * @return 部门人员列表
     */
    List<DailyViewUser> selectListLikeDeptId(@Param("deptId") String deptId, @Param("trueName") String trueName);

    /**
     * 根据钉钉用户id查询用户详情
     *
     * @param userId 钉钉用户id
     * @return 用户详情
     */
    DailyViewUser getDailyViewUserByUserId(String userId);

    /**
     * 根据手机号查询用户详情
     *
     * @param mobile 钉钉用户手机号
     * @return 用户详情
     */
    DailyViewUser getDailyViewUserByUsername(String mobile);

    /**
     * 根据邮箱地址列表查询邮箱主人详情列表
     *
     * @param list 邮箱地址列表
     * @return 邮箱主人详情列表
     */
    List<DailyViewUser> selectListByEmailList(@Param("list") CopyOnWriteArrayList<String> list);

    /**
     * 根据邮箱查询用户详情
     *
     * @param email 邮箱
     * @return 用户详情
     */
    DailyViewUser getDailyViewUserByEmail(String email);
}
