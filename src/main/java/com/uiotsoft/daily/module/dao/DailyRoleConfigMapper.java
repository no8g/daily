package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyRoleConfig;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>DailyRoleConfigMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 17:13</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyRoleConfigMapper extends BaseMapper<DailyRoleConfig> {

    /**
     * 根据用户手机号查询当前用户角色配置信息
     *
     * @param userName 用户手机号
     * @return 当前用户角色配置信息
     */
    DailyRoleConfig getDailRoleConfigByUser(@Param("userName") String userName);

    /**
     * 根据角色层级查询角色列表（查询出角色配置表中角色层级为2的所有部门(助理所在部门的部门id)）
     * 公司领导不一定在当前部门下
     *
     * @return 角色列表
     */
    List<DailyRoleConfig> selectListByRoleRank();

    /**
     * 获取所有部门列表
     *
     * @return 所有部门列表
     */
    List<DailyRoleConfig> getRoleConfigList();
}
