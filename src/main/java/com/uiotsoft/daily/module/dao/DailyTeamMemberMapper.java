package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyTeamMember;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>DailyTeamMemberMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月06日 10:38</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyTeamMemberMapper extends BaseMapper<DailyTeamMember> {

    /**
     * 批量插入项目成员
     *
     * @param teamMemberList 项目成员列表
     * @return 成功条数
     */
    int saveList(@Param("teamMemberList") List<DailyTeamMember> teamMemberList);
}
