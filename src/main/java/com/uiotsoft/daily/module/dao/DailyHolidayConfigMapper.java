package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyHolidayConfig;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>DailyHolidayConfigMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月23日 8:38</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyHolidayConfigMapper extends BaseMapper<DailyHolidayConfig> {

    /**
     * 批量插入节假日配置数据
     *
     * @param holidayConfigList 被插入的数据列表
     * @return 成功条数
     */
    int insertBatch(List<DailyHolidayConfig> holidayConfigList);

    /**
     * 根据日期查询节假日详情，用于判断昨天是否是节假日
     *
     * @return 节假日详情
     */
    DailyHolidayConfig getHolidayConfigInfo();
}
