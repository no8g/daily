package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyPositionConfig;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>DailyPositionMapper 此接口用于：职位相关接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月03日 10:02</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyPositionMapper extends BaseMapper<DailyPositionConfig> {

    /**
     * 根据职位类型获取职位名称
     *
     * @param positionType 职位类型
     * @return 职位名称
     */
    String getPositionByPositionType(@Param("positionType") Integer positionType);
}
