package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyMailContent;
import org.springframework.stereotype.Repository;

/**
 * <p>DailyMailContentMapper 此接口用于：邮件内容相关接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年06月24日 20:53</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyMailContentMapper extends BaseMapper<DailyMailContent> {
}
