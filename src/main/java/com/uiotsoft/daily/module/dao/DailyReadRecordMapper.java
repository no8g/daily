package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyJobReadRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>DailyReadRecordMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月08日 8:57</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyReadRecordMapper extends BaseMapper<DailyJobReadRecord> {

    /**
     * 根据当前用户手机号和日报id查询日报是否已读
     *
     * @param userName 当前用户手机号
     * @param jobId    日报id
     * @return 日报是否已读
     */
    DailyJobReadRecord selectOneByUserNameAndJobId(@Param("userName") String userName, @Param("jobId") Integer jobId);

    /**
     * 根据手机号和部门id查询日报已读记录列表，like查询
     *
     * @param userName   当前登录人手机号查询
     * @param deptId     部门id
     * @param createUser 日报创建人手机号
     * @param startDate  开始日期
     * @param endDate    结束日期
     * @return 日报已读记录列表
     */
    List<DailyJobReadRecord> selectListLikeDeptId(@Param("userName") String userName,
                                                  @Param("deptId") String deptId,
                                                  @Param("createUser") String createUser,
                                                  @Param("startDate") String startDate,
                                                  @Param("endDate") String endDate);

    /**
     * 根据手机号和部门id查询日报已读记录列表，equal查询
     *
     * @param userName   当前登录人手机号查询
     * @param deptId     部门id
     * @param createUser 日报创建人手机号
     * @param startDate  开始日期
     * @param endDate    结束日期
     * @return 日报已读记录列表
     */
    List<DailyJobReadRecord> selectListByDeptId(@Param("userName") String userName,
                                                @Param("deptId") String deptId,
                                                @Param("createUser") String createUser,
                                                @Param("startDate") String startDate,
                                                @Param("endDate") String endDate);
}
