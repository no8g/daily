package com.uiotsoft.daily.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.module.entity.DailyJob;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <p>DailyJobMapper 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月07日 11:24</p>
 * <p>@remark：</p>
 */
@Repository
public interface DailyJobMapper extends BaseMapper<DailyJob> {

    /**
     * 新增日报工作
     *
     * @param dailyJob 日报工作
     * @return 新增条数
     */
    int insertDaily(@Param("dailyJob") DailyJob dailyJob);

    /**
     * 根据日期查询一条日报信息
     *
     * @param userName   当前登录用户手机号
     * @param createDate 查询日期
     * @return 一条日报信息
     */
    DailyJob selectByCreateTime(@Param("userName") String userName, @Param("createDate") String createDate);

    /**
     * 根据用户手机号查询一条日报信息
     *
     * @param userName 用户手机号
     * @return 一条日报信息
     */
    DailyJob selectOneByCreateUser(String userName);

    /**
     * 根据部门id和创建日报的用户手机号，like查询日报列表
     *
     * @param deptId     部门id
     * @param createUser 创建日报的用户手机号
     * @param userName   当前登录人手机号
     * @param startDate  开始日期
     * @param endDate    结束日期
     * @return 日报列表
     */
    List<DailyJob> listLikeDeptId(@Param("deptId") String deptId,
                                  @Param("createUser") String createUser,
                                  @Param("userName") String userName,
                                  @Param("startDate") String startDate,
                                  @Param("endDate") String endDate);

    /**
     * 分页查询日报未读记录列表，like查询
     *
     * @param userName   手机号查询
     * @param deptId     部门id
     * @param createUser 创建日报的用户手机号
     * @param startDate  开始日期
     * @param endDate    结束日期
     * @return 日报未读记录列表
     */
    List<DailyJob> selectNoReadListLikeDeptId(@Param("userName") String userName,
                                              @Param("deptId") String deptId,
                                              @Param("createUser") String createUser,
                                              @Param("startDate") String startDate,
                                              @Param("endDate") String endDate);

    /**
     * 根据当前用户和当前日期查询当前日期以前的最晚一条日报（即昨天的日报，也正好跳过单双休和假期）
     *
     * @param userName   当前用户
     * @param createTime 当前日期
     * @return 昨日日报
     */
    DailyJob selectYesterdayDailyJob(@Param("userName") String userName, @Param("createTime") Date createTime);

    /**
     * 查询当日已提交日报人员列表
     *
     * @return 已提交日报人员列表
     */
    List<DailyJob> selectListByCreateTime();

    /**
     * 根据部门和日期查询日报列表，like查询
     *
     * @param deptId    部门
     * @param queryDate 日期
     * @return 日报列表
     */
    List<DailyJob> selectListLikeDeptIdAndCreateTime(@Param("deptId") String deptId, @Param("queryDate") String queryDate);

    /**
     * 根据手机号列表查询日报列表
     *
     * @param list       用户手机号列表
     * @param deptId     部门id
     * @param createUser 日报创建人
     * @param userName   当前登录人手机号
     * @param startDate  开始日期
     * @param endDate    结束日期
     * @return 日报列表
     */
    List<DailyJob> listInUsernameList(@Param("list") List<String> list,
                                      @Param("deptId") String deptId,
                                      @Param("createUser") String createUser,
                                      @Param("userName") String userName,
                                      @Param("startDate") String startDate,
                                      @Param("endDate") String endDate);

    /**
     * 根据创建时间查询日报列表，用于定时任务执行查看日报已提交列表
     *
     * @param holidayDate 创建时间查询日报
     * @return 日报列表
     */
    List<DailyJob> listByCreateTime(@Param("holidayDate") Date holidayDate);

    /**
     * 根据日报创建人查询日报列表，用于我发出的标签
     *
     * @param queryCreateUser 日报创建人
     * @return 日报创建人的日报列表
     */
    List<DailyJob> listByCreateUser(@Param("queryCreateUser") String queryCreateUser);

    /**
     * 分页查询研发中心部门下所有已读和未读的日报列表，用于查看日报中的全部标签
     *
     * @param createUser     日报创建人手机号
     * @param rdCenterDeptId 研发中心的部门id
     * @param startDate      开始日期
     * @param endDate        结束日期
     * @return 研发中心部门下所有已读和未读的日报列表
     */
    List<DailyJob> selectAllDailyListLikeDeptId(@Param("createUser") String createUser,
                                                @Param("rdCenterDeptId") String rdCenterDeptId,
                                                @Param("startDate") String startDate,
                                                @Param("endDate") String endDate);

    /**
     * 查询当前登录人部门下的未读数量，如果是马总，则查询研发中心下所有部门的未读数量
     *
     * @param userName         当前登录人手机号
     * @param roleConfigDeptId 当前登录人部门id，如果是马总，则是各个部门的部门id
     * @param createUser       日报创建人手机号
     * @param startDate        开始日期
     * @param endDate          结束日期
     * @return 部门下的未读数量
     */
    Long selectDeptCountLikeDeptId(@Param("userName") String userName,
                                   @Param("roleConfigDeptId") String roleConfigDeptId,
                                   @Param("createUser") String createUser,
                                   @Param("startDate") String startDate,
                                   @Param("endDate") String endDate);

    /**
     * 根据职位类型查询当前登录人未读日报数量
     *
     * @param userName     当前登录人手机号
     * @param positionType 职位类型
     * @param createUser   日报创建人手机号
     * @param startDate    开始日期
     * @param endDate      结束日期
     * @return 当前登录人未读日报数量
     */
    Long selectPositionCountByCreateUser(@Param("userName") String userName,
                                         @Param("positionType") Integer positionType,
                                         @Param("createUser") String createUser,
                                         @Param("startDate") String startDate,
                                         @Param("endDate") String endDate);

    /**
     * 根据团队成员查询当前登录人未读日报数量
     *
     * @param userName    当前登录人手机号
     * @param usernameSet 团队成员手机号集合
     * @param createUser  日报创建人手机号
     * @param startDate   开始日期
     * @param endDate     结束日期
     * @return 当前登录人未读日报数量
     */
    Long selectTeamCountInUsernameList(@Param("userName") String userName,
                                       @Param("usernameSet") Set<String> usernameSet,
                                       @Param("createUser") String createUser,
                                       @Param("startDate") String startDate,
                                       @Param("endDate") String endDate);

    /**
     * 查询当前登录人所有未读日报数量
     *
     * @param userName   当前登录人
     * @param deptId     部门id
     * @param createUser 日报创建人手机号
     * @param startDate  开始日期
     * @param endDate    结束日期
     * @return 所有未读日报数量
     */
    Long selectAllNoReadCount(@Param("userName") String userName,
                              @Param("deptId") String deptId,
                              @Param("createUser") String createUser,
                              @Param("startDate") String startDate,
                              @Param("endDate") String endDate);

    /**
     * 根据用户手机号列表查询团队日报未读列表
     *
     * @param finalUserNameList 用户手机号列表
     * @param createUser        日报创建人
     * @param userName          当前登录人
     * @param startDate         开始日期
     * @param endDate           结束日期
     * @return 团队日报未读列表
     */
    List<DailyJob> selectListInUserNameList(@Param("finalUserNameList") List<String> finalUserNameList,
                                            @Param("createUser") String createUser,
                                            @Param("userName") String userName,
                                            @Param("startDate") String startDate,
                                            @Param("endDate") String endDate);

    /**
     * 获取截止到22点已提交的日报手机号列表，用于定时任务，钉钉通知提醒未提交
     *
     * @return 截止到22点已提交的日报列表
     */
    List<String> selectUsernameListByCreateTime();
}
