package com.uiotsoft.daily.module.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>DailyUserVO 此类用于：研发信息系统首页左侧日报统计VO</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月26日 10:32</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "研发信息系统首页左侧日报统计VO")
@Data
public class DailyUserVO implements Serializable {

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "用户真实姓名")
    private String trueName;
}
