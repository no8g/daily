package com.uiotsoft.daily.module.vo;

import com.uiotsoft.daily.module.entity.DailyJob;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>DailyVO 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月12日 17:12</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "日报查询列表回显")
@Data
public class DailyVO implements Serializable {

    @ApiModelProperty(value = "工作日报列表")
    private List<DailyJob> dailyJobList;

    @ApiModelProperty(value = "未读数")
    private Long totalCount;
}
