package com.uiotsoft.daily.module.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>DailyStatisticsVO 此类用于：研发信息系统首页左侧日报统计VO</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月26日 10:27</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "研发信息系统首页左侧日报统计VO")
@Data
public class DailyStatisticsVO implements Serializable {

    @ApiModelProperty(value = "已发日报人数")
    private Integer haveSubmit;

    @ApiModelProperty(value = "未发日报人数")
    private Integer noSubmit;

    @ApiModelProperty(value = "未发日报列表")
    private List<DailyUserVO> noSubmitList;
}
