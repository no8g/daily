package com.uiotsoft.daily.module.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.uiotsoft.daily.module.entity.DailyJobPlan;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>YesterdayJobVO 此类用于：昨天日报完成情况前端返回数据</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月14日 18:06</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "昨天日报完成情况前端返回数据")
@Data
public class YesterdayJobVO implements Serializable {

    @ApiModelProperty(value = "昨天的工作日报时间回显")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "昨天的工作日报列表")
    private List<DailyJobPlan> yesterdayPlanList;

    @ApiModelProperty(value = "昨日未完成工作的日程项数，用于查看日报时未完成工作按钮数据展示")
    private Integer undoneCount;
}
