package com.uiotsoft.daily.operation.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.uiotsoft.daily.common.domain.vo.PageResult;
import com.uiotsoft.daily.operation.entity.SystemOperationLog;

import java.util.Map;

/**
 * 操作日志表接口
 * @author lizy
 * @date 2020-07-07
 */
public interface SystemOperationLogService extends IService<SystemOperationLog> {

    /**
     * 分页查询操作日志表
     * @param page
     * @param params
     * @return
     */
    PageResult<SystemOperationLog> listByPage(Page<SystemOperationLog> page, Map<String, Object> params);

}