package com.uiotsoft.daily.operation.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uiotsoft.daily.common.domain.vo.PageResult;
import com.uiotsoft.daily.operation.dao.SystemOperationLogMapper;
import com.uiotsoft.daily.operation.entity.SystemOperationLog;
import com.uiotsoft.daily.operation.service.SystemOperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * 操作日志表接口实现
 * @author lizy
 * @date 2020-07-07
 */
@Service("systemOperationLogService")
public class SystemOperationLogServiceImpl extends ServiceImpl<SystemOperationLogMapper, SystemOperationLog> implements SystemOperationLogService {

    @Autowired
    private SystemOperationLogMapper systemOperationLogMapper;

    /**
     * 分页查询操作日志表
     * @param page
     * @param params
     * @return
     */
    @Override
    public PageResult listByPage(Page<SystemOperationLog> page, Map<String, Object> params) {
        QueryWrapper<SystemOperationLog> queryWrapper = new QueryWrapper<SystemOperationLog>();
        IPage<SystemOperationLog> pageList = systemOperationLogMapper.selectPage(page, queryWrapper);
        return new PageResult(pageList.getRecords(),pageList.getTotal());
    }

}