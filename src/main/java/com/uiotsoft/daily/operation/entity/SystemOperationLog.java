package com.uiotsoft.daily.operation.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 操作日志表实体
 * @author lizy
 * @date 2020-07-07
 */
@Data
@TableName("daily_operation_log")
public class SystemOperationLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 功能模块
     */
    private String operationModule;

    /**
     * 操作类型
     */
    private String operationType;

    /**
     * 操作描述
     */
    private String operationDesc;

    /**
     * 请求参数
     */
    private String operationRequestParam;

    /**
     * 返回参数
     */
    private String operationResponseParam;

    /**
     * 操作人员账号
     */
    private String operationUserName;

    /**
     * 操作人员名称
     */
    private String operationTrueName;

    /**
     * 操作方法
     */
    private String operationMethod;

    /**
     * 请求URI
     */
    private String operationUri;

    /**
     * 请求IP
     */
    private String operationIp;

    /**
     * 操作时间
     */
    private Date operationCreateTime;

}