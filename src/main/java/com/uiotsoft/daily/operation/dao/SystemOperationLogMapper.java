package com.uiotsoft.daily.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uiotsoft.daily.operation.entity.SystemOperationLog;
import org.springframework.stereotype.Repository;

/**
 * 操作日志表mapprer
 * @author lizy
 * @date 2020-07-07
 */
@Repository
public interface SystemOperationLogMapper extends BaseMapper<SystemOperationLog> {

}