package com.uiotsoft.daily.operation.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.domain.vo.PageResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.operation.entity.SystemOperationLog;
import com.uiotsoft.daily.operation.service.SystemOperationLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 操作日志表 控制器 操作日志表相关接口
 *
 * @author lizy
 * @date 2020-07-07
 */
@Slf4j
@RestController
@RequestMapping("/systemOperationLog")
public class SystemOperationLogController extends BaseController {

    @Autowired
    private SystemOperationLogService systemOperationLogService;

    /**
     * 根据ID查询操作日志表数据
     *
     * @param id id
     * @return 操作日志表数据
     */
    @GetMapping(value = {"/{id}"})
    public JsonResult info(@PathVariable Integer id) {
        SystemOperationLog systemOperationLog = systemOperationLogService.getById(id);
        return systemOperationLog == null ? JsonResult.fail("查询数据为空") : JsonResult.ok(systemOperationLog);
    }

    /**
     * 分页查询操作日志表列表
     *
     * @param params 查询参数
     * @return 操作日志表列表
     */
    @GetMapping("/list")
    public PageResult<SystemOperationLog> list(@RequestParam Map<String, Object> params) {
        Page<SystemOperationLog> page = getPage();
        return systemOperationLogService.listByPage(page, params);
    }

    /**
     * 新增数据操作日志表数据
     *
     * @param systemOperationLog 参数
     * @return 是否成功
     */
    @PostMapping
    public JsonResult save(@RequestBody SystemOperationLog systemOperationLog) {
        boolean ret = systemOperationLogService.save(systemOperationLog);
        return ret == true ? JsonResult.ok() : JsonResult.fail("新增失败");
    }

    /**
     * 修改数据操作日志表数据
     *
     * @param systemOperationLog 参数
     * @return 是否成功
     */
    @PutMapping
    public JsonResult update(@RequestBody SystemOperationLog systemOperationLog) {
        boolean ret = systemOperationLogService.updateById(systemOperationLog);
        return ret == true ? JsonResult.ok() : JsonResult.fail("修改失败");
    }

    /**
     * 根据id逻辑删除操作日志表数据
     *
     * @param id 参数
     * @return 是否成功
     */
    @DeleteMapping("/{id}")
    public JsonResult delete(@PathVariable Integer id) {
        boolean ret = systemOperationLogService.removeById(id);
        return ret == true ? JsonResult.ok() : JsonResult.fail("删除失败");
    }

}