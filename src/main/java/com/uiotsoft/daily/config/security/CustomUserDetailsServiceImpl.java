/**
 * @(#) 2019年06月22日 下午4:29:14
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.config.security;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.uiotsoft.daily.account.exception.ApplicationException;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.common.util.MicroServiceClient;
import com.uiotsoft.daily.organization.entity.User;
import com.uiotsoft.daily.organization.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.Map.Entry;

/**
 * <p>CustomUserDetailsService.java此类用于获取用户信息</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark:</p>
 */
@Slf4j
@Component
public class CustomUserDetailsServiceImpl implements AuthenticationUserDetailsService<CasAssertionAuthenticationToken> {

    @Autowired
    private MicroServiceClient microServiceClient;

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserDetails(CasAssertionAuthenticationToken token) throws UsernameNotFoundException {
        // 此处涉及本地业务用户查询及资源服务获取资源(如菜单code)，自行实现
        AttributePrincipal attributePrincipal = token.getAssertion().getPrincipal();
        Set<String> roleSet = null;
        //解析sso回传的权限
        Map<String, Object> rolePrivilegeMap = new JSONObject();
        List<String> privileges = new ArrayList<>();
        if (attributePrincipal.getAttributes().containsKey(CommonConstants.EN_WORD_ROLE_PRIVILEGE_MAP)) {
            rolePrivilegeMap = (Map<String, Object>) attributePrincipal.getAttributes().get(CommonConstants.EN_WORD_ROLE_PRIVILEGE_MAP);
            roleSet = rolePrivilegeMap.keySet();
            for (Entry<String, Object> entry : rolePrivilegeMap.entrySet()) {
                privileges.addAll((Collection<? extends String>) entry.getValue());
            }
        }
        JSONObject menuJson = microServiceClient.getMenu(privileges);
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        extractAuthoritiesFromMenus(menuJson, authorities);
        CurrUserDetails u = new CurrUserDetails(token.getName(), authorities, menuJson);

        u.setPrivilegeCodeList(privileges);
        u.setTenantId(JSONArray.parseArray(attributePrincipal.getAttributes().get("tenantId").toString()).getLong(0));
        u.setTenantName(((List<String>) attributePrincipal.getAttributes().get("tenantName")).get(0));
        u.setTenantType(((List<String>) attributePrincipal.getAttributes().get("tenantType")).get(0));
        if (((List<String>) attributePrincipal.getAttributes().get(CommonConstants.EN_WORD_MOBILE)).size() != 0) {
            u.setMobile(((List<String>) attributePrincipal.getAttributes().get(CommonConstants.EN_WORD_MOBILE)).get(0));
        }
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        /*HttpSession session = request.getSession();*/
        User user = userService.getUser(u.getUsername());
        if (user == null) {
            log.error("用户信息不存在,请联系管理员!{}", u.getUsername());
            throw new ApplicationException("用户信息不存在,请联系管理员!" + u.getUsername());
        } else {
            user.setRoleCode(roleSet);
            user.setRoleCodeStr(roleSet.toString());
            u.setTrueName(user.getTrueName());
            u.setAgentSid(user.getAgentSid());
            u.setRoleRank(user.getRoleRank());
            u.setUserState(user.getUserState());
            u.setFid(user.getFid());
            u.setDepartment(user.getDepartment());
            /*session.setAttribute("user", user);*/
        }
        /*session.setAttribute("date", String.valueOf(System.currentTimeMillis()));*/
        return u;
    }

    private void extractAuthoritiesFromMenus(JSONObject menus, Collection<SimpleGrantedAuthority> authorities) {
        if (menus.containsKey(CommonConstants.EN_WORD_CODE) && !StrUtil.isEmpty(menus.getString(CommonConstants.EN_WORD_CODE))) {
            authorities.add(new SimpleGrantedAuthority(menus.getString(CommonConstants.EN_WORD_CODE)));
        }

        if (menus.containsKey(CommonConstants.EN_WORD_CHILDS) && !menus.getJSONArray(CommonConstants.EN_WORD_CHILDS).isEmpty()) {
            for (Object child : menus.getJSONArray(CommonConstants.EN_WORD_CHILDS)) {
                extractAuthoritiesFromMenus((JSONObject) child, authorities);
            }
        }

    }
}
