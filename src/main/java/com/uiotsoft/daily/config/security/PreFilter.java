package com.uiotsoft.daily.config.security;

import lombok.extern.slf4j.Slf4j;
import org.jasig.cas.client.util.AbstractConfigurationFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hjm
 * @date 2020.04.10
 */
@Slf4j
public class PreFilter extends AbstractConfigurationFilter {

	private static final String FILTER_APPLIED = "__spring_security_preFilter_filterApplied";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		if (request.getAttribute(FILTER_APPLIED) != null) {
			chain.doFilter(request, response);
			return;
		}
		// do something
		HttpServletRequest r = (HttpServletRequest) request;
		log.debug("accesss url : {}", r.getRequestURI());
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		request.setAttribute(FILTER_APPLIED, true);
		chain.doFilter(request, httpServletResponse);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

}
