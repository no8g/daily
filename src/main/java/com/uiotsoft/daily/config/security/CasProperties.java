package com.uiotsoft.daily.config.security;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Cas属性
 *
 * @author hjm
 * @date 2021.04.10
 */
@Component
@Data
public class CasProperties {

	@Value("${cas.server.url}")
	private String casServerPrefix;

	@Value("${cas.server.login_url}")
	private String casServerLoginUrl;

	@Value("${cas.server.logout_url}")
	private String casServerLogoutUrl;

	@Value("${cas.client.url}")
	private String appServicePrefix;

	@Value("${cas.client.login_url}")
	private String appServiceLoginUrl;

	@Value("${cas.client.logout_url}")
	private String appServiceLogoutUrl;
}
