package com.uiotsoft.daily.config.security;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * 用户详情
 * <p>CurrUserDetails.java此类用于描述当前用户信息详情</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 */
@Data
public class CurrUserDetails implements UserDetails {

    private static final long serialVersionUID = 4512857196797393681L;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户姓名")
    private String trueName;

    @ApiModelProperty(value = "部门id")
    private String agentSid;

    @ApiModelProperty(value = "代理商SID")
    private Integer roleRank;

    @ApiModelProperty(value = "用户状态")
    private Integer userState;

    @ApiModelProperty(value = "部门")
    private String department;

    @ApiModelProperty(value = "部门父id")
    private String fid;

    /**
     * 密码
     */
    private String password;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 租户名称
     */
    private String tenantName;

    /**
     * 租户类型
     */
    private String tenantType;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 授权集合
     */
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * 菜单
     */
    private JSONObject menus;


    /**
     * 用户权限编码集合
     */
    private List<String> privilegeCodeList;

    public CurrUserDetails() {
    }

    public CurrUserDetails(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public CurrUserDetails(String username, Collection<? extends GrantedAuthority> authorities, JSONObject menus) {
        this.username = username;
        this.authorities = authorities;
        this.setMenus(menus);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }

}
