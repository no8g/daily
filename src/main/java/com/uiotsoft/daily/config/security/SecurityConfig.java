/**
 * @(#) 2018年11月16日 下午4:29:14
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.config.security;

import org.jasig.cas.client.session.SingleSignOutFilter;
import org.jasig.cas.client.validation.json.Cas30JsonServiceTicketValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import javax.annotation.Resource;


/**
 * SecurityConfig.java此类用于springSecurity认证</p>
 *
 * @author czj
 * @date 2018年11月16日
 * @remark 注解说明：@EnableWebSecurity 启用web权限 @EnableGlobalMethodSecurity 启用方法验证
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CasAuthenticationProvider casProvider;

    @Resource
    private CasProperties casProperties;

    /**
     * service配置对象，设置客户端service的属性
     * 主要设置请求cas服务端后的回调路径,一般为主页地址，不可为登录地址
     *
     * @return ServiceProperties
     * @author 陈曾杰
     * @date 2019年3月8日
     */
    @Bean
    public ServiceProperties serviceProperties() {
        ServiceProperties serviceProperties = new ServiceProperties();
        // 设置回调的service路径，此为本应用的主页路径
        serviceProperties.setService(casProperties.getAppServicePrefix() + casProperties.getAppServiceLoginUrl());
        return serviceProperties;
    }

    /**
     * 配置ticket校验器
     *
     * @return
     */
    @Bean
    public Cas30JsonServiceTicketValidator cas30JsonServiceTicketValidator() {
        // 配置上服务端的校验ticket地址
        Cas30JsonServiceTicketValidator ticketValidator = new Cas30JsonServiceTicketValidator(casProperties.getCasServerPrefix());
        return ticketValidator;
    }

    /**
     * 创建cas校验类
     * <p>
     * <b>Notes:</b> TicketValidator、AuthenticationUserDetailService属性必须设置;
     * serviceProperties属性主要应用于ticketValidator用于去cas服务端检验ticket
     * </p>
     *
     * @return CasAuthenticationProvider
     */
    @Bean("casProvider")
    public CasAuthenticationProvider casAuthenticationProvider(
            AuthenticationUserDetailsService<CasAssertionAuthenticationToken> userDetailsService) {
        CasAuthenticationProvider provider = new CasAuthenticationProvider();
        provider.setKey("casProvider");
        provider.setServiceProperties(serviceProperties());
        provider.setTicketValidator(cas30JsonServiceTicketValidator());
        provider.setAuthenticationUserDetailsService(userDetailsService);
        return provider;
    }

    /**
     * 核心filter，单点注销，接受cas服务端发出的注销session请求
     *
     * @return SingleSignOutFilter
     */
    public SingleSignOutFilter singleSignOutFilter() {
        SingleSignOutFilter outFilter = new SingleSignOutFilter();
        // 设置cas服务端路径前缀，应用于front channel的注销请求
        outFilter.setCasServerUrlPrefix(casProperties.getCasServerPrefix());
        outFilter.setIgnoreInitConfiguration(true);
        return outFilter;
    }

    /**
     * 单点请求cas客户端退出Filter类
     * 请求/logout，转发至cas服务端进行注销
     */
    public LogoutFilter logoutFilter() {
        // 设置回调地址，以免注销后页面不再跳转
        StringBuilder logoutRedirectPath = new StringBuilder();
        logoutRedirectPath.append(casProperties.getCasServerPrefix()).append(casProperties.getCasServerLogoutUrl())
                .append("?service=").append(casProperties.getAppServicePrefix());
        LogoutFilter logoutFilter = new LogoutFilter(logoutRedirectPath.toString(), new SecurityContextLogoutHandler());

        logoutFilter.setFilterProcessesUrl(casProperties.getAppServiceLogoutUrl());
        return logoutFilter;
    }

    /**
     * 认证的入口，即跳转至服务端的cas地址
     * <b>Note:</b>浏览器访问不可直接填客户端的login请求,若如此则会返回Error页面，无法被此入口拦截
     */
    @Bean
    public CasAuthenticationEntryPoint casAuthenticationEntryPoint() {
        CasAuthenticationEntryPoint entryPoint = new CasAuthenticationEntryPoint();
        entryPoint.setServiceProperties(serviceProperties());
        entryPoint.setLoginUrl(casProperties.getCasServerPrefix() + casProperties.getCasServerLoginUrl());
        return entryPoint;
    }

    /**
     * cas filter类
     * 针对/login请求的校验
     *
     * @return CasAuthenticationFilter
     */
    public CasAuthenticationFilter casAuthenticationFilter() throws Exception {
        CasAuthenticationFilter casAuthenticationFilter = new CasAuthenticationFilter();
        casAuthenticationFilter.setAuthenticationManager(authenticationManager());
        //用于校验ticket与ServiceProperties中的service保持一致
        casAuthenticationFilter.setFilterProcessesUrl(casProperties.getAppServiceLoginUrl());
        casAuthenticationFilter.setAuthenticationSuccessHandler(new SimpleUrlAuthenticationSuccessHandler(casProperties.getAppServicePrefix()));
        return casAuthenticationFilter;
    }

    @Bean
    public PreFilter preFilter() {
        PreFilter preFilter = new PreFilter();
        return preFilter;
    }

    /**
     * 定义认证用户信息获取来源，密码校验规则等
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        auth.authenticationProvider(casProvider);
    }

    /**
     * 定义安全策略
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // 配置安全策略
                .antMatchers("/error").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/images/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/v2/api-docs").permitAll()
                .antMatchers("/configuration/ui").permitAll()
                .antMatchers("/configuration/security").permitAll()
                .antMatchers("/menu").permitAll()
                .antMatchers("/login/logout").permitAll()
                .antMatchers("/ding/**").permitAll()
                .antMatchers("/mobile/**").permitAll()
                // 日报统计相关
                .antMatchers("/api/**").permitAll()
                // 日报评论
                .antMatchers("/comment/**").permitAll()
                // 评论回复
                .antMatchers("/reply/**").permitAll()
                // 日报相关
                .antMatchers("/daily/**").permitAll()
                // 项目相关
                .antMatchers("/project/**").permitAll()
                // 日报已读
                .antMatchers("/readRecord/**").permitAll()
                // 日报角色配置相关接口
                .antMatchers("/roleConfig/**").permitAll()
                // 团队成员相关
                .antMatchers("/team/**").permitAll()
                // 职位相关接口
                .antMatchers("/position/**").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/static/**").permitAll()

                // swagger end
                .anyRequest().authenticated()
                .and()
                .formLogin() // 使用form表单登录
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling()
                //cas认证入口
                .authenticationEntryPoint(casAuthenticationEntryPoint())
                .and()
                .addFilter(casAuthenticationFilter())
                .addFilterBefore(singleSignOutFilter(), CasAuthenticationFilter.class)
                .addFilterBefore(logoutFilter(), SingleSignOutFilter.class)
                .addFilterBefore(preFilter(), LogoutFilter.class)
                //开启cors跨域
                .cors();
        // 禁用CSRF
        http.csrf().disable();
        http.headers().frameOptions().sameOrigin(); // 关闭spring security默认的frame访问限制
    }

    /**
     * 添加静态资源拦截
     *
     * @param web 参数
     * @throws Exception 异常
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/css/**", "/images/**", "/fonts/**", "/js/**");
    }

}
