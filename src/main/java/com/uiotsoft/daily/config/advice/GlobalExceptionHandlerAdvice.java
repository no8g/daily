package com.uiotsoft.daily.config.advice;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.exception.MisErrorCode;
import com.uiotsoft.daily.common.exception.MisException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局异常处理器
 *
 * @author lizy
 * @date 2019/04/19
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandlerAdvice {

    private static String PAGE_ERROR = "error";
    private static String PAGE_404 = "404";

    /**
     * 处理自定义的业务异常
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = MisException.class)
    public JsonResult bizExceptionHandler(HttpServletRequest request, HttpServletResponse response, MisException e){
        log.error("发生自定义业务异常！原因是：{}", e.getErrorMsg());
        return JsonResult.fail(e.getErrorCode(),e.getErrorMsg());
    }

    /**
     * 处理空指针的异常
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value =NullPointerException.class)
    public Object exceptionHandler(HttpServletRequest request, HttpServletResponse response, NullPointerException e){
        log.error("发生空指针异常！原因是:", e);
        return JsonResult.fail(MisErrorCode.E_600100.getErrorCode(), MisErrorCode.E_600100.getErrorMsg());
    }

    /**
     * 处理其他异常
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public JsonResult handleException(HttpServletRequest request, HttpServletResponse response, Exception e) {
        log.error("未知异常！原因是:", e);
        return JsonResult.fail(MisErrorCode.E_UNKOWN.getErrorCode(), MisErrorCode.E_UNKOWN.getErrorMsg());
    }

    /**
     * 404
     * @param request
     * @param exception
     * @return
     */
    @ExceptionHandler(value = NoHandlerFoundException.class)
    public String handleException(HttpServletRequest request, NoHandlerFoundException exception) {
        log.error(exception.getMessage(), exception);
        return PAGE_404;
    }

    /**
     * 判断是否是Ajax请求
     *
     * @param request
     * @return
     */
    public boolean isAjax(HttpServletRequest request) {
        return (request.getHeader("X-Requested-With") != null && "XMLHttpRequest".equals(request.getHeader("X-Requested-With")));
    }

}
