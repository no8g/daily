package com.uiotsoft.daily.config.mybatisplus;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.uiotsoft.daily.organization.entity.User;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


/**
 * mybatis自动填充公共字段
 *
 * @author liupengtao
 * @date 2019/04/26
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 添加默认公共字段
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (ObjectUtil.isNotNull(requestAttributes)) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            User user = (User) request.getSession().getAttribute("user");
            if (ObjectUtil.isNotNull(user)) {
                // 创建人账号
                this.setFieldValByName("createUser", user.getUserName(), metaObject);
                // 创建人账号
                this.setFieldValByName("createName", user.getTrueName(), metaObject);
                // 创建人账号
                this.setFieldValByName("userName", user.getUserName(), metaObject);
            }
        }
        // 创建日期
        this.setFieldValByName("createTime", new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (ObjectUtil.isNotNull(requestAttributes)) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            User user = (User) request.getSession().getAttribute("user");
            if (ObjectUtil.isNotNull(user)) {
                // 修改人名称
                this.setFieldValByName("updateUser", user.getUserName(), metaObject);
                // 修改人名称
                this.setFieldValByName("updateName", user.getTrueName(), metaObject);
            }
        }
        // 修改日期
        this.setFieldValByName("updateTime", new Date(), metaObject);
    }

}
