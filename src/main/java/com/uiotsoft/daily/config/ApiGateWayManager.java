package com.uiotsoft.daily.config;

import com.uiotsoft.micro.api.TokenClient;
import com.uiotsoft.micro.api.request.token.TokenRequest;

import java.util.Map;

/**
 * <p>ApiGateWayConfiguration.java此类用于web配置</p>
 * <p>@author:czj</p>
 * <p>@date:2018年11月16日</p>
 * <p>@remark:</p>
 */
public class ApiGateWayManager {
	
	
	public ApiGateWayManager(String clientId, String clientSecret, String apiGateWayUrl) {
		super();
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		TokenClient tokenClient = new TokenClient(apiGateWayUrl, clientId, clientSecret);
    	TokenRequest request = new TokenRequest();       
    	Map<String, Object> tokenMap = tokenClient.getAccessToken(request);
    	accessToken = (String)tokenMap.get("access_token");
	}


	private String clientId;
	
	private String clientSecret;
	
	private String accessToken;

	public String getClientId() {
		return clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public String getAccessToken() {
		return accessToken;
	}
	
	

}
