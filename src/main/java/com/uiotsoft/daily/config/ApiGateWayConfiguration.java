package com.uiotsoft.daily.config;


import com.uiotsoft.micro.api.UiotClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * <p>ApiGateWayConfiguration.java此类用于web配置</p>
 * <p>@author:czj</p>
 * <p>@date:2018年11月16日</p>
 * <p>@remark:</p>
 */
@Component
@Configuration
public class ApiGateWayConfiguration {
	
	@Value("${gateway.url}")
	private String url;
	
	@Value("${gateway.clientId}")
	private String clientId;

	@Value("${gateway.clientSecret}")
	private String clientSecret;
	
	@Bean
	public ApiGateWayManager apiGateWayManager(){
		return new ApiGateWayManager(clientId, clientSecret, url);
	}
	
	@Autowired
	public ApiGateWayManager apiGateWayManager;
	
	@Bean
	public UiotClient uiotClient(){
		return new UiotClient(url, clientId, clientSecret, apiGateWayManager.getAccessToken());
	}
	
}
