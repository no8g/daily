package com.uiotsoft.daily.config.redis;

import com.uiotsoft.daily.common.util.cache.Cache;
import com.uiotsoft.daily.common.util.cache.RedisCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * RedisConfig.java此类用于redis配置、session共享
 *
 * @author:陈曾杰
 * @date:2018年11月16日
 * @remark:
 */
@Configuration
@EnableRedisHttpSession(redisNamespace = "{dailyApplication}")
public class RedisConfig {

    @Bean
    public Cache cache(StringRedisTemplate redisTemplate) {
        return new RedisCache(redisTemplate);
    }

    @Bean
    public static ConfigureRedisAction configureRedisAction() {
        return ConfigureRedisAction.NO_OP;
    }

}