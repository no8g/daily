package com.uiotsoft.daily;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * <p>DailyApplication 此类用于：springboot启动项</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月31日 18:48</p>
 * <p>@remark：</p>
 */
@Slf4j
@EnableAsync
@EnableScheduling
@EnableFeignClients(basePackages = "com.uiotsoft")
@SpringBootApplication(scanBasePackages={"com.uiotsoft"})
@MapperScan({"com.uiotsoft.daily.*.dao"})
public class DailyApplication implements ApplicationRunner {

    public static void main(String[] args) {
        try {
            SpringApplication.run(DailyApplication.class, args);
        } catch (Exception e) {
            log.error("项目启动失败，报错原因 ============== ", e);
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("日报系统启动成功了！......");
    }
}
