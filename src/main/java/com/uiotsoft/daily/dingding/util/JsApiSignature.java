package com.uiotsoft.daily.dingding.util;

import com.uiotsoft.daily.dingding.exception.DingtalkEncryptException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

/**
 * <p>DingLoginController 此类用于：钉钉jsapi签名工具类</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月18日 15:06</p>
 * <p>@remark：正式项目请是写入到Mysql等持久化存储</p>
 */
public class JsApiSignature {

    public static String sign(String url, String nonce, Long timestamp, String ticket)
            throws DingtalkEncryptException {
        String plain = String.format(
                "jsapi_ticket=%s&noncestr=%s&timestamp=%d&url=%s",
                ticket, nonce, timestamp, url);
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            sha1.reset();
            sha1.update(plain.getBytes(StandardCharsets.UTF_8));
            return bytesToHex(sha1.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new DingtalkEncryptException(DingtalkEncryptException.COMPUTE_SIGNATURE_ERROR, e);
        }
    }

    /**
     * 模拟生成随机 nonce 字符串
     *
     * @return 随机字符串
     */
    public static String genNonce() {
        return bytesToHex(Long.toString(System.nanoTime()).getBytes(StandardCharsets.UTF_8));
    }

    private static String bytesToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
