package com.uiotsoft.daily.dingding.dto;

import com.uiotsoft.daily.module.entity.DailyJobPlan;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>RemindUndoneJobPlanDTO 此类用于：提醒未完成工作DTO，用于钉钉提醒未完成工作日程</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年07月07日 16:21</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "提醒未完成工作传入参数DTO")
@Data
public class RemindUndoneJobPlanDTO {

    @ApiModelProperty(value = "当前登录人手机号")
    private String userName;

    @ApiModelProperty(value = "当前登录人姓名")
    private String trueName;

    @ApiModelProperty(value = "日报工作计划")
    private DailyJobPlan dailyJobPlan;
}
