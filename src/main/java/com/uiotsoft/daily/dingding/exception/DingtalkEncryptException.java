package com.uiotsoft.daily.dingding.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>DingLoginController 此类用于：钉钉开放平台加解密异常类</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月18日 15:06</p>
 * <p>@remark：钉钉开放平台加解密异常类</p>
 */
public class DingtalkEncryptException extends Exception {

    /**
     * 成功
     */
    public static final int SUCCESS = 0;

    /**
     * 加密明文文本非法
     */
    public final static int ENCRYPTION_PLAINTEXT_ILLEGAL = 900001;

    /**
     * 加密时间戳参数非法
     */
    public final static int ENCRYPTION_TIMESTAMP_ILLEGAL = 900002;

    /**
     * 加密随机字符串参数非法
     */
    public final static int ENCRYPTION_NONCE_ILLEGAL = 900003;

    /**
     * 不合法的aeskey
     */
    public final static int AES_KEY_ILLEGAL = 900004;

    /**
     * 签名不匹配
     */
    public final static int SIGNATURE_NOT_MATCH = 900005;

    /**
     * 计算签名错误
     */
    public final static int COMPUTE_SIGNATURE_ERROR = 900006;

    /**
     * 计算加密文字错误
     */
    public final static int COMPUTE_ENCRYPT_TEXT_ERROR = 900007;

    /**
     * 计算解密文字错误
     */
    public final static int COMPUTE_DECRYPT_TEXT_ERROR = 900008;

    /**
     * 计算解密文字长度不匹配
     */
    public final static int COMPUTE_DECRYPT_TEXT_LENGTH_ERROR = 900009;

    /**
     * 计算解密文字corpid不匹配
     */
    public final static int COMPUTE_DECRYPT_TEXT_CORPID_ERROR = 900010;


    private static final Map<Integer, String> MSG_MAP = new HashMap<>();

    static {
        MSG_MAP.put(SUCCESS, "成功");
        MSG_MAP.put(ENCRYPTION_PLAINTEXT_ILLEGAL, "加密明文文本非法");
        MSG_MAP.put(ENCRYPTION_TIMESTAMP_ILLEGAL, "加密时间戳参数非法");
        MSG_MAP.put(ENCRYPTION_NONCE_ILLEGAL, "加密随机字符串参数非法");
        MSG_MAP.put(SIGNATURE_NOT_MATCH, "签名不匹配");
        MSG_MAP.put(COMPUTE_SIGNATURE_ERROR, "签名计算失败");
        MSG_MAP.put(AES_KEY_ILLEGAL, "不合法的aes key");
        MSG_MAP.put(COMPUTE_ENCRYPT_TEXT_ERROR, "计算加密文字错误");
        MSG_MAP.put(COMPUTE_DECRYPT_TEXT_ERROR, "计算解密文字错误");
        MSG_MAP.put(COMPUTE_DECRYPT_TEXT_LENGTH_ERROR, "计算解密文字长度不匹配");
        MSG_MAP.put(COMPUTE_DECRYPT_TEXT_CORPID_ERROR, "计算解密文字corpid或者suiteKey不匹配");
    }

    private final Integer code;

    public DingtalkEncryptException(Integer exceptionCode) {
        this(exceptionCode, null);
    }

    public DingtalkEncryptException(Integer exceptionCode, Throwable e) {
        super(MSG_MAP.get(exceptionCode), e);
        this.code = exceptionCode;
    }

    public Integer getCode() {
        return code;
    }
}
