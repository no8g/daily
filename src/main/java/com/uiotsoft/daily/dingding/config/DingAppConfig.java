package com.uiotsoft.daily.dingding.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * <p>DingAppConfig 此类用于：应用凭证配置</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月18日 17:25</p>
 * <p>@remark：</p>
 */
@Data
@Configuration
public class DingAppConfig {

    @Value("${dingtalk.app_key}")
    private String appKey;

    @Value("${dingtalk.app_secret}")
    private String appSecret;

    @Value("${dingtalk.agent_id}")
    private String agentId;

    @Value("${dingtalk.corp_id}")
    private String corpId;
}
