package com.uiotsoft.daily.dingding.service;

import com.uiotsoft.daily.module.entity.DailyViewUser;

/**
 * <p>DingAuthService 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月20日 15:56</p>
 * <p>@remark：</p>
 */
public interface DingAuthService {

    /**
     * 根据用户手机号查询是否是UIOT员工
     *
     * @param userId 钉钉用户id
     * @return UIOT员工信息
     */
    DailyViewUser getUserByUserId(String userId);

    /**
     * 根据用户手机号查询是否是UIOT员工
     *
     * @param mobile 钉钉用户手机号
     * @return UIOT员工信息
     */
    DailyViewUser getUserByUsername(String mobile);
}
