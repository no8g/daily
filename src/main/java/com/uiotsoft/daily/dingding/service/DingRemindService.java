package com.uiotsoft.daily.dingding.service;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.dingding.dto.RemindUndoneJobPlanDTO;
import com.uiotsoft.daily.organization.entity.User;

/**
 * <p>DingRemindService 此接口用于：钉钉提醒相关接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年07月07日 16:36</p>
 * <p>@remark：</p>
 */
public interface DingRemindService {

    /**
     * 钉钉提醒，用于未完成工作提醒
     *
     * @param user                   当前登录人
     * @param remindUndoneJobPlanDTO 钉钉提醒传入参数
     * @return 是否提醒成功
     */
    JsonResult remindUndoneJob(User user, RemindUndoneJobPlanDTO remindUndoneJobPlanDTO);
}
