package com.uiotsoft.daily.dingding.service.impl;

import com.uiotsoft.daily.dingding.service.DingAuthService;
import com.uiotsoft.daily.module.dao.DailyViewUserMapper;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>DingAuthServiceImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月20日 15:56</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DingAuthServiceImpl implements DingAuthService {

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Override
    public DailyViewUser getUserByUserId(String userId) {

        if (userId == null) {
            log.error("E|DingAuthServiceImpl|getUserByUserId()|根据用户手机号查询UIOT用户时，当前钉钉用户id为空！");
            return new DailyViewUser();
        }

        DailyViewUser dailyViewUser = dailyViewUserMapper.getDailyViewUserByUserId(userId);

        log.info("E|DingAuthServiceImpl|getUserByUserId()|根据用户手机号查询UIOT用户，userId = {}", userId);

        return dailyViewUser;
    }

    @Override
    public DailyViewUser getUserByUsername(String mobile) {

        if (mobile == null) {
            log.error("E|DingAuthServiceImpl|getUserByUsername()|根据用户手机号查询UIOT用户时，当前钉钉用户id为空！");
            return new DailyViewUser();
        }

        DailyViewUser dailyViewUser = dailyViewUserMapper.getDailyViewUserByUsername(mobile);

        log.info("E|DingAuthServiceImpl|getUserByUsername()|根据用户手机号查询UIOT用户，userId = {}", mobile);

        return dailyViewUser;
    }
}
