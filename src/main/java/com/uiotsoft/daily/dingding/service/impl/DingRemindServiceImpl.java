package com.uiotsoft.daily.dingding.service.impl;

import cn.hutool.core.date.DateUtil;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.dingding.domain.DingTalkMsg;
import com.uiotsoft.daily.dingding.dto.RemindUndoneJobPlanDTO;
import com.uiotsoft.daily.dingding.service.DingRemindService;
import com.uiotsoft.daily.dingding.util.DingDingMsg;
import com.uiotsoft.daily.dingding.util.DingTalkWorkNotice;
import com.uiotsoft.daily.module.dao.DailyJobMapper;
import com.uiotsoft.daily.module.dao.DailyViewUserMapper;
import com.uiotsoft.daily.module.entity.DailyJob;
import com.uiotsoft.daily.module.entity.DailyJobPlan;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import com.uiotsoft.daily.organization.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>DingRemindServiceImpl 此类用于：钉钉提醒相关接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年07月07日 16:36</p>
 * <p>@remark：</p>
 */
@Slf4j
@Service
public class DingRemindServiceImpl implements DingRemindService {

    @Resource
    private DingTalkWorkNotice dingTalkWorkNotice;

    @Resource
    private DailyJobMapper dailyJobMapper;

    @Resource
    private DailyViewUserMapper dailyViewUserMapper;

    @Override
    public JsonResult remindUndoneJob(User user, RemindUndoneJobPlanDTO remindUndoneJobPlanDTO) {

        if (user == null) {
            log.error("E|DingRemindServiceImpl|remindUndoneJob()|钉钉提醒日报未完成工作登录用户为空！");
            return JsonResult.fail("钉钉提醒日报未完成工作登录用户为空！");
        }

        log.info("E|DingRemindServiceImpl|remindUndoneJob()|钉钉提醒日报未完成工作传入参数 remindUndoneJobPlanDTO = {}", remindUndoneJobPlanDTO);
        // 获取当前登录人信息
        String userName = user.getUserName();
        DailyViewUser dailyViewUserByUsername = this.getDailyViewUserByUsername(userName);
        String trueName = dailyViewUserByUsername.getTrueName();
        // 日程安排
        DailyJobPlan dailyJobPlan = remindUndoneJobPlanDTO.getDailyJobPlan();
        Integer jobId = dailyJobPlan.getJobId();

        try {
            // 日报详情
            DailyJob dailyJob = dailyJobMapper.selectById(jobId);
            Date createTime = dailyJob.getCreateTime();
            String createUser = dailyJob.getCreateUser();

            // 获取被通知人的dingUserId
            DailyViewUser dailyViewUser = this.getDailyViewUserByUsername(createUser);
            String dingUserId = dailyViewUser.getDingUserId();

            DingTalkMsg dingTalkMsg = new DingTalkMsg();
            dingTalkMsg.setAuthor(trueName + " " + DateUtil.format(new Date(), "yyyy年MM月dd日"));
            dingTalkMsg.setContent("");
            dingTalkMsg.setTitle("工作未完成通知");
            dingTalkMsg.setMsgType("oa");
            dingTalkMsg.setUserList(Objects.isNull(dingUserId) ? createUser : dingUserId);
            dingTalkMsg.setFromList(getFormList(trueName, createTime, dailyJobPlan));
            dingTalkWorkNotice.sendDingMsg(dingTalkMsg);

            return JsonResult.ok();
        } catch (Exception e) {
            log.error("E|DingRemindServiceImpl|remindUndoneJob()|钉钉提醒日报未完成工作失败！【原因 = {}】", e.getMessage());
            return JsonResult.fail();
        }
    }

    private List<OapiMessageCorpconversationAsyncsendV2Request.Form> getFormList(String trueName, Date createTime, DailyJobPlan dailyJobPlan) {

        String jobContent = dailyJobPlan.getJobContent();
        String jobTime = dailyJobPlan.getJobTime();

        List<OapiMessageCorpconversationAsyncsendV2Request.Form> formList = DingDingMsg.create("【" + trueName + "】提醒你【",
                DateUtil.format(createTime, "yyyy-MM-dd HH:mm:ss") + "】创建的日报，还有未完成的事项，请确认！")
                .builder("日程开始时间：", DateUtil.format(createTime, "yyyy-MM-dd") + " " + jobTime)
                .builder("未完成工作日程：", jobContent)
                .builder("通知提醒时间：", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"))
                .collect();

        log.info("E|DingRemindServiceImpl|getFormList()|钉钉提醒日报未完成工作时，封装钉钉通知内容 = {}", formList);

        return formList;
    }

    /**
     * 根据用户手机号查询用户详情
     *
     * @param username 用户手机号
     * @return 用户详情
     */
    private DailyViewUser getDailyViewUserByUsername(String username) {

        DailyViewUser dailyViewUser = dailyViewUserMapper.getDailyViewUserByUsername(username);
        log.info("E|DingRemindServiceImpl|getDailyViewUserByUsername()|钉钉提醒日报根据手机号获取用户详情 dailyViewUser = {}", dailyViewUser);

        return dailyViewUser;
    }
}
