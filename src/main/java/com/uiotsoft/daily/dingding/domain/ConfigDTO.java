package com.uiotsoft.daily.dingding.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>DingLoginController 此类用于：JSAPI鉴权签名信息</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月18日 15:06</p>
 * <p>@remark：JSAPI鉴权签名信息</p>
 */
@Data
public class ConfigDTO implements Serializable {

    private String jsticket;

    /**
     * 随机串，自己定义
     */
    private String nonceStr;

    /**
     * 应用的标识
     */
    private String agentId;

    /**
     * 时间戳
     */
    private Long timeStamp;

    /**
     * 企业ID
     */
    private String corpId;

    /**
     * 签名
     */
    private String signature;

    /**
     * 选填。0表示微应用的jsapi,1表示服务窗的jsapi；不填默认为0。该参数从dingtalk.js的0.8.3版本开始支持
     */
    private Integer type;
}
