/**
 * @(#)DingTalkMsg.java 2021/4/14 17:21
 * Copyright HeNan UiotSoft. All rights reserved.
 */

package com.uiotsoft.daily.dingding.domain;

import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * DingTalkMsg.java此类用于
 *
 * @author: lizy
 * @date: 2021/4/14
 * @remark:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DingTalkMsg {

    /**
     * 消息类型
     */
    private String msgType;

    /**
     * 通知标题
     */
    private String title;

    /**
     * 通知内容
     */
    private String content;

    /**
     * 通知人员dingUserId列表
     */
    private String userList;

    /**
     * 消息链接地址
     */
    private String url;

    /**
     * OA Form 表单内容
     */
    private List<OapiMessageCorpconversationAsyncsendV2Request.Form> fromList;

    /**
     * 提醒人
     */
    private String author;
}
