package com.uiotsoft.daily.dingding.domain;

import com.uiotsoft.daily.module.entity.DailyViewUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>DingLoginController 此类用于：用户详情</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月18日 15:06</p>
 * <p>@remark：用户详情</p>
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserDTO extends DailyViewUser implements Serializable {

    /**
     * 用户名称
     */
    private String name;

    /**
     * 头像URL
     */
    private String avatar;

    /**
     * accessToken
     */
    private String accessToken;
}
