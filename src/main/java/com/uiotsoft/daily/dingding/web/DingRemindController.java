package com.uiotsoft.daily.dingding.web;

import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.web.BaseController;
import com.uiotsoft.daily.dingding.dto.RemindUndoneJobPlanDTO;
import com.uiotsoft.daily.dingding.service.DingRemindService;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>DingRemindController 此类用于：钉钉提醒相关接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年07月07日 16:05</p>
 * <p>@remark：</p>
 */
@Api(value = "dingRemindController", tags = "钉钉提醒相关接口")
@Slf4j
@RestController
@RequestMapping(value = "/ding/remind")
public class DingRemindController extends BaseController {

    @Resource
    private DingRemindService dingRemindService;

    @ApiOperation(value = "钉钉提醒，用于未完成工作提醒")
    @PostMapping(value = "/undone")
    public JsonResult remindUndoneJob(@RequestBody RemindUndoneJobPlanDTO remindUndoneJobPlanDTO) {

        User user = getUser();
        JsonResult jsonResult = dingRemindService.remindUndoneJob(user, remindUndoneJobPlanDTO);

        return jsonResult;
    }
}
