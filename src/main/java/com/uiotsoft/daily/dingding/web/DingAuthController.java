package com.uiotsoft.daily.dingding.web;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.request.OapiUserGetuserinfoRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.dingtalk.api.response.OapiUserGetuserinfoResponse;
import com.taobao.api.ApiException;
import com.uiotsoft.daily.common.domain.vo.JsonResult;
import com.uiotsoft.daily.common.util.CommonUtil;
import com.uiotsoft.daily.dingding.config.DingAppConfig;
import com.uiotsoft.daily.dingding.config.DingUrlConstant;
import com.uiotsoft.daily.dingding.domain.ConfigDTO;
import com.uiotsoft.daily.dingding.domain.ServiceResult;
import com.uiotsoft.daily.dingding.domain.UserDTO;
import com.uiotsoft.daily.dingding.exception.DingtalkEncryptException;
import com.uiotsoft.daily.dingding.service.DingAuthService;
import com.uiotsoft.daily.dingding.service.TokenService;
import com.uiotsoft.daily.dingding.util.JsApiSignature;
import com.uiotsoft.daily.module.entity.DailyViewUser;
import com.uiotsoft.daily.organization.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Optional;

/**
 * <p>DingLoginController 此类用于：钉钉企业内部应用免登（H5微应用）</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年05月18日 15:06</p>
 * <p>@remark：钉钉企业内部微应用DEMO, 实现了身份验证（免登）功能</p>
 */
@Api(value = "dingAuthController", tags = "钉钉企业内部应用免登（H5微应用）")
@Slf4j
@Controller
@RequestMapping(value = "/ding")
public class DingAuthController {

    @Resource
    private TokenService tokenService;

    @Resource
    private DingAppConfig dingAppConfig;

    @Resource
    private DingAuthService dingAuthService;

    /**
     * 欢迎页面，通过 /welcome 访问，判断后端服务是否启动
     *
     * @return 字符串 welcome
     */
    @ApiOperation(value = "日报跳转的首页地址")
    @RequestMapping("/login")
    public String mobileLogin() {
        return "mobile/dinglogin";
    }

    /**
     * 欢迎页面，通过 /welcome 访问，判断后端服务是否启动
     *
     * @return 字符串 welcome
     */
    @ApiOperation(value = "日报跳转的首页地址")
    @RequestMapping("/index")
    public String mobileIndex() {
        return "mobile/index";
    }

    /**
     * 钉钉跳转到页面
     *
     * @param request  请求
     * @param response 响应
     * @return ModelAndView 页面
     */
    @RequestMapping("/toIndex")
    public ModelAndView toDingView(HttpServletRequest request, HttpServletResponse response) {
        String view = request.getParameter("view");
        Map<String, Object> pMap = CommonUtil.getParameterMap(request);
        return new ModelAndView(view, pMap);
    }

    /**
     * 钉钉用户登录，显示当前登录用户的userId和名称
     *
     * @param authCode 免登临时authCode
     * @return 当前用户
     */
    @ApiOperation(value = "钉钉用户登录，显示当前登录用户的userId和名称")
    @PostMapping("/avoidLogin")
    @ResponseBody
    public JsonResult login(@RequestBody String authCode, HttpServletRequest request) {
        String accessToken;

        // 获取accessToken
        ServiceResult<String> accessTokenSr = tokenService.getAccessToken();
        if (!accessTokenSr.isSuccess()) {
            return JsonResult.fail(Integer.parseInt(accessTokenSr.getCode()), accessTokenSr.getMessage());
        }
        accessToken = accessTokenSr.getResult();

        JSONObject jsonObject = JSONObject.parseObject(authCode);
        String getAuthCode = (String) jsonObject.get("authCode");
        // 获取用户userId
        ServiceResult<String> userIdSr = getUserInfo(accessToken, getAuthCode);
        if (!userIdSr.isSuccess()) {
            return JsonResult.fail(Integer.parseInt(userIdSr.getCode()), userIdSr.getMessage());
        }

        // 获取用户详情
        JsonResult userInfo = this.getUser(accessToken, userIdSr.getResult());
        UserDTO userDTO = (UserDTO) userInfo.getResult();
        log.info("根据accessToken和用户userId查询出的用户信息 userDTO = {}", userDTO);
        HttpSession session = request.getSession();
        User user = this.getUserFromUserDTO(userDTO);
        session.setAttribute("date", String.valueOf(System.currentTimeMillis()));
        session.setAttribute("user", user);

        return userInfo;
    }

    /**
     * 封装User对象
     *
     * @param userDTO userDTO对象
     * @return User对象
     */
    private User getUserFromUserDTO(UserDTO userDTO) {

        User user = new User();
        user.setUserName(userDTO.getUserName());
        user.setTrueName(userDTO.getTrueName());
        user.setUserId(userDTO.getUserId());
        user.setAgentSid(userDTO.getAgentSid());
        user.setCreateDate(userDTO.getCreateDate());
        user.setDingUserId(userDTO.getDingUserId());
        user.setDepartment(userDTO.getAgentName());
        user.setFid(userDTO.getFid());
        user.setHandset(userDTO.getHandset());
        user.setHeadPortrait(userDTO.getHeadPortrait());
        user.setIsAdmin(userDTO.getIsAdmin());
        user.setIsAlarm(userDTO.getIsAlarm());
        user.setIsLeader(userDTO.getIsLeader());
        user.setLoginCount(userDTO.getLoginCount());
        user.setLoginLastDate(userDTO.getLoginLastDate());
        user.setLoginLastIp(userDTO.getLoginLastIp());
        user.setOrgEmail(userDTO.getOrgEmail());
        user.setPosition(userDTO.getPosition());
        user.setPassword(userDTO.getPassword());
        user.setPwdLastDate(userDTO.getPwdLastDate());
        user.setRoleId(userDTO.getRoleId());
        user.setUserLevel(userDTO.getUserLevel());
        user.setUserState(userDTO.getUserState());

        return user;
    }

    /**
     * 访问/user/getuserinfo接口获取用户userId
     *
     * @param accessToken access_token
     * @param authCode    临时授权码
     * @return 用户userId或错误信息
     */
    private ServiceResult<String> getUserInfo(String accessToken, String authCode) {
        DingTalkClient client = new DefaultDingTalkClient(DingUrlConstant.URL_GET_USER_INFO);
        OapiUserGetuserinfoRequest request = new OapiUserGetuserinfoRequest();
        request.setCode(authCode);
        request.setHttpMethod("GET");

        OapiUserGetuserinfoResponse response;
        try {
            response = client.execute(request, accessToken);
        } catch (ApiException e) {
            log.error("Failed to {}", DingUrlConstant.URL_GET_USER_INFO, e);
            return ServiceResult.failure(e.getErrCode(), "Failed to getUserInfo: " + e.getErrMsg());
        }
        if (!response.isSuccess()) {
            return ServiceResult.failure(response.getErrorCode(), response.getErrmsg());
        }

        return ServiceResult.success(response.getUserid());
    }

    /**
     * 访问/user/get 获取用户名称
     *
     * @param accessToken access_token
     * @param userId      用户userId
     * @return 用户名称或错误信息
     */
    private JsonResult getUser(String accessToken, String userId) {
        DingTalkClient client = new DefaultDingTalkClient(DingUrlConstant.URL_USER_GET);
        OapiUserGetRequest request = new OapiUserGetRequest();
        request.setUserid(userId);
        request.setHttpMethod("GET");

        OapiUserGetResponse response;
        try {
            response = client.execute(request, accessToken);
        } catch (ApiException e) {
            log.error("Failed to {}", DingUrlConstant.URL_USER_GET, e);
            return JsonResult.fail(Integer.parseInt(e.getErrCode()), "Failed to getUserName: " + e.getErrMsg());
        }

        UserDTO user = this.assembleUserDTO(response, accessToken);

        return JsonResult.ok(user);
    }

    /**
     * 封装返回的用户信息
     *
     * @param response    response
     * @param accessToken accessToken
     * @return 用户信息
     */
    private UserDTO assembleUserDTO(OapiUserGetResponse response, String accessToken) {

        UserDTO user = new UserDTO();
        String userid = response.getUserid();
        String mobile = response.getMobile();
        try {
            DailyViewUser dailyViewUser = Optional.ofNullable(dingAuthService.getUserByUserId(userid))
                    .orElse(dingAuthService.getUserByUsername(mobile));

            BeanUtil.copyProperties(dailyViewUser, user);
            user.setAccessToken(accessToken);
            user.setName(response.getName());
            user.setAvatar(response.getAvatar());
        } catch (Exception e) {
            log.error("E|DingAuthController|assembleUserDTO()|根据用户钉钉userId或者用户钉钉手机号mobile查询用户信息失败！");
        }

        return user;
    }

    @ApiOperation(value = "钉钉配置接口")
    @PostMapping("/config")
    @ResponseBody
    public JsonResult config(HttpServletRequest request) {
        ConfigDTO config = new ConfigDTO();

        String url = request.getRequestURL().toString();
        String queryString = request.getQueryString();
        if (queryString != null) {
            url = url + queryString;
        }

        ServiceResult<String> jsTicketSr = tokenService.getJsTicket();
        if (!jsTicketSr.isSuccess()) {
            return JsonResult.fail(Integer.parseInt(jsTicketSr.getCode()), jsTicketSr.getMessage());
        }

        config.setAgentId(dingAppConfig.getAgentId());
        config.setCorpId(dingAppConfig.getCorpId());
        config.setJsticket(jsTicketSr.getResult());
        config.setNonceStr(JsApiSignature.genNonce());
        config.setTimeStamp(System.currentTimeMillis() / 1000);
        String sign;
        try {
            sign = JsApiSignature.sign(url, config.getNonceStr(), config.getTimeStamp(), config.getJsticket());
        } catch (DingtalkEncryptException e) {
            return JsonResult.fail(e.getCode(), e.getMessage());
        }
        config.setSignature(sign);
        return JsonResult.ok(config);
    }
}
