/**
 * @(#)LogCompare.java 2020/6/30 15:31
 * Copyright HeNan UiotSoft. All rights reserved.
 */

package com.uiotsoft.daily.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * UpdateDataCompare.java此类用于记录修改比较操作日志
 *
 * @author: lizy
 * @date: 2020/6/30
 * @remark:
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface UpdateDataCompare {

    /**
     * 中文名称
     *
     * @return String
     */
    String name();

    /**
     * Date 如何格式化，默认为空
     * @return String
     */
    String dateFormat() default "";

}
