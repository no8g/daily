package com.uiotsoft.daily.common.annotation;

import java.lang.annotation.*;

/**
 * 自定义操作日志注解
 *
 * @Author chencg
 * @Date 11:16 2020/4/13
 **/
//注解放置的目标位置,METHOD是可注解在方法级别上
@Target(ElementType.METHOD)
//注解在哪个阶段执行
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Operation {

    // 操作模块
    String operModul() default "";

    // 操作类型
    String operType() default "";

    // 操作说明
    String operDesc() default "";
}