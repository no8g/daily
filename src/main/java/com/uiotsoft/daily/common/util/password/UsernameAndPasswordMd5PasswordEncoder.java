package com.uiotsoft.daily.common.util.password;

import com.uiotsoft.daily.common.util.Md5Util;

/**
 * <p>UsernameAndPasswordMD5PasswordEncoder.java此类用于web配置</p>
 * <p>@author:czj</p>
 * <p>@date:2018年11月16日</p>
 * <p>@remark:</p>
 */
public class UsernameAndPasswordMd5PasswordEncoder implements PasswordEncoder {

	
	@Override
	public String encode(String password, String... salt) {
		
		if(salt == null){
			throw new RuntimeException("salt为空");
		}
		System.out.println("salt:" + salt + "  ->" +salt.length);
		if(salt.length != 1){
		    throw new RuntimeException("期望的salt为用户名");
		}

		return Md5Util.stringToMd5(salt[0] + password);
	}
	
	public static void main(String[] args) {
		String pwd = new UsernameAndPasswordMd5PasswordEncoder().encode("035219", "15639035219");
		System.out.println(pwd);
	}

}
