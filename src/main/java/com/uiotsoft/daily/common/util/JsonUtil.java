/**
 * @(#)JsonUtil.java 2015-11-9 下午5:17:22
 *
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>JsonUtil.java此类用于</p>
 * <p>@author:sxb</p>
 * <p>@date:2015-11-9</p>
 * <p>@remark:</p>
 */
public class JsonUtil {
	private static Logger logger = LogManager.getLogger(JsonUtil.class);
    private  static ObjectMapper objectMapper = new ObjectMapper();
    public static String objectTojson(Object object){
        return JSON.toJSONString(object, SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteNullNumberAsZero, SerializerFeature.WriteDateUseDateFormat);
    }
    

    public static String listTojson(List list){
        return JSON.toJSONString(list, SerializerFeature.WriteDateUseDateFormat);
    }
    /**
     * 把json数据转换成类 T
     *
     * @param json
     * @param clazz
     * @return
     */
    public static <T> T fromJson(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }
    /**
     * 把json数据转换成 list<T> 类型
     *
     * @param json
     * @param clazz
     * @return
     */
    public static <T> List<T> fromJsonToList(String json, Class<T> clazz) {
        return JSON.parseArray(json, clazz);
    }
    /**
     * 字符串Json格式转换为对象Map
     * @param strJson {"username":"sxb"}
     * @return
     */
    public static Map<String, Object> jsonToMap(String strJson){
        Map<String, Object> jsoMap = new HashMap<String, Object>(8);
        try {
            jsoMap = JSONObject.parseObject(strJson,Map.class);
        } catch (JSONException e) {
            logger.error("Json格式转换为对象Map入参strJson打印：{}", strJson);
        	logger.error("Json格式转换为对象Map异常", e);
        }
        
        return jsoMap;
    }
    /**
     * 字符串Json 转换为对象List
     * @param strJson [{"username":"sxb"}]
     * @return
     */
    public static List<Map<String, Object>> jsonToList(String strJson){
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            list = JSONObject.parseObject(strJson, List.class);
        } catch (JSONException e) {
        	logger.error("Json转换为对象List入参strJson打印：{}", strJson);
        	logger.error("Json转换为对象List异常", e);
        }
        return list;
    }
    /**
     * Json字符数组  转换为对象list
     * @param 
     * @return
     */
    public static List<Map<String, Object>> jsonarrToList(String strJson){
       List<Map<String, Object>> listObjectSec = new ArrayList<Map<String, Object>>();
      try {
        listObjectSec = JSONArray.parseObject(strJson,List.class);
        }
       catch (JSONException e) {
           logger.error("Json转换为对象List入参strJson打印：{}", strJson);
    	   logger.error("Json转换为对象List异常", e);
         }
        System.out.println(listObjectSec);
	    return listObjectSec;
    }

    /**
     * 将json转换成对象Class
     * @param src
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T>T jsonToObject(String src, Class<T> clazz){
        if(src == null || src == "" || clazz == null){
            return null;
        }
        try {
            return clazz.equals(String.class) ? (T) src : objectMapper.readValue(src,clazz);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
    	String strArr = "[{\"0\":\"zhangsan\",\"1\":\"lisi\",\"2\":\"wangwu\",\"3\":\"maliu\"}," +
                "{\"00\":\"zhangsan\",\"11\":\"lisi\",\"22\":\"wangwu\",\"33\":\"maliu\"}]";
    	//jsonarrToList(strArr);
    	jsonarrToList(strArr);
    	
    }
}
