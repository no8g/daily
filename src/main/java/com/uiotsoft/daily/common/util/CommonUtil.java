/**
 * @(#)CommonUtil.java 2018年4月10日 下午3:32:04
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.common.util;

import com.uiotsoft.daily.common.constant.CommonConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * CommonUtil.java此类用于
 * </p>
 * <p>
 *
 * @author:wangyk </p>
 * <p>
 * @date:2018年4月10日 </p>
 * <p>
 * @remark: </p>
 */
public class CommonUtil {
    /**
     * 从request中获得参数Map，并返回可读的Map
     *
     * @param request 请求
     * @return 参数Map
     */
    public static Map<String, Object> getParameterMap(HttpServletRequest request) {
        // 参数Map
        Map parameters = request.getParameterMap();
        HttpSession session = request.getSession();
        Enumeration<String> enu = session.getAttributeNames();

        // 返回值Map
        Map<String, Object> returnMap = new HashMap<String, Object>(16);
        Iterator entries = parameters.entrySet().iterator();
        Map.Entry entry;
        while (entries.hasNext()) {
            String name = "";
            String value = "";
            entry = (Map.Entry) entries.next();
            name = (String) entry.getKey();
            Object valueObj = entry.getValue();
            if (null == valueObj) {
                value = "";
            } else if (valueObj instanceof String[]) {
                String[] values = (String[]) valueObj;
                for (int i = 0; i < values.length; i++) {
                    value += values[i] + ",";
                }
                value = value.substring(0, value.length() - 1);
            } else {
                value = valueObj.toString().trim();
            }
            returnMap.put(name, value);
        }
        // session的值覆盖request中的值
        while (enu.hasMoreElements()) {
            String key = enu.nextElement();
            returnMap.put(key, session.getAttribute(key));
        }

        return returnMap;
    }

    /**
     * 生成订单编号
     */
    public static String createcId() {
        Random rand = new Random();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        DecimalFormat df = new DecimalFormat("0000");
        int n = rand.nextInt(10000);
        String cid = sdf.format(new Date()) + df.format(n);
        return cid;
    }

    /**
     * 汉字转换为Unicode
     *
     * @param s
     * @return
     */
    public static String toUnicode(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            if (s.charAt(i) <= 256) {
                sb.append("\\u00");
            } else {
                sb.append("\\u");
            }
            sb.append(Integer.toHexString(s.charAt(i)));
        }
        return sb.toString();
    }

    /**
     * Unicode转换为中文
     *
     * @param utfString
     * @return
     */
    public static String convert(String utfString) {
        StringBuilder sb = new StringBuilder();
        int i = -1;
        int pos = 0;

        while ((i = utfString.indexOf(CommonConstants.LETTER_LOWERCASE_U, pos)) != -1) {
            sb.append(utfString.substring(pos, i));
            if (i + 5 < utfString.length()) {
                pos = i + 6;
                sb.append((char) Integer.parseInt(utfString.substring(i + 2, i + 6), 16));
            }
        }

        return sb.toString();
    }

}
