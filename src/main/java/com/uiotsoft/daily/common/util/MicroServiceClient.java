package com.uiotsoft.daily.common.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.uiotsoft.daily.account.dto.AccountDTO;
import com.uiotsoft.daily.account.dto.AccountQueryParams;
import com.uiotsoft.daily.account.dto.TenantDTO;
import com.uiotsoft.daily.account.dto.TenantRoleDTO;
import com.uiotsoft.daily.account.exception.ApplicationException;
import com.uiotsoft.daily.account.exception.ErrorCode;
import com.uiotsoft.daily.common.constant.CommonConstants;
import com.uiotsoft.daily.common.util.password.UsernameAndPasswordMd5PasswordEncoder;
import com.uiotsoft.micro.api.UiotClient;
import com.uiotsoft.micro.api.UiotHashMap;
import com.uiotsoft.micro.api.exception.ApiException;
import com.uiotsoft.micro.api.request.UiotRequest;
import com.uiotsoft.micro.constant.Constants;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <p>MicroServiceClient.java此类用于统一用户接口</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 */
@Component
@Data
@Slf4j
public class MicroServiceClient {

    @Autowired
    private UiotClient uiotClient;

    @Value("${unified.applicationCode}")
    private String applicationCode;

    @Value("${tenantType}")
    private String tenantType;

    /**
     * 解绑账号和租户关系
     *
     * @param username 用户名
     * @param tenantId 租户id
     * @return
     */
    public boolean deleteAccountTenant(String username, Long tenantId) {
        UiotRequest uiotRequest = new UiotRequest();
        String api = String.format("/user/unbind/account/%s/tenant/%s", username, tenantId);
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_DELETE);
        JSONObject response = new JSONObject();
        try {
            response = uiotClient.execute(uiotRequest);
            Integer code = response.getInteger("code");
            if (code != 0) {
                log.error(response.getString("msg"));
                throw new ApplicationException(ErrorCode.E_110107);
            }
        } catch (ApiException e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110107);
        }
        return true;
    }

    /**
     * 创建账号
     *
     * @param username
     * @param password 调用时不需要加密处理，默认 手机号后6位
     * @param mobile
     * @return
     */
    public boolean createAccount(String username, String password, String mobile) {
        UiotRequest uiotRequest = new UiotRequest();
        String api = String.format("/user/account");
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_POST);
        Map<String, String> prams = new HashMap<>(8);
        prams.put("username", username);
        prams.put("mobile", mobile);
        prams.put("password", new UsernameAndPasswordMd5PasswordEncoder().encode(password, username));
        uiotRequest.setBodyParams(prams);
        JSONObject response = new JSONObject();
        try {
            response = uiotClient.execute(uiotRequest);
            Integer code = response.getInteger("code");
            if (code != 0) {
                log.error(response.getString("msg"));
                throw new ApplicationException(ErrorCode.E_110101);
            }
        } catch (ApiException e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110101);
        }
        return true;
    }

    /**
     * 绑定账号到租户下
     *
     * @param username
     * @param tenantId
     * @return
     */
    public void bindAccountToTenant(String username, Long tenantId) {
        UiotRequest uiotRequest = new UiotRequest();
        String api = String.format("/user/bind/account/%s/tenant/%s", username, tenantId);
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_PUT);
        JSONObject response = new JSONObject();
        try {
            response = uiotClient.execute(uiotRequest);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.E_110106, response.getString("msg"));
            }
        } catch (ApiException e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110106);
        }
    }


    /**
     * 验证账号密码是否正确
     *
     * @param userName
     * @param password
     * @return
     */
    public boolean isCertificate(String userName, String password) {
        boolean result = true;
        String api = String.format("/user/certificate/");
        Map<String, String> prams = new HashMap<>(8);
        prams.put("username", userName);
        prams.put("password", password);
        UiotRequest uiotRequest = new UiotRequest();
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_POST);
        uiotRequest.setBodyParams(prams);
        try {
            JSONObject response = uiotClient.execute(uiotRequest);
            if (response.getIntValue(CommonConstants.EN_WORD_CODE) != 0) {
                result = false;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110102);
        }
        return result;
    }

    /**
     * 检查该用户名是否存在
     *
     * @param userName
     * @return
     */
    public Map<String, Object> checkAccountExist(String userName) {
        boolean exist = false;
        Integer code = 0;
        UiotRequest uiotRequest = new UiotRequest();
        String api = String.format("/user/username/%s", userName);
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_GET);
        JSONObject response = new JSONObject();
        try {
            response = uiotClient.execute(uiotRequest);
            code = response.getInteger("code");
            exist = response.getBooleanValue("result");
        } catch (ApiException e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110102);
        }
        Map<String, Object> map = new HashMap<>(8);
        map.put("code", code);
        map.put("exist", exist);
        return map;
    }

    /**
     * 用户认证
     *
     * @param userName
     * @return
     */
    public AccountDTO userCertificate(String userName, String password) {
        String api = String.format("/user/certificate/");
        Map<String, String> prams = new HashMap<>(8);
        prams.put("username", userName);
        prams.put("password", password);
        UiotRequest uiotRequest = new UiotRequest();
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_POST);
        uiotRequest.setBodyParams(prams);
        AccountDTO accountDTO = new AccountDTO();
        try {
            JSONObject response = uiotClient.execute(uiotRequest);
            if (response.getIntValue(CommonConstants.EN_WORD_CODE) != 0) {
                return null;
            }
            String accountDtoJson = response.getString("result");
            accountDTO = JsonUtil.fromJson(accountDtoJson, AccountDTO.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110102);
        }
        return accountDTO;
    }

    /**
     * 修改手机号
     *
     * @param userName
     * @return
     */
    public boolean modifyMobile(String userName, String mobile) {
        boolean flag = false;
        String api = String.format("/user/account/mobile");
        Map<String, String> prams = new HashMap<>(8);
        prams.put("username", userName);
        prams.put("mobile", mobile);
        UiotRequest uiotRequest = new UiotRequest();
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_PUT);
        uiotRequest.setBodyParams(prams);
        try {
            JSONObject response = uiotClient.execute(uiotRequest);
            if (response.getIntValue(CommonConstants.EN_WORD_CODE) != 0) {
                throw new ApplicationException(response.getString("msg"));
            }
            flag = true;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110102);
        }
        return flag;
    }

    /**
     * 查询用户所属某租户类型租户列表
     *
     * @param userName
     * @return
     */
    public List<TenantDTO> queryTenant(String userName, String tenantType) {
        List<TenantDTO> list = new ArrayList<>();
        String api = String.format("/user/tenant/username/%s/tenantTypeCode/%s", userName, tenantType);
        UiotRequest uiotRequest = new UiotRequest();
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_GET);
        try {
            JSONObject response = uiotClient.execute(uiotRequest);
            if (response.getIntValue(CommonConstants.EN_WORD_CODE) != 0) {
                return null;
            }
            String result = response.getString("result");
            list = JsonUtil.fromJsonToList(result, TenantDTO.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110102);
        }
        return list;
    }

    /**
     * 创建租户
     *
     * @param name
     * @return
     */
    public Long createTenant(String name) {
        UiotRequest uiotRequest = new UiotRequest();
        String api = String.format("/user/tenant/");
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_POST);
        Map<String, String> prams = new HashMap<>(8);
        prams.put("name", name);
        prams.put("tenantTypeCode", tenantType);
        uiotRequest.setBodyParams(prams);
        JSONObject response = new JSONObject();
        try {
            response = uiotClient.execute(uiotRequest);
            Integer code = response.getInteger("code");
            if (code != 0) {
                log.error(response.getString("msg"));
            }
            return response.getJSONObject("result").getLong("id");
        } catch (ApiException e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110102);
        }
    }

    /**
     * 忘记密码
     *
     * @param username
     * @param password
     * @return
     */
    public boolean updatePasswordNoOld(String username, String password) {
        String api = String.format("user/account/password/noOld");
        Map<String, String> prams = new HashMap<>(8);
        prams.put("username", username);
        prams.put("newPassword", new UsernameAndPasswordMd5PasswordEncoder().encode(password, username));
        UiotRequest uiotRequest = new UiotRequest();
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_PUT);
        uiotRequest.setBodyParams(prams);
        try {
            JSONObject response = uiotClient.execute(uiotRequest);
            if (response.getIntValue(CommonConstants.EN_WORD_CODE) != 0) {
                throw new ApplicationException(response.getString("msg"));
            }
            return true;
        } catch (Exception e) {
            log.error(ErrorCode.E_110102.getDesc() + ":" + e.getMessage());
            return false;
        }
    }

    /**
     * 修改密码
     *
     * @param username
     * @param oldPassword
     * @param newPassword
     */
    public void updatePassword(String username, String oldPassword, String newPassword) {
        String api = String.format("/user/account/password");
        Map<String, String> prams = new HashMap<>(8);
        prams.put("username", username);
        prams.put("newPassword", new UsernameAndPasswordMd5PasswordEncoder().encode(newPassword, username));
        prams.put("oldPassword", new UsernameAndPasswordMd5PasswordEncoder().encode(oldPassword, username));
        UiotRequest uiotRequest = new UiotRequest();
        uiotRequest.setApi(api);
        uiotRequest.setMethod(Constants.METHOD_PUT);
        uiotRequest.setBodyParams(prams);
        try {
            JSONObject response = uiotClient.execute(uiotRequest);
            if (response.getIntValue(CommonConstants.EN_WORD_CODE) != 0) {
                throw new UsernameNotFoundException(response.getString("msg"));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationException(ErrorCode.E_110102);
        }
    }

    /**
     * 绑定租户所属租户类型的角色至拥有者
     *
     * @param username
     * @param tentantId
     * @param roleList
     */
    public void bindRole(String username, Long tentantId, List<String> roleList) {
        String owner = String.format("%s_%s", username, tentantId);
        UiotRequest request = new UiotRequest();
        String api = String.format("/authorization/%s", owner);
        String method = Constants.METHOD_POST;
        request.setApi(api);
        request.setMethod(method);
        request.setBodyParams(roleList);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(response.getString("msg"));
            }
        } catch (ApiException e) {
            e.printStackTrace();
            throw new ApplicationException(ErrorCode.E_110104);
        }
    }

    /**
     * 将用户在此租户下的已绑定过的角色进行解绑
     *
     * @param userName
     * @param tenantId
     * @param roleList
     */
    public void unBindRole(String userName, Long tenantId, List<String> roleList) {
        String owner = String.format("%s_%s", userName, tenantId);
        UiotRequest request = new UiotRequest();
        String api = String.format("/authorization/%s", owner);
        String method = Constants.METHOD_DELETE;
        request.setApi(api);
        request.setMethod(method);
        request.setBodyParams(roleList);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.CUSTOM, response.getString("msg"));
            }
        } catch (ApiException e) {
            log.error(ErrorCode.E_110109.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110109);
        }
    }

    /**
     * 获取租户类型下的角色
     *
     * @param tenantTypeCode
     */
    public JSONArray getTenantTypeRole(String tenantTypeCode) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/tenantType/%s", tenantTypeCode);
        request.setApi(api);
        request.setMethod(Constants.METHOD_GET);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(response.getString("msg"));
            }
            JSONArray roles = response.getJSONObject("result").getJSONArray("roles");
            System.out.println(roles);
            return roles;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110104);
        }
    }

    /**
     * 登陆租户
     *
     * @return 授权信息，角色-权限关系
     * @author:陈曾杰
     * @date:2018年11月27日 下午5:06:59
     */
    public Map<String, Object> userSession(Long tenantId, String userName, String password) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/session");
        request.setApi(api);
        request.setMethod(Constants.METHOD_POST);
        Map<String, Object> bodyParasms = new HashMap<>(8);
        bodyParasms.put("tenantId", tenantId);
        bodyParasms.put("username", userName);
        bodyParasms.put("password", password);
        request.setBodyParams(bodyParasms);
        JSONObject rolePrivilegeResponse;
        try {
            rolePrivilegeResponse = uiotClient.execute(request);
            JSONObject rolePrivilegeResponseResult = rolePrivilegeResponse.getJSONObject("result");
            Map<String, Object> rolePrivilegeMap =
                    rolePrivilegeResponseResult.getJSONObject("rolePrivilegeMap");
            return rolePrivilegeMap;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110103.getDesc(), e);
        }
        return null;
    }

    /**
     * 根据角色编码获取权限树
     *
     * @return
     * @author:陈曾杰
     * @date:2018年11月27日 下午5:06:59
     */
    public Map<String, Object> authorizationPrivilegeTreeRoles(String roleCodes) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/authorization/privilegeTree/roles/%s", roleCodes);
        request.setApi(api);
        request.setMethod(Constants.METHOD_GET);
        JSONObject privilegeResponse;
        try {
            privilegeResponse = uiotClient.execute(request);
            JSONObject privilegeResponseResult = privilegeResponse.getJSONObject("result");
            return privilegeResponseResult;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110103.getDesc(), e);
        }
        return null;
    }

    /**
     * 获取用户在此租户下的角色
     *
     * @param userName
     * @param tenantId
     */
    public List<String> authorization(String userName, Long tenantId) {
        String owner = String.format("%s_%s", userName, tenantId);
        UiotRequest request = new UiotRequest();
        String api = String.format("/authorization/%s", owner);
        String method = Constants.METHOD_GET;
        request.setApi(api);
        request.setMethod(method);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.CUSTOM, response.getString("msg"));
            }
            List<String> roleCodeList = new ArrayList<String>();
            JSONObject rolePrivilegeResponseResult = response.getJSONObject("result");
            Map<String, Object> rolePrivilegeMap = rolePrivilegeResponseResult.getJSONObject("rolePrivilegeMap");
            for (Entry<String, Object> entry : rolePrivilegeMap.entrySet()) {
                roleCodeList.add(entry.getKey());
            }
            return roleCodeList;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110104);
        }
    }

    /**
     * 查询租户下的角色
     *
     * @param tenantId
     * @return
     * @author:陈曾杰
     * @date:2018年11月30日 下午8:30:25
     */
    public JSONArray queryTenantRole(Long tenantId) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/tenant/%s/role", tenantId);
        String method = Constants.METHOD_GET;
        request.setApi(api);
        request.setMethod(method);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(response.getString("msg"));
            }
            JSONArray roles = response.getJSONArray("result");
            return roles;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110102);
        }
    }

    /**
     * 用户与租户解绑
     *
     * @param userName
     * @param tenantId
     * @author:陈曾杰
     * @date:2018年12月3日 下午3:07:51
     */
    public void userUnbindAccount(String userName, Long tenantId) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/unbind/account/%s/tenant/%s", userName, tenantId);
        String method = Constants.METHOD_DELETE;
        request.setApi(api);
        request.setMethod(method);
        try {
            JSONObject response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.CUSTOM, response.getString("msg"));
            }
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110102);
        }
    }

    /**
     * 加载指定应用的资源,获取权限
     *
     * @return menu
     * @author:陈曾杰
     * @date:2018年11月27日 下午5:06:59
     */
    public JSONObject getMenu(List<String> privileageCodes) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/resource/%s", applicationCode);
        request.setApi(api);
        request.setMethod(Constants.METHOD_POST);
        request.setBodyParams(privileageCodes);
        JSONObject resuuceResponse;
        try {
            resuuceResponse = uiotClient.execute(request);
            // 资源转换为本地权限
            JSONObject result = resuuceResponse.getJSONObject("result");
            if (result.isEmpty()) {
                throw new ApplicationException(ErrorCode.CUSTOM, "没有权限");
            }
            JSONObject appRes = result.getJSONObject("appRes");
            JSONObject menu = appRes.getJSONObject("menu");
            return menu;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110103.getDesc(), e);
        }
        return null;
    }

    /**
     * 根据应用编码加载指定应用的资源,获取权限
     *
     * @return menu
     * @author:lzy
     * @date:2018年11月27日 下午5:06:59
     */
    public JSONObject getMenu(List<String> privileageCodes, String menuCode) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/resource/%s", menuCode);
        request.setApi(api);
        request.setMethod(Constants.METHOD_POST);
        request.setBodyParams(privileageCodes);
        JSONObject resuuceResponse;
        try {
            resuuceResponse = uiotClient.execute(request);
            // 资源转换为本地权限
            JSONObject result = resuuceResponse.getJSONObject("result");
            if (result.isEmpty()) {
                throw new ApplicationException(ErrorCode.CUSTOM, "没有权限");
            }
            JSONObject appRes = result.getJSONObject("appRes");
            JSONObject menu = appRes.getJSONObject("menu");
            return menu;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110103.getDesc(), e);
        }
        return null;
    }

    /**
     * 分页查询账号信息
     *
     * @param pageNo
     * @param pageSize
     * @param params
     * @return
     * @author:陈曾杰
     * @date:2019年3月8日
     */
    public Page<AccountDTO> pageAccount(Integer pageNo, Integer pageSize, AccountQueryParams params) {
        UiotRequest request = new UiotRequest();
        request.setApi(String.format("/user/account/page"));
        request.setMethod(Constants.METHOD_POST);
        UiotHashMap urlParams = new UiotHashMap();
        urlParams.put("pageNo", pageNo);
        urlParams.put("pageSize", pageSize);
        request.setUrlParams(urlParams);
        request.setBodyParams(params);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.E_110108);
            }
            JSONObject result = response.getJSONObject("result");
            JSONArray accounts = result.getJSONArray("content");
            int total = result.getInteger("totalElements");
            List<AccountDTO> accountList = accounts.toJavaList(AccountDTO.class);
            Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
            Page<AccountDTO> page = new PageImpl<>(accountList, pageable, total);
            return page;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110103.getDesc(), e);
        }
        return null;
    }

    /**
     * 获取账号信息
     *
     * @param username
     * @return
     * @author:陈曾杰
     * @date:2019年3月8日
     */
    public JSONObject getAccount(String username) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/account/%s", username);
        String method = Constants.METHOD_GET;
        request.setApi(api);
        request.setMethod(method);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(response.getString("msg"));
            }
            JSONObject accountJson = response.getJSONObject("result");
            return accountJson;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110102);
        }
    }

    /**
     * 租户内新增角色
     *
     * @param tenantId
     * @param role
     * @author:陈曾杰
     * @date:2019年3月8日
     */
    public void createTenantRole(Long tenantId, TenantRoleDTO role) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/tenant/%s/role", tenantId);
        String method = Constants.METHOD_POST;
        request.setApi(api);
        request.setMethod(method);
        String roleCode = String.format("%s_%s", role.getCode(), tenantId);
        role.setCode(roleCode);
        request.setBodyParams(role);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.CUSTOM, response.getString("msg"));
            }
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110102);
        }
    }

    /**
     * 修改租户内角色：修改角色的名称
     *
     * @param tenantId
     * @param role
     * @author:陈曾杰
     * @date:2019年3月8日
     */
    public void modifyTenantRole(Long tenantId, TenantRoleDTO role) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/tenant/%s/role", tenantId);
        String method = Constants.METHOD_PUT;
        request.setApi(api);
        request.setMethod(method);
        request.setBodyParams(role);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.CUSTOM, response.getString("msg"));
            }
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110102);
        }
    }

    /**
     * 删除租户下角色
     *
     * @param tenantId
     * @param roleCode
     * @author:陈曾杰
     * @date:2019年3月8日
     */
    public void removeTenantRole(Long tenantId, String roleCode) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/tenant/%s/role/%s", tenantId, roleCode);
        String method = Constants.METHOD_DELETE;
        request.setApi(api);
        request.setMethod(method);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.CUSTOM, response.getString("msg"));
            }
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110102);
        }

    }

    /**
     * 获取角色下用户
     *
     * @param roleCode
     */
    public List<String> authorizationByRoleCode(String roleCode) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/authorization/owner/byRoleCode");
        String method = Constants.METHOD_POST;
        request.setApi(api);
        request.setMethod(method);
        UiotHashMap urlParams = new UiotHashMap();
        urlParams.put("roleCode", roleCode);
        urlParams.put("pageNo", 1);
        urlParams.put("pageSize", 9999);
        request.setUrlParams(urlParams);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            Integer code = response.getInteger("code");
            if (code != 0) {
                throw new ApplicationException(ErrorCode.CUSTOM, response.getString("msg"));
            }
            List<String> accountList = new ArrayList<String>();
            JSONObject responseResult = response.getJSONObject("result");
            JSONArray accounts = responseResult.getJSONArray("content");
            List<Map> list = accounts.toJavaList(Map.class);
            for (Map map : list) {
                Object owner = map.get("owner");
                if (owner != null && !"".equals(owner)) {
                    accountList.add(owner.toString());
                }
            }
            return accountList;
        } catch (ApiException e) {
            log.error(ErrorCode.E_110102.getDesc(), e);
            throw new ApplicationException(ErrorCode.E_110104);
        }
    }
}
