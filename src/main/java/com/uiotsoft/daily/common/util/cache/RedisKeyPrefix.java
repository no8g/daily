/**
 * @(#)RedisKeyPrefix.java 2018年12月10日 下午4:35:51
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.common.util.cache;


/**
 * <p>RedisKeyPrefix.java此类用于redis前缀常量配置</p>
 * <p>格式(项目名:表名(功能名):id:）</p>
 * <p>@author:陈曾杰</p>
 * <p>@date:2018年12月10日</p>
 * <p>@remark:</p>
 */
public interface RedisKeyPrefix {

    /**
     * 登录tokenId
     */
    public static final String LOGIN_ACCESS_TOKEN = "flowapplication:login:accesstoken:%s";

    /**
     * accessToken
     */
    public static final String DING_DING_ACCESS_TOKEN = "flowapplication:dingding:accesstoken:";

}
