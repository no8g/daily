/**
 * @(#)MD5Util.java 2018年4月18日 下午6:22:47
 *
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.common.util;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * <p>MD5Util.java此类用于MD5</p>
 * <p>@author:wangyk</p>
 * <p>@date:2018年4月18日</p>
 * <p>@remark:</p>
 */
public class Md5Util {

    /**
     * 全局数组
     */
    private final static String[] STR_DIGITIS = {"0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    public Md5Util() {}
    /**
     * MD5加码 生成32位md5码 
     * @param inStr
     * @return
     */
    public static String stringToMd5(String inStr){
      MessageDigest md5 = null;  
      try{  
          md5 = MessageDigest.getInstance("MD5");  
      }catch (Exception e){  
        System.out.println(e.toString());  
        e.printStackTrace();  
        return "";  
      }  
      char[] charArray = inStr.toCharArray();  
      byte[] byteArray = new byte[charArray.length];  
      for (int i = 0; i < charArray.length; i++) {
          byteArray[i] = (byte) charArray[i];
      }
          byte[] md5Bytes = md5.digest(byteArray);  
          StringBuffer hexValue = new StringBuffer();  
          for (int i = 0; i < md5Bytes.length; i++){  
            int val = ((int) md5Bytes[i]) & 0xff;  
            if (val < 16) {
                hexValue.append("0");
            }
              hexValue.append(Integer.toHexString(val));  
          }  
           return hexValue.toString();  
      }

    /**
     * 返回形式为数字跟字符串
     * @param bByte
     * @return
     */
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return STR_DIGITIS[iD1] + STR_DIGITIS[iD2];
    }

    /**
     * 返回形式只为数字
     * @param bByte
     * @return
     */
    private static String byteToNum(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += 256;
        }
        return String.valueOf(iRet);
    }

   /**
    * 转换字节数组为16进制字串
    * @param bByte
    * @return
    */
    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }

    public static String getMd5Code(String strObj) {
        String resultString = null;
        try {
            resultString = new String(strObj);
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return resultString;
    }


    public static void main(String[] args) {
        String sn = UUID.randomUUID().toString().replaceAll("-", "");
        System.out.println(sn);
        System.out.println(sn.substring(sn.length() - 27));
    }
}
