package com.uiotsoft.daily.common.util.password;

/**
 * <p>ApiGateWayConfiguration.java此类用于web配置</p>
 * <p>@author:czj</p>
 * <p>@date:2018年11月16日</p>
 * <p>@remark:</p>
 */
public interface PasswordEncoder {

    /**
     * 加密
     *
     * @param password password
     * @param salt     salt
     * @return
     */
    String encode(String password, String... salt);

}
