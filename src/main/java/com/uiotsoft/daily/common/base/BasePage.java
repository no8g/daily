package com.uiotsoft.daily.common.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>BasePage 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月12日 15:43</p>
 * <p>@remark：</p>
 */
@ApiModel(value = "分页参数")
@Data
public class BasePage {

    @ApiModelProperty(value = "当前页", example = "1")
    private int pageNum = 1;

    @ApiModelProperty(value = "每页大小", example = "10")
    private int pageSize = 10;
}
