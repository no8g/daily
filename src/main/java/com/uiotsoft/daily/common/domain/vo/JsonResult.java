package com.uiotsoft.daily.common.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;


/**
 * 接口返回数据格式
 * @author liupengtao
 * @date 2019-04-22
 */
@Data
public class JsonResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 成功标志
     */
    private boolean success = true;

    /**
     * 返回处理消息
     */
    private String message = "操作成功！";

    /**
     * 返回代码
     */
    private Integer code = 0;

    /**
     * 返回数据对象 data
     */
    private T result;

    /**
     * 导入成功条数
     * */
    private Integer countSuccess;
    /**
     * 导入失败条数
     * */
    private Integer countFail;

    public JsonResult() {

    }

    public void success(String message) {
        this.message = message;
        this.code = 0;
        this.success = true;
    }

    public static JsonResult<Object> fail() {
        return fail(1, "操作失败");
    }

    public static JsonResult<Object> fail(String message) {
        return fail(1, message);
    }

    public static JsonResult<Object> fail(int code, String message) {
        JsonResult<Object> result = new JsonResult<Object>();
        result.setCode(code);
        result.setMessage(message);
        result.setSuccess(false);
        return result;
    }

    public static JsonResult<Object> ok() {
        JsonResult<Object> result = new JsonResult<Object>();
        result.setSuccess(true);
        result.setCode(0);
        return result;
    }

    public static JsonResult<Object> ok(String msg) {
        JsonResult<Object> result = new JsonResult<Object>();
        result.setSuccess(true);
        result.setCode(0);
        result.setMessage(msg);
        return result;
    }

    public static JsonResult<Object> ok(Object data) {
        JsonResult<Object> result = new JsonResult<Object>();
        result.setSuccess(true);
        result.setCode(0);
        result.setResult(data);
        return result;
    }

    /**
     * 成功 code=0
     *
     * @return true/false
     */
    @JsonIgnore
    public boolean isSuccess() {
        return 0 == this.code;
    }

    /**
     * 失败
     *
     * @return true/false
     */
    @JsonIgnore
    public boolean isFail() {
        return !isSuccess();
    }

}