package com.uiotsoft.daily.common.constant;

/**
 * <p>NumberConstants 此类用于：数字常量</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月10日 14:24</p>
 * <p>@remark：</p>
 */
public class NumberConstants {

    /**
     * 角色配置级别：1、总监，2、总监助理，3、部门经理或者部门副经理
     */
    public static final int NUMBER_ONE = 1;
    public static final int NUMBER_TWO = 2;
    public static final int NUMBER_THREE = 3;

    public static final int NUMBER_MINUS_ONE = -1;
    public static final int NUMBER_ZERO = 0;
    public static final int NUMBER_TWELVE = 12;
    public static final int NUMBER_FIFTEEN = 15;

    /**
     * 周几，1周日，2周一，3周二，4周三，5周四，6周五，7周六
     */
    public static final int DAY_OF_WEEK_SATURDAY = 7;
}
