package com.uiotsoft.daily.common.constant;

/**
 * <p>CommonConstants 此类用于：通用常量</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月13日 11:07</p>
 * <p>@remark：</p>
 */
public class CommonConstants {

    /**
     * 日报已读和未读常量
     */
    public static final String HAVE_READ = "已读";
    public static final String NO_READ = "未读";

    /**
     * 英文字母相关
     */
    public static final String CAPITAL_N = "N";
    public static final String LOWER_CASE_N = "n";

    /**
     * 布尔类型
     */
    public static final String FLAG_TRUE = "true";
    public static final String FLAG_FALSE = "false";
    public static final String REQUEST_UNKNOWN = "unknown";

    /**
     * IP地址相关
     */
    public static final String IP_ADDRESS_127 = "127.0.0.1";
    public static final String IP_ADDRESS_MAC = "0:0:0:0:0:0:0:1";

    /**
     * 浏览器相关常量
     */
    public static final String BROWSER_OPR = "OPR";
    public static final String BROWSER_CHROME = "Chrome";
    public static final String BROWSER_QQ_BROWSER = "QQBrowser";
    public static final String BROWSER_EDGE = "Edge";
    public static final String BROWSER_SE = "SE";
    public static final String BROWSER_SAFARI = "Safari";

    /**
     * 英文标点符号相关
     */
    public static final String EN_PUNCTUATION_COMMA = ",";

    /**
     * 英文单词相关
     */
    public static final String EN_WORD_MOBILE = "mobile";
    public static final String EN_WORD_CODE = "code";
    public static final String EN_WORD_CHILDS = "childs";
    public static final String EN_WORD_ROLE_PRIVILEGE_MAP = "rolePrivilegeMap";

    /**
     * 字符串数字
     */
    public static final String STRING_ZERO = "0";
    public static final String STRING_ONE = "1";

    /**
     * 部门id，当角色配置表中角色层级为1的时候，需要用户公司领导助理的部门id
     */
    public static final String RD_CENTER_CTO_TEL = "18538290858";
    public static final String RD_CENTER_DEPT_ID = "84681000011001";

    /**
     * 日报系统钉钉JSTICKET命名空间和ACCESSTOKEN命名空间
     */
    public static final String DAILY_DING_JS_TICKET = "dailyApplication:login:ticket:";
    public static final String DAILY_DING_ACCESS_TOKEN = "dailyApplication:login:accessToken:";

    /**
     * 匿名用户
     */
    public static final String ANONYMOUS_USER = "anonymousUser";

    /**
     * 斜杠 + u
     */
    public static final String LETTER_LOWERCASE_U = "\\u";

    /**
     * 星期六、星期日
     */
    public static final String CN_SATURDAY = "星期六";
    public static final String CN_SUNDAY = "星期日";

    /**
     * 邮箱常用常量，TO 收件人，CC 抄送人地址，BCC 密送人地址
     */
    public static final String MAIL_TO = "TO";
    public static final String MAIL_CC = "CC";
    public static final String MAIL_BCC = "BCC";
    /**
     * 根据MimeType类型的不同执行不同的操作，name
     */
    public static final String MAIL_MIMETYPE_NAME = "name";
    public static final String MAIL_MIMETYPE_PLAIN = "text/plain";
    public static final String MAIL_MIMETYPE_HTML = "text/html";
    public static final String MAIL_MIMETYPE_MULTIPART = "multipart/*";
    public static final String MAIL_MIMETYPE_MESSAGE = "message/rfc822";
    /**
     * 电脑系统 win
     */
    public static final String WINDOW_SYSTEM_WIN = "win";
    public static final String WINDOW_SYSTEM_OS_NAME = "os.name";
}
