package com.uiotsoft.daily.common.exception;

/**
 * 系统自定义异常码
 * @author lizy
 * @date 2020/06/02
 */
public enum MisErrorCode implements BaseErrorInfoInterface {

    ////////////////////////////////////日报异常编码//////////////////////////
    E_100100(100100,"获取当前登录人失败!"),
    E_100101(100101,"没有找到该流程类型!"),
    E_100102(100102,"没有找到下步办理人!"),
    E_100103(100103,"没有找到对应的流程决策信息!"),
    E_100104(100104,"没有找到对应的流程令牌信息!"),
    E_100105(100105,"新建审批失败，返回审批实例ID为空!"),
    E_100106(100106,"初始化根令牌失败!"),
    E_100107(100107,"没有找到对应的流程实例ID!"),

    ////////////////////////////////////组织信息异常编码//////////////////////////
    E_200100(200100,"获取用户手机号为空!"),

    ////////////////////////////////////数据操作错误定义编码//////////////////////////
    E_600100(600100,"空指针异常!"),

    E_UNKOWN(999999,"未知错误!");

    /**
     * 错误码
     */
    private int errorCode;

    /**
     * 错误描述
     */
    private String errorMsg;

    MisErrorCode(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String getErrorMsg() {
        return errorMsg;
    }

}
