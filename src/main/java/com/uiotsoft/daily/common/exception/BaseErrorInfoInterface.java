/**
 * @(#)BaseErrorInfoInterface.java 2020/6/2 14:05
 * Copyright HeNan UiotSoft. All rights reserved.
 */

package com.uiotsoft.daily.common.exception;

/**
 * BaseErrorInfoInterface.java此类用于
 * @author: lizy
 * @date: 2020/6/2
 * @remark:
 */
public interface BaseErrorInfoInterface {

    /**
     * 错误码
     * @return
     */
    int getErrorCode();

    /**
     * 错误描述
     * @return
     */
    String getErrorMsg();
}
