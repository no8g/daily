package com.uiotsoft.daily.common.exception;

import cn.hutool.core.util.StrUtil;

/**
 * 系统自定义异常
 * @author lizy
 * @date 2020/06/02
 */
public class MisException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    protected int errorCode;
    /**
     * 错误信息
     */
    protected String errorMsg;

    public MisException() {
        super();
    }

    public MisException(BaseErrorInfoInterface errorInfoInterface) {
        super(StrUtil.toString(errorInfoInterface.getErrorCode()));
        this.errorCode = errorInfoInterface.getErrorCode();
        this.errorMsg = errorInfoInterface.getErrorMsg();
    }

    public MisException(BaseErrorInfoInterface errorInfoInterface, Throwable cause) {
        super(StrUtil.toString(errorInfoInterface.getErrorCode()), cause);
        this.errorCode = errorInfoInterface.getErrorCode();
        this.errorMsg = errorInfoInterface.getErrorMsg();
    }

    public MisException(String errorMsg) {
        super(errorMsg);
        this.errorMsg = errorMsg;
    }

    public MisException(int errorCode, String errorMsg) {
        super(StrUtil.toString(errorCode));
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public MisException(int errorCode, String errorMsg, Throwable cause) {
        super(StrUtil.toString(errorCode), cause);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }


    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String getMessage() {
        return errorMsg;
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

}
