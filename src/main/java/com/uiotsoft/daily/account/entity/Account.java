/**
 * @(#)Account.java 2019年06月22日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>Account.java此类用于用户管理</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark:</p>
 */
@Data
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long accountId;

    /**
     * 用户名
     */
    private String username;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 密码
     */
    private String password;

}
