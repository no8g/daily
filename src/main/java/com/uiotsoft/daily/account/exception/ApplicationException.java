package com.uiotsoft.daily.account.exception;

/**
 * <p>ApiGateWayConfiguration.java此类用于web配置</p>
 * <p>@author:czj</p>
 * <p>@date:2018年11月16日</p>
 * <p>@remark:</p>
 */
public class ApplicationException extends RuntimeException {


    private static final long serialVersionUID = 5565760508056698922L;

    private ErrorCode errorCode;

    public ApplicationException(ErrorCode errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public ApplicationException() {
        super();
    }

    public ApplicationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

    public ApplicationException(ErrorCode errorCode, String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
        this.errorCode = errorCode;
    }

    public ApplicationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ApplicationException(ErrorCode errorCode, String arg0, Throwable arg1) {
        super(arg0, arg1);
        this.errorCode = errorCode;
    }

    public ApplicationException(String arg0) {
        super(arg0);
    }

    public ApplicationException(ErrorCode errorCode, String arg0) {
        super(arg0);
        this.errorCode = errorCode;
    }

    public ApplicationException(Throwable arg0) {
        super(arg0);
    }

    public ApplicationException(ErrorCode errorCode, Throwable arg0) {
        super(arg0);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        if (errorCode == null) {
            return ErrorCode.UNKOWN;
        }
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

}
