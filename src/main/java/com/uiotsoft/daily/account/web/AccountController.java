/**
 * @(#)AccountController.java 2018年10月30日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.web;

import com.uiotsoft.daily.account.dto.*;
import com.uiotsoft.daily.account.exception.ApplicationException;
import com.uiotsoft.daily.account.exception.ErrorCode;
import com.uiotsoft.daily.account.service.AccountService;
import com.uiotsoft.daily.account.service.RoleService;
import com.uiotsoft.daily.common.domain.vo.PageResult;
import com.uiotsoft.daily.config.security.CurrUserDetails;
import com.uiotsoft.micro.api.exception.ApiException;
import com.uiotsoft.micro.common.domain.RestResponse;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * <p>
 * AccountController.java此类用于账号管理
 * </p>
 * <p>
 * @author:陈曾杰
 * </p>
 * <p>
 * @date:2018年11月16日
 * </p>
 * <p>
 * @remark:
 * </p>
 */
@Api(tags = {"账号管理API"})
@Slf4j
@RestController
@RequestMapping
public class AccountController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private RoleService roleService;

    /**
     * 获取账号列表
     *
     * @param accountQueryParams
     * @param page
     * @return
     * @author 陈曾杰
     * @date 2018年11月16日 上午9:53:00
     */
    @ApiOperation(value = "账号查询")
    @ApiResponses({@ApiResponse(code = 0, message = "查询成功")})
    @GetMapping(value = "/account/list/page")
    public PageResult pageAccountByConditions(AccountQueryParams accountQueryParams, Page page) {
        CurrUserDetails userDetails = (CurrUserDetails)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        accountQueryParams.setTenantId(userDetails.getTenantId());
        Map<String, Object> map = accountService.pageAccountByConditions(accountQueryParams, page.getCurrent(), page.getPageSize());
        List<AccountDTO> list = (List<AccountDTO>) map.get("list");
        int count = (int) map.get("count");
        int total = (int) map.get("pageTotal");
        page.setCount(count);
        page.setTotal(total);
        return new PageResult(list, count);
    }

    /**
     * 检查用户名是否存在
     *
     * @param username
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:03:50
     */
    @GetMapping(value = "/account/userName/exist")
    public boolean validUserNameExist(String username) {
        Map<String, Object> map = accountService.validUserNameExist(username);
        boolean exist = (boolean) map.get("exist");
        return exist;
    }

    /**
     * 添加用户
     *
     * @param account
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:02:25
     */
    @ApiOperation(value = "账号添加")
    @ApiResponses({@ApiResponse(code = 0, message = "添加成功")})
    @PostMapping(value = "/account")
    public RestResponse<Boolean> add(AccountDTO account) {
        CurrUserDetails userDetails = (CurrUserDetails)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        accountService.createAccount(account, userDetails.getTenantId());
        return RestResponse.success();
    }

    /**
     * 获取用户信息
     *
     * @param username
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:04:07
     */
    @GetMapping(value = "/account/{username}")
    public RestResponse<AccountDTO> getAccount(@PathVariable("username") String username) {
        AccountDTO accountDTO = accountService.getAccount(username);
        return RestResponse.success(accountDTO);
    }

    /**
     * 修改账号（修改手机号）
     *
     * @param modifyMobileParams
     * @return
     * @author:陈曾杰
     * @date:2019年3月8日
     */
    @PutMapping(value = "/account/mobile")
    public RestResponse<Object> updateMobile(ModifyMobileParams modifyMobileParams) {
        accountService.modifyAccountMobile(modifyMobileParams.getUsername(), modifyMobileParams.getMobile());
        return RestResponse.success();
    }

    /**
     * 修改密码
     *
     * @param modifyPasswordParams
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:01:40
     */
    @PutMapping(value = "/account/password")
    public Map<String, Object> updatePwd(@RequestBody ModifyPasswordParams modifyPasswordParams) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        modifyPasswordParams.setUsername(userDetails.getUsername());
        Map<String, Object> returnMap = accountService.modifyAccountPassword(modifyPasswordParams);
        return returnMap;
    }

    /**
     * 删除用户
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:02:48
     */
    @DeleteMapping(value = "/account/{userName}")
    public RestResponse<Boolean> delete(@PathVariable("userName") String userName) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CurrUserDetails) {
            CurrUserDetails userDetails = (CurrUserDetails) principal;
            accountService.delete(userName, userDetails.getTenantId());
            return RestResponse.success();
        } else {
            throw new ApplicationException(ErrorCode.E_100110);
        }
    }

    /**
     * 获取已绑定角色
     *
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:03:31
     */
    @ApiOperation("获取某账号已绑定的角色")
    @ApiImplicitParam(name = "userName", value = "账号名", required = true, dataType = "String", paramType = "path")
    @GetMapping(value = "/account/role")
    public RestResponse<List<String>> getAccountRole(String username) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<String> roleCodeList = accountService.getAccountRole(username, userDetails.getTenantId());
        return RestResponse.success(roleCodeList);
    }

    /**
     * 账号绑定角色
     *
     * @param username
     * @param role
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:03:40
     */
    @ApiOperation("绑定角色")
    @ApiImplicitParams({@ApiImplicitParam(name = "userName", value = "账号", dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "role", value = "角色，可以多个,String数组", required = true, dataType = "String[]", paramType = "query"),})
    @PostMapping(value = "/account/role/actions/bind")
    public RestResponse<Object> bindRole(String username, String[] role) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        accountService.bindRole(username, userDetails.getTenantId(), role);
        return RestResponse.success();
    }

    /**
     * 查询角色
     *
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:03:24
     */
    @ApiOperation("查询角色")
    @GetMapping(value = "/account/queryRole")
    @ResponseBody
    public RestResponse<List<TenantRoleDTO>> queryRole() {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        List<TenantRoleDTO> returnMap = roleService.queryTenantRole(userDetails.getTenantId());
        return RestResponse.success(returnMap);
    }

    /**
     * 查询已有权限
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:13:40
     */
    @ApiOperation("查询某一角色下已有权限")
    @ApiImplicitParam(name = "code", value = "角色编码", required = true, dataType = "String", paramType = "path")
    @GetMapping(value = "/account/queryBindPrivilege/{code}")
    public Map<String, Object> queryBindPrivilege(@PathVariable("code") String code) {
        Map<String, Object> returnMap = roleService.queryBindPrivilege(code);
        return returnMap;
    }

    /**
     * 绑定权限
     * @param code
     * @param privileges
     * @return
     */
    @ApiOperation("为某一角色绑定权限")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "角色编码", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "privileges", value = "权限  List<String>", required = true, dataType = "List<String>", paramType = "body"),})

    @PostMapping(value = "/account/bindPrivilege/{code}")
    public Map<String, Object> bindPrivilege(@PathVariable("code") String code, String[] privileges) {
        List<String> pri = Arrays.asList(privileges);
        Map<String, Object> returnMap = roleService.bindPrivilege(code, pri);
        return returnMap;
    }

    /**
     * 查询登录所用租户的所有权限
     * @return
     * @author 孔得峰
     * @throws ApiException
     * @date 2018年8月15日 上午10:13:49
     */
    @ApiOperation("查询登录租户的所有权限,权限树")
    @GetMapping(value = "/account/role/privilegeTree")
    public Map<String, Object> queryPrivilegeTree() throws ApiException {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Map<String, Object> returnMap = roleService.queryPrivilegeTree(userDetails.getTenantType());
        return returnMap;
    }


    /**
     * 添加角色
     * @param role
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:14:51
     */
    @PostMapping(value = "/account/role")
    public RestResponse<Boolean> add(TenantRoleDTO role) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        roleService.createTenantRole(userDetails.getTenantId(), role);
        return RestResponse.success(true);
    }

    /**
     * 修改角色
     * @param role
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:14:36
     */
    @PutMapping(value = "/account/role")
    public RestResponse<Boolean> update(TenantRoleDTO role) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        roleService.modifyTenantRole(userDetails.getTenantId(), role);
        return RestResponse.success(true);
    }


    /**
     * 删除角色
     * @param code
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:14:44
     */
    @DeleteMapping(value = "/account/role/{code}")
    public RestResponse<Boolean> deleteRole(@PathVariable("code") String code) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        roleService.removeTenantRole(userDetails.getTenantId(), code);
        return RestResponse.success(true);
    }

    /**
     * 密码重置
     * @param modifyPasswordParams
     * @return
     */
    @PostMapping("/account/resetPwd")
    public Map<String, Object> resetPwd(ModifyPasswordParams modifyPasswordParams) {
        Map<String, Object> returnMap = accountService.resetAccountPassword(modifyPasswordParams);
        return returnMap;
    }
}
