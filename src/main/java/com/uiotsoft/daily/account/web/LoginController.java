package com.uiotsoft.daily.account.web;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <p>LoginController 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年04月27日 11:15</p>
 * <p>@remark：</p>
 */
@RestController
@RequestMapping
public class LoginController {

    /**
     * 退出
     *
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping(value = "logout", method = RequestMethod.POST)
    @ResponseBody
    public String logout(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return "/index";
    }
}
