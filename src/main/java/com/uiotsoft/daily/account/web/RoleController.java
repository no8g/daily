/**
 * @(#)RoleController.java 2018年10月30日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.web;

import com.uiotsoft.daily.account.dto.TenantRoleDTO;
import com.uiotsoft.daily.account.service.RoleService;
import com.uiotsoft.daily.config.security.CurrUserDetails;
import com.uiotsoft.micro.api.exception.ApiException;
import com.uiotsoft.micro.common.domain.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 角色控制层
 * <p>RoleController.java此类用于处理角色相关业务</p>
 * <p>@author:孔得峰</p>
 * <p>@date:2018年11月16日</p>
 */
@RestController
@RequestMapping
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 查询当前用户所属租户的角色列表
     * @return
     * @author:陈曾杰
     * @date:2018年11月19日 上午9:03:24
     */
    @GetMapping(value = "/tenant/role/list")
    public RestResponse<List<TenantRoleDTO>> queryTenantRoleList() {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<TenantRoleDTO> tenantRoleDtoList = roleService.queryTenantRole(userDetails.getTenantId());
        return RestResponse.success(tenantRoleDtoList);
    }

    /**
     * 添加角色
     * @param role
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:14:51
     */
    @PostMapping(value = "/role")
    public RestResponse<Boolean> add(TenantRoleDTO role) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        roleService.createTenantRole(userDetails.getTenantId(), role);
        return RestResponse.success(true);
    }

    /**
     * 修改角色
     * @param role
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:14:36
     */
    @PutMapping(value = "/role")
    public RestResponse<Boolean> update(TenantRoleDTO role) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        roleService.modifyTenantRole(userDetails.getTenantId(), role);
        return RestResponse.success(true);
    }


    /**
     * 删除角色
     * @param code
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:14:44
     */
    @DeleteMapping(value = "/role/{code}")
    public RestResponse<Boolean> delete(@PathVariable("code") String code) {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        roleService.removeTenantRole(userDetails.getTenantId(), code);
        return RestResponse.success(true);
    }


    /**
     * 查询登录所用租户的所有权限
     * @return
     * @author 孔得峰
     * @throws ApiException
     * @date 2018年8月15日 上午10:13:49
     */
    @GetMapping(value = "/role/privilegeTree")
    public Map<String, Object> queryPrivilegeTree() throws ApiException {
        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Map<String, Object> returnMap = roleService.queryPrivilegeTree(userDetails.getTenantType());
        return returnMap;
    }

    /**
     * 查询已有权限
     * @return
     * @author 孔得峰
     * @date 2018年8月15日 上午10:13:40
     */
    @GetMapping(value = "/role/queryBindPrivilege/{code}")
    public Map<String, Object> queryBindPrivilege(@PathVariable("code") String code) {
        Map<String, Object> returnMap = roleService.queryBindPrivilege(code);
        return returnMap;
    }

    /**
     *  绑定权限
     * @param code
     * @param privileges
     * @return
     */
    @PostMapping(value = "/role/bindPrivilege/{code}")
    public Map<String, Object> bindPrivilege(@PathVariable("code") String code, @RequestBody List<String> privileges) {
        Map<String, Object> returnMap = roleService.bindPrivilege(code, privileges);
        return returnMap;
    }
}
