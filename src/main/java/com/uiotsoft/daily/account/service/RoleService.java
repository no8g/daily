/**
 * @(#)RoleService.java 2018年10月30日 上午8:38:45
 *
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.service;

import com.uiotsoft.daily.account.dto.TenantRoleDTO;
import com.uiotsoft.micro.api.exception.ApiException;

import java.util.List;
import java.util.Map;


/**
 * <p>RoleService.java此类用于角色管理</p>
 * <p>@author:陈曾杰</p>
 * <p>@date:2018年11月19日</p>
 * <p>@remark:</p>
 */
public interface RoleService {
	

	/**
	 * 创建租户内角色
	 * 
	 * @param tenantId
	 * @param role
	 */
	void createTenantRole(Long tenantId, TenantRoleDTO role);

	/**
	 * 删除租户内角色
	 * 
	 * @param tenantId
	 * @param roleCode
	 * @return 
	 */
	void removeTenantRole(Long tenantId, String roleCode);
	
	/**
	 * 修改租户内角色
	 * 
	 * @param tenantId
	 * @param role
	 */
	void modifyTenantRole(Long tenantId, TenantRoleDTO role);

	/**
	 * 查询某租户下角色
	 * 
	 * @param tenantId
	 * @return
	 */
	List<TenantRoleDTO> queryTenantRole(Long tenantId);
	
	/**
	 * 查询租户类型权限列表
	 * @param tenantType
	 * @return
	 * @author 孔得峰
	 * @throws ApiException 
	 * @date 2018年8月14日 下午5:15:13
	 */
	Map<String, Object> queryPrivilegeTree(String tenantType) throws ApiException;
	/**
	 * 获取角色已经绑定的权限
	 * @param code
	 * @return
	 * @author 孔得峰
	 * @date 2018年8月15日 上午10:17:04
	 */
	Map<String, Object> queryBindPrivilege(String code);
	/**
	 * 绑定角色权限
	 * @param code
	 * @param privileges
	 * @return
	 * @author 孔得峰
	 * @date 2018年8月15日 上午11:31:51
	 */
	Map<String, Object> bindPrivilege(String code, List<String> privileges);

}
