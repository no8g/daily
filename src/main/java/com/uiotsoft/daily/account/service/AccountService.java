/**
 * @(#)AccountService.java 2019年10月30日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.service;

import com.uiotsoft.daily.account.dto.AccountDTO;
import com.uiotsoft.daily.account.dto.AccountQueryParams;
import com.uiotsoft.daily.account.dto.ModifyPasswordParams;
import com.uiotsoft.daily.organization.entity.User;

import java.util.List;
import java.util.Map;


/**
 * <p>AccountService.java此类用于用户服务</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark: 租户id要冗余至业务信息</p>
 */
public interface AccountService {

    /**
     * 分页条件查询账号列表
     *
     * @param query    query
     * @param pageNo   pageNo
     * @param pageSize pageSize
     * @return Map<String, Object>
     */
    Map<String, Object> pageAccountByConditions(AccountQueryParams query, Integer pageNo, Integer pageSize);

    /**
     * 校验账号是否存在
     *
     * @param userName userName
     * @return Map<String, Object>
     */
    Map<String, Object> validUserNameExist(String userName);

    /**
     * 创建账号
     *
     * @param account  account
     * @param tenantId tenantId
     */
    void createAccount(AccountDTO account, Long tenantId);

    /**
     * 查询账号
     *
     * @param userName userName
     * @return AccountDTO
     */
    AccountDTO getAccount(String userName);

    /**
     * 修改密码
     *
     * @param modifyPasswordParams modifyPasswordParams
     * @return Map<String, Object>
     */
    Map<String, Object> modifyAccountPassword(ModifyPasswordParams modifyPasswordParams);

    /**
     * 修改手机号
     *
     * @param userName userName
     * @param mobile   mobile
     */
    void modifyAccountMobile(String userName, String mobile);

    /**
     * 获取账号已经绑定的角色
     *
     * @param userName userName
     * @param tenantId tenantId
     * @return List<String>
     */
    List<String> getAccountRole(String userName, Long tenantId);

    /**
     * 账号绑定角色
     *
     * @param username username
     * @param tenantId tenantId
     * @param roles    roles
     */
    void bindRole(String username, Long tenantId, String[] roles);

    /**
     * 账号绑定单个角色
     *
     * @param userName userName
     * @param tenantId tenantId
     * @param role     role
     */
    void bindSingleRole(String userName, Long tenantId, String role);

    /**
     * 账号解绑角色
     *
     * @param userName userName
     * @param tenantId tenantId
     * @param roles    roles
     */
    void unBindRole(String userName, Long tenantId, String[] roles);

    /**
     * 删除用户
     *
     * @param userName userName
     * @param tenantId tenantId
     */
    void delete(String userName, Long tenantId);

    /**
     * 密码重置
     *
     * @param modifyPasswordParams modifyPasswordParams
     * @return Map<String, Object>
     */
    Map<String, Object> resetAccountPassword(ModifyPasswordParams modifyPasswordParams);

    /**
     * 根据角色编码查询用户列表
     *
     * @param roleCode roleCode
     * @return List<User>
     */
    List<User> queryUserListByRoleCode(String roleCode);

}
