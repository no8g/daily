/**
 * @(#)AccountServiceImpl.java 2019年06月22日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.uiotsoft.daily.account.dto.AccountDTO;
import com.uiotsoft.daily.account.dto.AccountQueryParams;
import com.uiotsoft.daily.account.dto.ModifyPasswordParams;
import com.uiotsoft.daily.account.exception.ApplicationException;
import com.uiotsoft.daily.account.exception.ErrorCode;
import com.uiotsoft.daily.common.util.JsonUtil;
import com.uiotsoft.daily.common.util.MicroServiceClient;
import com.uiotsoft.daily.common.util.RandomUtils;
import com.uiotsoft.daily.common.util.password.UsernameAndPasswordMd5PasswordEncoder;
import com.uiotsoft.daily.config.security.CurrUserDetails;
import com.uiotsoft.daily.organization.entity.User;
import com.uiotsoft.daily.organization.service.UserService;
import com.uiotsoft.micro.api.UiotClient;
import com.uiotsoft.micro.api.exception.ApiException;
import com.uiotsoft.micro.api.request.UiotRequest;
import com.uiotsoft.micro.constant.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * AccountServiceImpl.java此类用于用户管理接口实现
 *
 * @author:hmx
 * @date:2019年06月22日
 * @remark:
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class AccountServiceImpl implements AccountService {

    @Autowired
    private UiotClient uiotClient;

    @Autowired
    private MicroServiceClient microServiceClient;

    @Resource
    private UserService userService;

    @Override
    public Map<String, Object> pageAccountByConditions(AccountQueryParams queryParams, Integer pageNo, Integer pageSize) {
        Page<AccountDTO> page = microServiceClient.pageAccount(pageNo, pageSize, queryParams);
        if (page == null) {
            throw new ApplicationException(ErrorCode.E_110108);
        }
        List<AccountDTO> list = page.getContent();
        int count = (int) page.getTotalElements();
        int pageTotal = page.getTotalPages();
        Map<String, Object> map = new HashMap<>(16);
        map.put("list", list);
        map.put("count", count);
        map.put("pageTotal", pageTotal);
        return map;
    }

    @Override
    public Map<String, Object> validUserNameExist(String userName) {
        Map<String, Object> exist = microServiceClient.checkAccountExist(userName);
        return exist;
    }

    /**
     * 创建账号
     */
    @Override
    public void createAccount(AccountDTO account, Long tenantId) {
        if (StrUtil.isBlank(account.getUsername())) {
            throw new ApplicationException(ErrorCode.E_120101);
        }
        if (StrUtil.isBlank(account.getMobile())) {
            throw new ApplicationException(ErrorCode.E_120103);
        }
        if (StrUtil.isBlank(account.getPassword())) {
            throw new ApplicationException(ErrorCode.E_120102);
        }
        String userName = account.getUsername();
        Map<String, Object> map = microServiceClient.checkAccountExist(userName);
        boolean exist = (boolean) map.get("exist");
        Integer code = (Integer) map.get("code");
        // 账号存在
        if (exist && code == 0) {
            microServiceClient.bindAccountToTenant(userName, tenantId);
        }
        // 账号不存在，新建账号
        else if (!exist && code == 0) {
            microServiceClient.createAccount(userName, account.getPassword(), account.getMobile());
            microServiceClient.bindAccountToTenant(userName, tenantId);
        }
    }

    @Override
    public AccountDTO getAccount(String username) {
        JSONObject accountJson = microServiceClient.getAccount(username);
        AccountDTO accountDTO = JSONObject.toJavaObject(accountJson, AccountDTO.class);
        return accountDTO;
    }

    @Override
    public void modifyAccountMobile(String userName, String mobile) {
        microServiceClient.modifyMobile(userName, mobile);
    }

    @Override
    public void delete(String userName, Long tenantId) {
        // 解绑角色
        List<String> roleList = microServiceClient.authorization(userName, tenantId);
        if (roleList.size() > 0) {
            microServiceClient.unBindRole(userName, tenantId, roleList);
        }
        // 解绑租户
        microServiceClient.userUnbindAccount(userName, tenantId);
    }

    @Override
    public Map<String, Object> modifyAccountPassword(ModifyPasswordParams modifyPasswordParams) {
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/account/password");
        String method = Constants.METHOD_PUT;
        request.setApi(api);
        request.setMethod(method);
        String newPassword = new UsernameAndPasswordMd5PasswordEncoder().encode(modifyPasswordParams.getNewPassword(),
                modifyPasswordParams.getUsername());
        String oldPassword = new UsernameAndPasswordMd5PasswordEncoder().encode(modifyPasswordParams.getOldPassword(),
                modifyPasswordParams.getUsername());
        modifyPasswordParams.setNewPassword(newPassword);
        modifyPasswordParams.setOldPassword(oldPassword);
        request.setBodyParams(modifyPasswordParams);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            log.info(JsonUtil.objectTojson(response));
        } catch (ApiException e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("修改密码失败");
        }
        Map<String, Object> returnMap = new HashMap<>(16);
        Integer code = response.getInteger("code");
        if (code != 0) {
            returnMap.put("code", code);
            returnMap.put("msg", response.getString("msg"));
        } else {
            returnMap.put("code", 0);
        }
        return returnMap;
    }

    @Override
    public List<String> getAccountRole(String userName, Long tenantId) {
        List<String> roleCodeList = microServiceClient.authorization(userName, tenantId);
        return roleCodeList;
    }

    @Override
    public void bindRole(String userName, Long tenantId, String[] roles) {
        if (roles == null || roles.length == 0) {
            microServiceClient.unBindRole(userName, tenantId, Arrays.asList(roles));
        } else {
            microServiceClient.bindRole(userName, tenantId, Arrays.asList(roles));
        }
    }

    @Override
    public void bindSingleRole(String userName, Long tenantId, String role) {
        // 先查询此账号下绑定角色
        List<String> roleCodeList = microServiceClient.authorization(userName, tenantId);
        // 判断角色列表中是否存在该角色
        if (!CollUtil.contains(roleCodeList, role) && StrUtil.isNotBlank(role)) {
            roleCodeList.add(role);
            microServiceClient.bindRole(userName, tenantId, roleCodeList);
        }
    }

    @Override
    public void unBindRole(String userName, Long tenantId, String[] roles) {
        microServiceClient.unBindRole(userName, tenantId, Arrays.asList(roles));
    }

    @Override
    public Map<String, Object> resetAccountPassword(ModifyPasswordParams modifyPasswordParams) {

        CurrUserDetails userDetails = (CurrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // 生成6位随机密码
        String randomPass = RandomUtils.getRandom(6);
        modifyPasswordParams.setNewPassword(randomPass);
        UiotRequest request = new UiotRequest();
        String api = String.format("/user/account/password");
        String method = Constants.METHOD_PUT;
        request.setApi(api);
        request.setMethod(method);
        String newPassword = new UsernameAndPasswordMd5PasswordEncoder().encode(modifyPasswordParams.getNewPassword(), modifyPasswordParams.getUsername());
        String oldPassword = modifyPasswordParams.getOldPassword();
        modifyPasswordParams.setNewPassword(newPassword);
        modifyPasswordParams.setOldPassword(oldPassword);
        request.setBodyParams(modifyPasswordParams);
        JSONObject response = null;
        try {
            response = uiotClient.execute(request);
            log.info(JsonUtil.objectTojson(response));
            User userInfo = userService.getUser(modifyPasswordParams.getUsername());
        } catch (ApiException e) {
            log.error("修改密码失败异常：", e);
            throw new UsernameNotFoundException("修改密码失败");
        }
        Map<String, Object> returnMap = new HashMap<>(16);
        Integer code = response.getInteger("code");
        if (code != 0) {
            returnMap.put("code", code);
            returnMap.put("msg", response.getString("msg"));
        } else {
            returnMap.put("code", 0);
        }
        return returnMap;
    }

    /**
     * 根据角色编码获取用户列表
     *
     * @param roleCode
     * @return
     */
    @Override
    public List<User> queryUserListByRoleCode(String roleCode) {
        List<User> userList = new ArrayList<>();
        List<String> list = microServiceClient.authorizationByRoleCode(roleCode);
        if (CollUtil.isEmpty(list)) {
            return userList;
        }
        for (String account : list) {
            if (StrUtil.isBlank(account)) {
                continue;
            }
            User user = userService.getUser(account);
            if (ObjectUtil.isNull(user)) {
                user = new User();
                user.setUserName(account);
                user.setTrueName(account);
            }
            userList.add(user);
        }
        return userList;
    }

}
