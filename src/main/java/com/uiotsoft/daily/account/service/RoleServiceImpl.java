/**
 * @(#)RoleServiceImpl.java 2018年10月30日 上午8:38:45
 *
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.uiotsoft.daily.account.dto.TenantRoleDTO;
import com.uiotsoft.daily.common.util.MicroServiceClient;
import com.uiotsoft.micro.api.UiotClient;
import com.uiotsoft.micro.api.exception.ApiException;
import com.uiotsoft.micro.api.request.UiotRequest;
import com.uiotsoft.micro.constant.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 孔得峰
 * @date 2018年8月14日 上午10:41:42
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RoleServiceImpl implements RoleService {

	@Autowired
	private UiotClient uiotClient;
	@Autowired
	private MicroServiceClient microServiceClient;
	
	@Override
	public void createTenantRole(Long tenantId, TenantRoleDTO role) {
		microServiceClient.createTenantRole(tenantId,role);
	}

	@Override
	public void removeTenantRole(Long tenantId, String roleCode) {
		microServiceClient.removeTenantRole(tenantId,roleCode);
	}

	@Override
	public void modifyTenantRole(Long tenantId, TenantRoleDTO role) {
		microServiceClient.modifyTenantRole(tenantId,role);
	}

	@Override
	public List<TenantRoleDTO> queryTenantRole(Long tenantId) {
		JSONArray roleArray = microServiceClient.queryTenantRole(tenantId);
		 List<TenantRoleDTO> list = roleArray.toJavaList(TenantRoleDTO.class);
		return list;
	}

	@Override
	public Map<String, Object> queryPrivilegeTree(String tenantType) throws ApiException {
		UiotRequest request = new UiotRequest();
		String api = String.format("/user/tenantType/%s", tenantType);
		String method = Constants.METHOD_GET;
		request.setApi(api);
		request.setMethod(method);
		JSONObject response = null;
		response = uiotClient.execute(request);
		JSONArray roles = response.getJSONObject("result").getJSONArray("roles");
		List<String> roleList = new ArrayList<>();
		if(roles != null) {
			for(Object role : roles) {
				@SuppressWarnings("unchecked")
				Map<String, Object> roleMap = (Map<String,Object>) role;
				String roleCode = roleMap.get("roleCode").toString();
				roleList.add(roleCode);
			}
		}
		JSONObject privileges = queryPrivileges(String.join(",", roleList));
		return privileges;
	}
	
	public JSONObject queryPrivileges(String roleCodes) {
		UiotRequest request = new UiotRequest();
		String api = String.format("/authorization/privilegeTree/roles/%s", roleCodes);
		String method = Constants.METHOD_GET;
		request.setApi(api);
		request.setMethod(method);
		JSONObject response = null;
		try {
			response = uiotClient.execute(request);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		JSONObject privileges = response.getJSONObject("result");
		return privileges;
	}

	@Override
	public Map<String, Object> queryBindPrivilege(String code) {
		UiotRequest request = new UiotRequest();
		String api = String.format("/authorization/role/%s", code);
		String method = Constants.METHOD_GET;
		request.setApi(api);
		request.setMethod(method);
		JSONObject response = null;
		try {
			response = uiotClient.execute(request);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		JSONObject role = response.getJSONObject("result");
		return role;
	}

	@Override
	public Map<String, Object> bindPrivilege(String roleCode, List<String> privileges) {
		UiotRequest request = new UiotRequest();
		String api = String.format("/authorization/role/%s/privileges/", roleCode);
		String method = Constants.METHOD_PUT;
		request.setApi(api);
		request.setMethod(method);
		request.setBodyParams((JSONObject) privileges);
		JSONObject response = null;
		try {
			response = uiotClient.execute(request);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		Map<String, Object> returnMap = new HashMap<>(16);
		Integer code = response.getInteger("code");
		if(code != 0){
			returnMap.put("code", code);
			returnMap.put("msg", response.getString("msg"));
		}else {
			returnMap.put("code", 0);
		}
		return returnMap;
	}
	
	
}
