/**
 * @(#)ModifyMobileParams.java 2018年10月30日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>ModifyMobileParams.java此类用于修改手机号参数</p>
 * <p>@author:陈曾杰</p>
 * <p>@date:2018年11月19日</p>
 * <p>@remark:</p>
 */
@Data
@ApiModel(value = "ModifyMobileParams", description = "修改手机号参数")
public class ModifyMobileParams {

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("要更改的手机号")
    private String mobile;

    @ApiModelProperty("老手机号收到的验证码")
    private String code;

}
