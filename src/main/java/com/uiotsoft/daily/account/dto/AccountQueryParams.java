/**
 * @(#)AccountQueryParams.java 2019年06月22日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.dto;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>AccountQueryParams.java此类用于账号查询参数</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark:</p>
 */
@Data
@ApiModel(value = "AccountQueryParams", description = "账号查询参数")
public class AccountQueryParams extends JSONObject {

    @ApiModelProperty("所属租户id")
    private Long tenantId;

    @ApiModelProperty("所属租户id列表")
    private List<Long> tenantIds;

    @ApiModelProperty("用户名(模糊匹配)")
    private String username;

    private String accountName;


    private String mobile;

    private String currUserName;

    /**
     * 部门id
     */
    private Long deptId;

}
