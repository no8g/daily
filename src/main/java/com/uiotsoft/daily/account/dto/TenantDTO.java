/**
 * @(#)TenantDTO.java 2019年06月22日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>TenantDTO.java此类用于租户信息</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark:</p>
 */
@Data
@ApiModel(value = "TenantDTO", description = "租户信息")
public class TenantDTO {

    @ApiModelProperty("租户id")
    private Long id;

    @ApiModelProperty("租户名称")
    private String name;

    @ApiModelProperty("租户类型编码")
    private String tenantTypeCode;

}
