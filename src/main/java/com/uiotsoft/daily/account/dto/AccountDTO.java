/**
 * @(#)AccountDTO.java 2019年06月22日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.dto;

import com.uiotsoft.daily.account.entity.Account;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>AccountDTO.java此类用于用户管理</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark:</p>
 */
@Data
@ApiModel(value = "AccountDTO", description = "账号信息DTO")
public class AccountDTO {

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;
    /**
     * 区域编号
     */
    @ApiModelProperty("区域编号")
    private String areaCode;
    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String mobile;
    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;
    /**
     * 盐
     */
    private String salt;
    /**
     * 所属租户
     */
    @ApiModelProperty("所属租户")
    private List<TenantDTO> tenants;

    private String trueName;

    public static List<AccountDTO> convertEntityToDtos(List<Account> entityList) {
        List<AccountDTO> dtoList = new ArrayList<>();
        for (Account entity : entityList) {
            if (entity == null) {
                continue;
            }
            dtoList.add(convertEntityToDto(entity));
        }
        return dtoList;
    }

    public static AccountDTO convertEntityToDto(Account entity) {
        if (entity == null) {
            return null;
        }
        AccountDTO dto = new AccountDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public static Account convertDtoToEntity(AccountDTO accountDTO) {
        Account account = new Account();
        BeanUtils.copyProperties(accountDTO, account);
        return account;
    }
}
