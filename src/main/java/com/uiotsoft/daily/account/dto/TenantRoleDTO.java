/**
 * @(#)TenantRoleDTO.java 2019年06月22日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.dto;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>TenantRoleDTO.java此类用于租户内角色信息</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark:</p>
 */
@ApiModel(value = "TenantRoleDTO", description = "租户内角色信息")
@Data
public class TenantRoleDTO extends JSONObject {

    @ApiModelProperty("角色编码")
    private String code;

    @ApiModelProperty("角色名称")
    private String name;

    @ApiModelProperty("角色所拥有的权限列表")
    private List<String> privilegeCodes;

}
