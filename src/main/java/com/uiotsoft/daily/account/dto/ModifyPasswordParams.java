/**
 * @(#)ModifyPasswordParams.java 2019年06月22日 上午8:38:45
 * <p>
 * Copyright HeNan UiotSoft. All rights reserved.
 */
package com.uiotsoft.daily.account.dto;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>ModifyPasswordParams.java此类用于修改密码参数</p>
 * <p>@author:hmx</p>
 * <p>@date:2019年06月22日</p>
 * <p>@remark:</p>
 */
@Data
@ApiModel(value = "ModifyPasswordParams", description = "修改密码参数")
public class ModifyPasswordParams extends JSONObject {

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("新密码")
    private String newPassword;

    @ApiModelProperty("老密码")
    private String oldPassword;

}
