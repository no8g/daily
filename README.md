# daily

#### Description
公司每天需要员工填写的日报内容

#### Software Architecture
Software architecture description
Spring boot (2.0.0.RELEASE) + Spring security cas + Mybatis-plus (3.1.1) + Jdk 1.8 + druid (1.1.9) + javax.mail (1.6.1) + poi (3.10.1) + maven + Vue

#### Installation

1.  安装JDK 1.8
2.  Intellij Idea
3.  Maven
4.  添加pom.xml构建文件中的依赖

#### Functions

主要有以下功能

1.  接入公司的统一用户系统
2.  数据Excel导出
3.  定时任务执行读取邮箱收件箱内容，并按日期筛选收件箱邮件
4.  定时任务执行日报未提交钉钉通知功能
5.  定时任务执行日报未提交记录保存
6.  钉钉企业内部应用免登（H5微应用）
7.  日报评论钉钉提醒功能
8.  定时任务异步执行


#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
